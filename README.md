# Honyomi - Manga Reader Client

[![pipeline status](https://gitlab.com/laituananh1711/honyomi_client/badges/master/pipeline.svg)](https://gitlab.com/laituananh1711/honyomi_client/-/commits/master) [![coverage report](https://gitlab.com/laituananh1711/honyomi_client/badges/master/coverage.svg)](https://gitlab.com/laituananh1711/honyomi_client/-/commits/master)

This is React Client for Honyomi Manga Reader Apps. It's based on the venerable [React Boilerplate](https://cansahin.gitbook.io/react-boilerplate-cra-template/)

## Install

You should use the latest Yarn 1 package manager for this repository.

```sh
# Set yarn version to latest
yarn set version latest
```

```sh
# Install dependencies
yarn
```

### Generate components & redux slice

See more [here](https://cansahin.gitbook.io/react-boilerplate-cra-template/tools/commands).

```sh
yarn generate
```

### Test & Coverage

```sh
# Run test in watch mode
yarn test

# Run test for CI/CD
yarn test:ci
```
