import { PaletteMode } from '@mui/material';

const getTheme = (mode: PaletteMode) => ({
  palette: {
    mode,
    ...(mode === 'light'
      ? {
          // palette values for light mode
          primary: {
            main: '#1a1a1a',
            light: '#cacaca',
            dark: '#cacaca',
            contrastText: '#f4f4f4',
          },
          secondary: {
            main: '#f4f4f4',
            light: '#1a1a1a',
            dark: '#1a1a1a',
          },
          contrast: {
            main: '#1a1a1a',
            light: '#f4f4f4',
            dark: '#f4f4f4',
            contrastText: '#1a1a1a',
          },
          background: {
            default: '#ffffff',
            paper: '#ffffff',
            card: '#f4f4f4',
            cardDarker: '#eaeaea',
            contrastCard: '#1a1a1a',
            chip: '#e7e7e7',
          },
          text: {
            primary: '#1a1a1a',
            secondary: '#6d6d6d',
          },
          divider: '#ffffff',
        }
      : {
          // palette values for dark mode
          primary: {
            main: '#f4f4f4',
            dark: '#cacaca',
            light: '#333333',
            contrastText: '#1a1a1a',
          },
          secondary: {
            main: '#1a1a1a',
            dark: '#f4f4f4',
            light: '#f4f4f4',
          },
          contrast: {
            main: '#1a1a1a',
            dark: '#cacaca',
            light: '#cacaca',
            contrastText: '#1a1a1a',
          },
          background: {
            default: '#000000',
            paper: '#000000',
            card: '#1a1a1a',
            cardDarker: '#343434',
            contrastCard: '#f4f4f4',
            chip: '#333333',
          },
          text: {
            primary: '#f4f4f4',
            secondary: '#989898',
          },
          divider: '#000000',
        }),
  },
});

declare module '@mui/material/styles/createPalette' {
  interface TypeBackground {
    card?: string;
    contrastCard?: string;
    chip?: string;
  }
}

declare module '@mui/material/styles' {
  interface Palette {
    contrast: Palette['primary'];
  }

  interface PaletteOptions {
    contrast?: PaletteOptions['primary'];
  }
}

// declare module '@mui/material/IconButton' {
//   interface IconButtonPropsColorOverrides {
//     contrast: true;
//   }
// }

declare module '@mui/material/Button' {
  interface ButtonPropsColorOverrides {
    contrast: true;
  }
}

export default getTheme;
