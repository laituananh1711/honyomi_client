import {
  cleanup,
  Matcher,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import React, { useEffect } from 'react';
import { useAsync } from './useAsync';

const TestComponentFactory = (promise: Promise<any>): React.FC => {
  return function TestComponent() {
    const { loading, data, error, run } = useAsync();

    useEffect(() => {
      run(promise);
    }, [run]);

    return (
      <div>
        <div>{loading && 'Loading'}</div>
        <div>{data}</div>
        <div>{error?.message}</div>
      </div>
    );
  };
};

describe('useAsync', () => {
  afterEach(() => cleanup());

  interface testCase {
    name: string;
    textPresent: Matcher;
    promiseProp: () => Promise<any>;
  }

  const testCases: testCase[] = [
    {
      name: 'should show resolved data',
      textPresent: 'Resolved',
      promiseProp: () => Promise.resolve('Resolved'),
    },
    {
      name: 'should show rejected data',
      textPresent: 'Rejected',
      promiseProp: () => Promise.reject(new Error('Rejected')),
    },
  ];

  for (const tt of testCases) {
    it(tt.name, async () => {
      const TestComponent = TestComponentFactory(tt.promiseProp());
      render(<TestComponent />);

      expect(screen.getByText('Loading')).toBeInTheDocument();

      await waitForElementToBeRemoved(() => screen.getByText('Loading'));

      expect(screen.getByText(tt.textPresent)).toBeInTheDocument();
    });
  }
});
