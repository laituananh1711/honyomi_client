import { CircularProgress } from '@mui/material';
import authApi from 'app/api/auth/authApi';
import { User } from 'app/models/User';
import React from 'react';
import { createContext, useContext, useState } from 'react';
import { initialSession, Session, userDefault } from '../models/session';
import { useAsync } from './useAsync';

export const SessionContext = createContext<
  [Session, (session: Session) => void]
>([initialSession, () => {}]);
export const useSessionContext = () => useContext(SessionContext);

export const SessionContextProvider: React.FC = props => {
  const { loading, data, error, run } = useAsync<User>();

  let curUser = data ?? userDefault;

  React.useEffect(() => {
    run(authApi.getProfile());
  }, [run]);

  const [sessionState, setSessionState] = useState({
    ...initialSession,
    currentUser: userDefault,
  });
  if (loading) {
    return <CircularProgress />;
  } else {
    let isAuthenticated = error ? false : true;
    const defaultSessionContext: [Session, typeof setSessionState] = [
      {
        ...sessionState,
        isAuthenticated: isAuthenticated,
        currentUser: curUser,
      },
      setSessionState,
    ];

    return (
      <SessionContext.Provider value={defaultSessionContext}>
        {props.children}
      </SessionContext.Provider>
    );
  }
};
