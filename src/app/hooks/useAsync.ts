import { useCallback, useEffect, useReducer, useRef } from 'react';

export interface State<T> {
  error?: Error;
  loading: boolean;
  data?: T;
}

export enum ActionType {
  LOADING = 'loading',
  RESOLVED = 'resolved',
  REJECTED = 'rejected',
}

export interface Action {
  type: ActionType;
  data?: any;
  error?: Error;
}

function reducer<T = any>(state: State<T>, action: Action): State<T> {
  switch (action.type) {
    case ActionType.LOADING:
      return { ...state, loading: true };
    case ActionType.RESOLVED:
      return { ...state, loading: false, data: action.data };
    case ActionType.REJECTED:
      return { ...state, loading: false, error: action.error };
  }
}

/**
 * Create a promise resolver. Returns a list of variables to
 * indicates the current status of this promise.
 * @param initState initial state of this function
 * @returns
 */
export function useAsync<T = any>(initState?: Partial<State<T>>) {
  const initialState: State<T> = { loading: false, ...initState };

  const [state, dispatch] = useReducer<
    (state: State<T>, action: Action) => State<T>
  >(reducer, initialState);

  // Stop dispatching if this component is unmounted before
  // the promise resolves.
  const mountedRef = useRef(false);
  useEffect(() => {
    mountedRef.current = true;
    return () => {
      mountedRef.current = false;
    };
  }, []);

  const run = useCallback<(p: Promise<T>) => void>(promise => {
    dispatch({ type: ActionType.LOADING });
    promise
      .then(data => {
        if (mountedRef.current) dispatch({ type: ActionType.RESOLVED, data });
      })
      .catch(err => {
        if (mountedRef.current)
          dispatch({ type: ActionType.REJECTED, error: err });
      });
  }, []);

  return { ...state, run };
}
