import { _t } from '../../../utils/messages';
import { translations } from '../../../locales/translations';

export const messages = {
  rss: () => _t(translations.footer.rss),
  discover: () => _t(translations.footer.discover),
  about: () => _t(translations.footer.about),
  discord: () => _t(translations.footer.discord),
};
