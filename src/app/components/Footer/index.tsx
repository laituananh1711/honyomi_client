import styled from 'styled-components/macro';
import { Box } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';

const tradeMark = 'CatManga © 2021';

export default function Footer() {
  const { t } = useTranslation();
  return (
    <Container
      sx={{
        backgroundColor: 'background.contrastCard',
        color: 'text.secondary',
      }}
    >
      <TradeMark>{tradeMark}</TradeMark>
      <Nav>
        <NavItem>{t(messages.discover())}</NavItem>
        <NavItem>{t(messages.about())}</NavItem>
        <NavItem>{t(messages.discord())}</NavItem>
        <NavItem>{t(messages.rss())}</NavItem>
      </Nav>
    </Container>
  );
}

const Container = styled(Box)`
  display: grid;
  grid-template-columns: repeat(6, minmax(0rem, 1fr));
  grid-column-gap: 1.6rem;
  padding: 6.4rem 3.2rem;
  grid-row-gap: 4rem;

  @media (min-width: 768px) {
    grid-template-columns: repeat(13, minmax(0rem, 1fr));
    grid-column-gap: 2rem;
    padding: 6.4rem 4rem;
  }

  @media (min-width: 1270px) {
    grid-template-columns: repeat(24, minmax(0rem, 1fr));
    max-width: none;
  }
`;

const TradeMark = styled.p`
  grid-column: 1/-1;
  line-height: 3rem;

  @media (min-width: 768px) {
    grid-column: 1 / span 6;
  }

  margin: 0;
  padding: 0;
`;

const Nav = styled.div`
  grid-column: 1/-1;
  text-align: left;

  @media (min-width: 768px) {
    text-align: right;
    grid-column: -1 / span 6;
  }
`;

const NavItem = styled.a`
  //color: var(--color-100);
  -webkit-text-decoration: none;
  text-decoration: none;
  text-underline-offset: 4px;
  //text-decoration-color: var(--color-link-underline-inverted);
  margin-right: 3.2rem;
  line-height: 3rem;
  white-space: nowrap;
  
  @media (min-width: 768px){
    margin-right: 2.4rem;
`;
