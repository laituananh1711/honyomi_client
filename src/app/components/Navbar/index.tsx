import { Close, LoginRounded, MenuRounded, Search } from '@mui/icons-material';
import {
  Avatar,
  Backdrop,
  BackdropProps,
  Box,
  BoxProps,
  Drawer,
  DrawerProps,
  Menu,
  MenuItem,
  MenuProps,
  useMediaQuery,
} from '@mui/material';
import { useSessionContext } from 'app/hooks/sessionContext';
import { Link } from '../Link';
import IconButton from '@mui/material/IconButton';
import { duration, styled } from '@mui/material/styles';
import React, {
  KeyboardEvent,
  MouseEvent,
  ReactNode,
  useEffect,
  useState,
} from 'react';
import { useTranslation } from 'react-i18next';
import mangaApi from '../../api/manga/mangaApi';
import { SERIES } from '../../api/manga/mangaApi.constants';
import { MostPopularMangaDTO } from '../../api/types';
import { useAsync } from '../../hooks/useAsync';
import { Manga } from '../../models/Manga';
import { CardImageDiscover } from '../CardImageDiscover';
import CustomInput from '../CustomInput';
import { ThemeSwitch } from '../ThemeSwitch';
import FavIcon from './FavIcon';
import { messages } from './messages';

type Anchor = 'top' | 'left' | 'bottom' | 'right';

export function Navbar() {
  const { t } = useTranslation();
  const small = useMediaQuery('(max-width:768px)');

  const [open, setOpen] = useState<boolean>(false);
  const [session] = useSessionContext();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const userMenu = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const navBarItems = [
    {
      name: t(messages.home()),
      path: '/',
    },
    {
      name: t(messages.discover()),
      path: '/discover',
    },
    {
      name: t(messages.favorites()),
      path: '/favorites',
    },
  ];

  const toggleDrawer =
    (anchor: Anchor, open: boolean) => (event: KeyboardEvent | MouseEvent) => {
      if (
        event.type === 'keydown' &&
        ((event as KeyboardEvent).key === 'Tab' ||
          (event as KeyboardEvent).key === 'Shift')
      ) {
        return;
      }

      setOpen(open);
    };

  const [searchQuery, setSearchQuery] = useState('');

  const { data: mostPopularMangas, run: runMostPopularManga } = useAsync<
    MostPopularMangaDTO[]
  >({
    data: [],
  });

  useEffect(() => {
    runMostPopularManga(mangaApi.getMostPopularManga());
  }, [runMostPopularManga]);

  const { data: mangas, run: runSearchManga } = useAsync<Manga[]>({
    data: [],
  });

  useEffect(() => {
    // run get manga api, if search queries is empty, return empty results.
    const mangasPromise = new Promise<Manga[]>(resolve => {
      if (searchQuery === '') {
        return resolve([]);
      }
      mangaApi
        .getManga({
          q: searchQuery,
        })
        .then(res => resolve(res.data));
    });

    runSearchManga(mangasPromise);
  }, [runSearchManga, searchQuery]);

  const searchResults: ReactNode = (
    <SearchResultsContainer>
      {mangas!.length === 0
        ? t(messages.noResult())
        : mangas!.map(item => (
            <CardImageDiscover
              key={item.id}
              title={item.title}
              imageUrl={item.coverImage.original.url}
              path={`/${SERIES}/${item.id}`}
              style={{ width: '13rem' }}
              showMoreInfo={false}
            />
          ))}
    </SearchResultsContainer>
  );

  const search = (anchor: Anchor) => (
    <>
      {!small ? (
        <SearchContainer>
          <FixedButton>
            <RightItem>
              <IconButton color="inherit" onClick={toggleDrawer(anchor, false)}>
                <Close />
              </IconButton>
            </RightItem>
          </FixedButton>
          <SearchInputContainer>
            <SearchInputIcon>
              <RightItem>
                <Search />
              </RightItem>
            </SearchInputIcon>
            <SearchInput
              placeholder={t(messages.search())}
              onChange={event => {
                setSearchQuery(event.target.value);
              }}
              value={searchQuery}
            />
          </SearchInputContainer>
          {searchQuery === '' ? (
            <PopularContainer>
              <PopularTitle>{t(messages.popular())}</PopularTitle>
              {mostPopularMangas!.map(item => (
                <PopularItem key={item.manga.id}>
                  <MostPopularMangaLink to={`/${SERIES}/${item.manga.id}`}>
                    {item.manga.title}
                  </MostPopularMangaLink>
                </PopularItem>
              ))}
            </PopularContainer>
          ) : (
            searchResults
          )}
        </SearchContainer>
      ) : (
        <MenuContainer>
          <MenuHeader>
            <MenuIcon href="/">
              <FavIcon />
            </MenuIcon>
            <NavRight style={{ margin: 'auto' }}>
              <RelativeButton>
                <RightItem>
                  <ThemeSwitch />
                </RightItem>
              </RelativeButton>
              <RelativeButton>
                <RightItem>
                  <IconButton
                    color="inherit"
                    onClick={toggleDrawer(anchor, false)}
                  >
                    <Close />
                  </IconButton>
                </RightItem>
              </RelativeButton>
            </NavRight>
          </MenuHeader>
          <SearchInputContainer
            style={{ margin: '0rem 3.2rem', height: 'auto' }}
          >
            <SearchInputIcon>
              <RightItem>
                <Search />
              </RightItem>
            </SearchInputIcon>
            <SearchInput placeholder={t(messages.search())} />
          </SearchInputContainer>
          <MenuItems>
            {navBarItems.map(item => (
              <LeftItem key={item.path} to={item.path}>
                <p style={{ fontSize: '1.8rem' }}>{item.name}</p>
              </LeftItem>
            ))}
          </MenuItems>
        </MenuContainer>
      )}
    </>
  );

  const anchor: Anchor = 'top';

  return (
    <>
      <SearchDrawer
        anchor={anchor}
        open={open}
        BackdropComponent={SearchBackdrop}
        onClose={toggleDrawer(anchor, false)}
        keepMounted={true}
        elevation={0}
        transitionDuration={
          small
            ? { appear: 0, enter: 0, exit: 0 }
            : { enter: duration.enteringScreen, exit: duration.leavingScreen }
        }
      >
        {search(anchor)}
      </SearchDrawer>
      <NavGrid>
        <NavLeft>
          {navBarItems.map(item => (
            <LeftItem key={item.path} to={item.path}>
              <p>{item.name}</p>
            </LeftItem>
          ))}
        </NavLeft>
        <NavRight>
          {!small ? (
            <>
              <RightItem>
                <IconButton
                  color="inherit"
                  onClick={toggleDrawer(anchor, true)}
                >
                  <Search />
                </IconButton>
              </RightItem>
              <RightItem>
                {session.isAuthenticated ? (
                  <IconButton color="inherit" onClick={handleClick}>
                    <Avatar
                      alt={session.currentUser.username}
                      src={session.currentUser.avatarUrl}
                      sx={{ width: '2.5rem', height: '2.5rem' }}
                    />
                  </IconButton>
                ) : (
                  <Link to="/login">
                    <LoginRounded sx={{ color: 'text.primary' }} />
                  </Link>
                )}
              </RightItem>
              <RightItem>
                <ThemeSwitch />
              </RightItem>
              <StyledMenu
                anchorEl={anchorEl}
                open={userMenu}
                onClose={handleClose}
              >
                <StyledLink to={'/history'}>
                  <MenuItem autoFocus onClick={handleClose}>
                    {t(messages.history())}
                    {/*<Settings />*/}
                  </MenuItem>
                </StyledLink>
                <StyledLink to={'/user/settings'}>
                  <MenuItem onClick={handleClose}>
                    {t(messages.settings())}
                  </MenuItem>
                </StyledLink>
                <StyledLink to={'/logout'}>
                  <MenuItem onClick={handleClose}>
                    {t(messages.logout())}
                  </MenuItem>
                </StyledLink>
              </StyledMenu>
            </>
          ) : (
            <>
              <RightItem>
                <IconButton
                  color="inherit"
                  onClick={toggleDrawer(anchor, true)}
                >
                  <MenuRounded />
                </IconButton>
              </RightItem>
            </>
          )}
        </NavRight>
        <Icon to="/">
          <FavIcon />
        </Icon>
      </NavGrid>
      <NavMenu />
    </>
  );
}

const StyledLink = styled(Link)`
  text-decoration: none;
  color: inherit;

  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`;

const SearchDrawer = styled(Drawer)<DrawerProps>(({ theme }) => ({
  transition:
    'max-width 0.3s ease 0s, padding 0.3s ease 0s, opacity 0.2s ease 0s',
}));

const SearchBackdrop = styled(Backdrop)<BackdropProps>(({ theme }) => ({
  backgroundColor: theme.palette.primary.main,
  opacity: '0.5 !important',
}));

const SearchContainer = styled(Box)<BoxProps>(({ theme }) => ({
  display: 'grid',
  gridTemplateColumns: 'repeat(6, minmax(0rem, 1fr))',
  gridColumnGap: '1.6rem',
  backgroundColor: theme.palette.background.default,
  padding: '0 4rem',
  width: '100%',
  minHeight: '9.6rem',
  maxHeight: '100%',
  transition:
    'background .5s ease, color .1s ease, fill .5s ease, border .5s ease',

  fontSize: '1.6rem',
  lineHeight: '2.4rem',
  fontWeight: 700,

  '@media (min-width: 768px)': {
    gridTemplateColumns: 'repeat(13, minmax(0rem, 1fr))',
    gridColumnGap: '2rem',
  },

  '@media (min-width: 1270px)': {
    gridTemplateColumns: 'repeat(24, minmax(0rem, 1fr))',
  },
}));

const SearchInputContainer = styled('div')(({ theme }) => ({
  position: 'relative',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '9.6rem',
  animation: '0.32s ease 0s 1 normal forwards running animation-1lawk3v',

  '@media (min-width: 768px)': {
    gridColumn: '2/13',
  },
  '@media (min-width: 1270px)': {
    gridColumn: '8/18',
  },
}));

const SearchInput = styled(CustomInput)`
  padding: 1.2rem 4.8rem;
`;
const SearchInputIcon = styled('div')`
  background: transparent;
  border: none;
  cursor: pointer;
  padding: 0px;
  height: 1.8rem;
  width: 1.8rem;
  outline: none;
  position: absolute;
  top: 50%;
  left: 1.6rem;
  transform: translateY(-50%);
  pointer-events: none;
`;

const PopularContainer = styled('div')(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  gap: '0.8rem',
  padding: '1.2rem 0rem 6.4rem',

  '@media (min-width: 768px)': {
    gridColumn: '2/-2',
  },
  '@media (min-width: 1270px)': {
    gridColumn: '8/-8',
  },
}));

const PopularTitle = styled('div')(({ theme }) => ({
  color: theme.palette.text.secondary,
  marginBottom: '0.8rem',
  fontSize: '1.4rem',
  lineHeight: '2rem',
  fontWeight: 700,
}));

const PopularItem = styled('a')`
  h1,
  h2,
  p {
    margin: 0;
    padding: 0;
    line-height: 1;
  }

  p {
    text-underline-offset: 0.4rem;
    cursor: pointer;
    font-size: 1.6rem;
    line-height: 2.4rem;
    font-weight: 500;

    :hover {
      text-decoration: underline;
    }
  }
`;

const FixedButton = styled('div')`
  position: fixed;
  right: 4rem;
  top: 4rem;
`;
const MenuContainer = styled('div')(({ theme }) => ({
  pointerEvents: 'all',
  zIndex: 1000,
  minHeight: 'max(100vh, 52rem)',
  paddingBottom: '3.2rem',
  top: '0px',
  left: '0px',
  width: '100%',
  transition: 'background 0.3s ease 0s',
  position: 'fixed',
  backgroundColor: theme.palette.background.default,
  maxHeight: '100%',
  overflowY: 'auto',
}));

const MenuHeader = styled('div')`
  margin-bottom: 3.2rem;
  padding: 3.2rem 3.2rem 0px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const MenuIcon = styled('a')`
  height: auto;
  width: 5.4rem;
`;

const MenuItems = styled('div')`
  width: 100%;
  grid-column: 1 / -1;
  display: flex;
  flex-direction: column;
  pointer-events: all;
  padding: 5.6rem 3.2rem;
`;
const RelativeButton = styled('div')`
  transform: translateX(-50%) translateY(-50%);
  background: transparent;
  border: none;
  cursor: pointer;
  padding: 0px;
  height: 1.8rem;
  width: 1.8rem;
  outline: none;
  transition: opacity 0.3s ease 0s;
  z-index: 99999;
  position: relative;
  opacity: 1;
`;

const NavGrid = styled(Box)`
  display: grid;
  grid-template-columns: repeat(6, minmax(0rem, 1fr));
  grid-column-gap: 1.6rem;
  width: 100%;
  margin: 0;
  padding: 0 3.2rem;
  position: absolute;
  z-index: 999;
  pointer-events: none;
  @media (min-width: 768px) {
    grid-template-columns: repeat(13, minmax(0rem, 1fr));
    grid-column-gap: 2rem;
    padding: 0 4rem;
  }
  @media (min-width: 1270px) {
    grid-template-columns: repeat(24, minmax(0rem, 1fr));
    max-width: none;
  }
`;

const NavLeft = styled('div')`
  height: 9.6rem;
  grid-column: 1/-6;
  grid-row: 1;
  pointer-events: all;
  @media (min-width: 768px) {
    display: flex;
  }
  @media (max-width: 768px) {
    display: none;
  }
`;

const LeftItem = styled(Link)(({ theme }) => ({
  marginRight: '3.2rem',
  height: '100%',
  '@media (min-width: 768px)': {
    opacity: 1,
    transform: 'translateX(0rem)',
  },
  textDecoration: 'none',
  fontSize: '1.4rem',
  lineHeight: '2rem',
  fontWeight: 700,
  color: theme.palette.text.primary,
}));

const NavRight = styled('div')`
  grid-column: -5/-1;
  width: 100%;
  margin-top: 4rem;
  display: flex;
  justify-content: flex-end;
  gap: 3.2rem;
  pointer-events: all;
  @media (min-width: 768px) {
    align-items: center;
    margin-top: 0;
  }
`;

const RightItem = styled('div')`
  background: transparent;
  border: none;
  cursor: pointer;
  padding: 0;
  height: 1.8rem;
  width: 1.8rem;
  outline: none;

  svg {
    position: relative;
    background: transparent;
    font-size: 1.8rem;
    text-align: center;
    height: 2.2rem;
    width: 2.2rem;
    -webkit-transition: background 0.2s ease-in-out, color 0.2s ease-in-out;
    transition: background 0.2s ease-in-out, color 0.2s ease-in-out;
  }

  @media (min-width: 768px) {
    grid-column: -2 / -1;
    display: flex;
    align-content: center;
    align-items: center;
    justify-content: center;
  }
`;

const Icon = styled(Link)`
  grid-column: 1/3;
  width: 100%;
  max-width: 8rem;
  pointer-events: all;
  position: absolute;
  top: 3.2rem;
  left: 0;
  color: inherit;

  svg {
    width: 5.8rem;
    height: auto;
  }

  @media (min-width: 768px) {
    position: fixed;
    left: 3.2rem;
    top: 9.6rem;
    z-index: 9999;
    grid-row: 2;
  }
  @media (min-width: 1270px) {
    grid-column: 1/3;
    svg {
      width: 8rem;
    }
  }
`;

const NavMenu = styled('div')`
  display: none;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 40;
  height: 100vh;
  -webkit-transition: opacity 0.3s ease;
  transition: opacity 0.3s ease;
  opacity: 0;
  pointer-events: none;
  -webkit-backdrop-filter: blur(0rem);
  backdrop-filter: blur(0rem);
  background: rgba(152, 152, 152, 0.5);
`;

const StyledMenu = styled((props: MenuProps) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'left',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'left',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    animation: '0.2s ease-in-out 0s 1 normal none running animation-1l98evl',
    position: 'absolute',
    left: '0rem',
    transform: 'translateY(100%)',
    backgroundColor: theme.palette.background.card,
    filter: `drop-shadow( 0px 1.2rem 3rem ${theme.palette.text.secondary})`,
    zIndex: 99,
    width: '100%',
    maxHeight: '28.8rem',
    borderRadius: '0.8rem',
    overflowY: 'auto',
    marginTop: '1.6rem',
    '@media (min-width: 768px)': {
      width: '33.9rem',
    },
    '& .MuiMenu-list': {
      padding: '0.8rem',
      boxSizing: 'border-box',
      display: 'block',
      overflowX: 'hidden',
      '&:last-child': {
        paddingBottom: '0',
      },
    },
    '& .MuiMenuItem-root': {
      fontSize: '1.6rem',
      marginBottom: '0.8rem',
      textTransform: 'capitalize',
      display: 'flex',
      webkitBoxAlign: 'center',
      alignItems: 'center',
      padding: '1.8rem 1.8rem 1.8rem 1.6rem',
      borderRadius: '0.8rem',
      cursor: 'pointer',
      transition: 'background 0.2s ease 0s',
      background: theme.palette.background.card,
      pointEvents: 'none',
      '&:hover': {
        backgroundColor: theme.palette.background.contrastCard,
        color: theme.palette.primary.contrastText,
        '& .large-chip': {
          backgroundColor: theme.palette.primary.contrastText,
          color: theme.palette.primary.contrastText,
          borderColor: theme.palette.primary.contrastText,
        },
      },
      '&:active': {
        backgroundColor: theme.palette.background.contrastCard,
        color: theme.palette.primary.contrastText,
      },
    },
    '& .Mui-selected': {
      backgroundColor: theme.palette.background.contrastCard,
      color: theme.palette.primary.contrastText,
      '& .large-chip': {
        backgroundColor: theme.palette.primary.contrastText,
        color: theme.palette.primary.contrastText,
        borderColor: theme.palette.primary.contrastText,
      },
    },
  },
}));

const SearchResultsContainer = styled('div')(({ theme }) => ({
  display: 'flex',
  flexDirection: 'row',
  gap: '0.8rem',
  flexWrap: 'wrap',
  padding: '1.2rem 0rem 6.4rem',

  '@media (min-width: 768px)': {
    gridColumn: '2/-2',
  },
  '@media (min-width: 1270px)': {
    gridColumn: '8/-8',
  },
}));

const MostPopularMangaLink = styled(Link)(({ theme }) => ({
  fontWeight: 'normal',
  color: theme.palette.primary.main,
  textDecoration: 'underline',
  textUnderlineOffset: '4px',
  textDecorationColor: theme.palette.primary.light,

  '&:hover': {
    color: theme.palette.primary.main,
    textDecorationColor: theme.palette.primary.main,
  },
}));
