import { _t } from '../../../utils/messages';
import { translations } from '../../../locales/translations';

export const messages = {
  home: () => _t(translations.navBar.home),
  discover: () => _t(translations.navBar.discover),
  about: () => _t(translations.navBar.about),
  discord: () => _t(translations.navBar.discord),
  search: () => _t(translations.navBar.search),
  popular: () => _t(translations.navBar.popular),
  profile: () => _t(translations.navBar.profile),
  settings: () => _t(translations.navBar.settings),
  noResult: () => _t(translations.navBar.noResultFound),
  favorites: () => _t(translations.favorites),
  logout: () => _t(translations.navBar.logout),
  history: () => _t(translations.history),
};
