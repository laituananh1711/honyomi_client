import * as React from 'react';
import styled from 'styled-components/macro';
import { Helmet } from 'react-helmet-async';
import { messages } from './messages';
import { useTranslation } from 'react-i18next';
import { Box } from '@mui/material';
import { CustomButton } from '../Button';
import { useTheme } from '@mui/material/styles';
import { Link } from 'react-router-dom';
import { UnderlinedLink } from '../Link';

export function NotFoundPage() {
  const { t } = useTranslation();
  const theme = useTheme();
  return (
    <>
      <Helmet>
        <title>{t(messages.title())}</title>
        <meta name="description" content={t(messages.metadataDes())} />
      </Helmet>
      <Wrapper>
        <Container>
          <Title style={{ color: `${theme.palette.text.secondary}` }}>
            {t(messages.title())}
          </Title>
          <Content>{t(messages.content())}</Content>
          <Note>
            <p>
              {t(messages.note())}
              <UnderlinedLink
                to={'https://discord.com'}
                target="_blank"
                rel="noreferrer"
              >
                {t(messages.discord())}
              </UnderlinedLink>
              .
            </p>
          </Note>
          <ButtonWrapper to="/">
            <CustomButton
              variant={'contained'}
              disableElevation
              btnSize={'large'}
            >
              {t(messages.back())}
            </CustomButton>
          </ButtonWrapper>
        </Container>
      </Wrapper>
    </>
  );
}

const Wrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(6, minmax(0rem, 1fr));
  grid-column-gap: 1.6rem;
  padding: 6.4rem 3.2rem;
  min-height: calc(100vh - 20.3rem);

  @media (min-width: 768px) {
    grid-template-columns: repeat(13, minmax(0rem, 1fr));
    grid-column-gap: 2rem;
    padding: 6.4rem 4rem;
    min-height: calc(100vh - 15.7rem);
  }

  @media (min-width: 1270px) {
    grid-template-columns: repeat(24, minmax(0rem, 1fr));
    max-width: none;
  }
`;

const Container = styled.div`
  grid-column: 1/-1;
  margin-top: 5.4rem;
  width: 100%;

  h1,
  h2,
  p {
    margin: 0;
    padding: 0;
    line-height: 1;
  }

  @media (min-width: 768px) {
    grid-column: 2 / span 7;
    margin-top: 3.2rem;
  }

  @media (min-width: 1270px) {
    grid-column: 5 / span 9;
  }

  @media (min-width: 1560px) {
    grid-column: 5 / span 8;
  }
`;

const Title = styled.p`
  font-size: 1.8rem;
  line-height: 2.4rem;
  font-weight: 700;

  @media (min-width: 375px) {
    font-size: 2.4rem;
  }
`;
const Content = styled.p`
  padding-bottom: 1rem;
  font-size: 3.2rem;
  line-height: 3.2rem;
  font-weight: 700;

  @media (min-width: 375px) {
    font-size: 4.8rem;
    line-height: 4.8rem;
    font-weight: 700;
  }
`;

const Note = styled.div`
  margin-top: 6.4rem;

  p {
    font-size: 1.8rem;
    line-height: 2.6rem;
    font-weight: 400;
    a {
      -webkit-text-decoration: underline;
      text-decoration: underline;
      text-underline-offset: 4px;
      text-decoration-color: var(--color-link-underline);

      :visited,
      :active,
      :hover {
        list-style: none;
        text-decoration: none;
        color: inherit;
      }
    }
  }
`;

const ButtonWrapper = styled(Link)`
  display: block;
  margin-top: 4rem;
  text-decoration: none;
`;
