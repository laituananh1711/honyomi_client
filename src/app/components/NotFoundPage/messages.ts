import { _t } from '../../../utils/messages';
import { translations } from '../../../locales/translations';

export const messages = {
  metadataDes: () => _t(translations.notFound.metadata.description),
  title: () => _t(translations.notFound.title),
  content: () => _t(translations.notFound.content),
  note: () => _t(translations.notFound.note),
  back: () => _t(translations.notFound.back),
  discord: () => _t(translations.notFound.discord),
};
