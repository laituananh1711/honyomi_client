export const rankColors = new Map([
  [1, `linear-gradient(91.61deg, #ffd749 0.14%, #FF7A00 100.79%)`],
  [2, `linear-gradient(91.71deg, #8fffe4 0%, #1D5CFF 100%)`],
  [3, `linear-gradient(91.71deg, #a1ff89 0%, #00BE7A 100%)`],
]);

export const iconColors = new Map([
  [1, '#ffd749'],
  [2, '#8fffe4'],
  [3, '#a1ff89'],
]);

export const IconCrown = (rank: number) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    aria-hidden="true"
    role="img"
    width="2rem"
    height="2rem"
    preserveAspectRatio="xMidYMid meet"
    viewBox="0 0 640 512"
  >
    <path
      d="M528 448H112c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h416c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zm64-320c-26.5 0-48 21.5-48 48c0 7.1 1.6 13.7 4.4 19.8L476 239.2c-15.4 9.2-35.3 4-44.2-11.6L350.3 85C361 76.2 368 63 368 48c0-26.5-21.5-48-48-48s-48 21.5-48 48c0 15 7 28.2 17.7 37l-81.5 142.6c-8.9 15.6-28.9 20.8-44.2 11.6l-72.3-43.4c2.7-6 4.4-12.7 4.4-19.8c0-26.5-21.5-48-48-48S0 149.5 0 176s21.5 48 48 48c2.6 0 5.2-.4 7.7-.8L128 416h384l72.3-192.8c2.5.4 5.1.8 7.7.8c26.5 0 48-21.5 48-48s-21.5-48-48-48z"
      fill={iconColors.get(rank)}
    />
  </svg>
);
