import { Box, useMediaQuery } from '@mui/material';
import styled from 'styled-components';
import IconButton from '@mui/material/IconButton';
import React, { useContext, useEffect, useState } from 'react';
import {
  ArrowBackIosRounded,
  KeyboardTab,
  Settings,
} from '@mui/icons-material';
import { CardChapter } from '../CardChapter';
import { ReaderParams, SideBarContext } from '../../pages/Series/Reader';
import { useParams } from 'react-router-dom';
import { Chapter as ChapterModel } from 'app/models/Chapter';
import { READER, SERIES } from 'app/api/manga/mangaApi.constants';
import { Link } from '../Link';

interface SideBarProps {
  chapters: ChapterModel[];
  mangaTitle: string;
}

export default function SideBar({ chapters, mangaTitle }: SideBarProps) {
  const [expanded, setExpanded] = useState<boolean>(true);
  const sideBar = useContext(SideBarContext);
  const small = useMediaQuery('(max-width:1270px)');
  let { mangaId, number } = useParams<ReaderParams>() ?? null;

  const handleExpand = () => {
    setExpanded(prevState => !prevState);
    sideBar.toggleSideBar();
  };

  useEffect(() => {
    setExpanded(!small);
  }, [small]);

  return (
    <SideContainer>
      <Top>
        <TopItem>
          <IconButton color="inherit">
            <Settings />
          </IconButton>
        </TopItem>
        <TopItem>
          <IconButton
            color="inherit"
            style={{ transform: `scaleX(${expanded ? -1 : 1})` }}
            onClick={handleExpand}
          >
            <KeyboardTab />
          </IconButton>
        </TopItem>
      </Top>
      {/*<Bottom style={expanded ? {} : { display: 'none' }}>*/}
      <Bottom
        style={{
          transform: `translateX(${expanded ? 0 : -150}%)`,
        }}
      >
        <Title to={`/${SERIES}/${mangaId}`}>
          <ArrowBackIosRounded />
          <p>{mangaTitle}</p>
        </Title>
        <ChapterList>
          {chapters.map(chapter => (
            <div key={chapter.id}>
              <Link to={`/${READER}/${mangaId}/${chapter.number}`}>
                <Chapter>
                  <CardChapter
                    number={chapter.number}
                    chapterTitle={chapter.title}
                    focus={number === chapter.number ? true : false}
                  />
                </Chapter>
              </Link>
            </div>
          ))}
        </ChapterList>
      </Bottom>
      <Blur style={{ display: expanded ? '' : 'none' }} />
    </SideContainer>
  );
}

const SideContainer = styled(Box)`
  grid-column: 1/-1;
  grid-row: 2;
  padding: 0 3.2rem;
  z-index: 10;
  @media (min-width: 768px) {
    grid-column: 1/5;
    padding: 0 4rem;
    display: grid;
    grid-template-rows: 3.2rem 7.4rem 1fr;
  }
  @media (min-width: 1270px) {
    grid-column: 1/5;
    grid-row: 1;
    padding: 0rem;
    grid-template-rows: 22.6rem 6rem 1fr;
  }
  @media (min-width: 1560px) {
    grid-column: 1/5;
    max-width: 30rem;
  }
`;

const Top = styled.div`
  display: flex;
  margin-bottom: 4rem;
  position: relative;
  @media (min-width: 768px) {
    grid-column: 1;
    grid-row: 2/2;
    margin-bottom: 4.8rem;
  }
`;

const TopItem = styled.div`
  background: transparent;
  border: none;
  cursor: pointer;
  padding: 0;
  height: 1.8rem;
  width: 1.8rem;
  outline: none;
  margin-right: 4.6rem;
  transform: translateY(-8px) translateX(-8px);

  svg {
    position: relative;
    background: transparent;
    font-size: 1.8rem;
    text-align: center;
    height: 2.2rem;
    width: 2.2rem;
    -webkit-transition: background 0.2s ease-in-out, color 0.2s ease-in-out;
    transition: background 0.2s ease-in-out, color 0.2s ease-in-out;
  }
`;
const Bottom = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  min-width: 0px;
  transform: translateX(0px);
  transition: transform 0.4s ease 0s;
  @media (min-width: 768px) {
    height: calc(100vh - 3.2rem);
    position: relative;
    grid-area: 3 / 1 / -1 / 1;
  }
  //@media (max-width: 1270px) {
  //  display: none;
  //}
`;
const Title = styled(Link)(({ theme }) => ({
  color: theme.palette.text.primary,
  marginBottom: '20px',
  alignItems: 'flex-start',
  display: 'flex',
  zIndex: 99,
  position: 'relative',
  textDecoration: 'none',
  '&:hover': {
    svg: {
      marginLeft: '-1rem',
    },
  },

  svg: {
    textAlign: 'center',
    height: '1.8rem',
    width: '1.8rem',
    position: 'absolute',
    marginTop: '0.4rem',
    transition: 'margin-left 0.3s ease',
  },

  p: {
    margin: 0,
    padding: 0,
    fontSize: '1.8rem',
    lineHeight: '2.2rem',
    fontWeight: 700,
    marginLeft: '20px',
  },
}));

const ChapterList = styled.div`
  height: calc(100vh - 29.6rem);
  //overflow: hidden;
  overflow-y: scroll;

  ::-webkit-scrollbar {
    display: none;
  }

  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none;
  @media (min-width: 768px) {
    height: calc(100vh - 40rem);
  }
`;

const Chapter = styled.div`
  margin-bottom: 20px;
`;

const Blur = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(100, 100, 100, 0.5);
  backdrop-filter: blur(0.8rem);
  z-index: -1;
  opacity: 1;
  transition: opacity 0.5s ease 0s;
  @media (min-width: 768px) {
    display: none;
  }
`;
