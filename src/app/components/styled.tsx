import { Card, CardContent, CardProps, styled } from '@mui/material';
import React from 'react';

export const PageContainer = styled('div')`
  display: grid;
  grid-template-columns: repeat(6, minmax(0rem, 1fr));
  grid-column-gap: 1.6rem;
  padding: 6.4rem 3.2rem;

  @media (min-width: 768px) {
    grid-template-columns: repeat(13, minmax(0rem, 1fr));
    grid-column-gap: 2rem;
  }

  @media (min-width: 1270px) {
    grid-template-columns: repeat(24, minmax(0rem, 1fr));
    max-width: none;
  }

  @media (min-width: 768px) {
    padding: 6.4rem 4rem;
  }
`;

export const PageTitle = styled('p')`
  font-size: 2.4rem;
  line-height: 4rem;
  font-weight: 700;
  grid-row: 1;
  grid-column: 1/-1;
  margin-top: 2rem;

  @media (min-width: 375px) {
    font-size: 4rem;
  }

  @media (min-width: 768px) {
    margin-top: 0;
    grid-column: 2/-1;
  }

  @media (min-width: 1270px) {
    margin-top: 0;
    grid-column: 5/-1;
  }
`;

export const PageContent = styled('div')`
  grid-column: 1/-1;

  @media (min-width: 768px) {
    grid-column: 2/-1;
  }

  @media (min-width: 1270px) {
    grid-column: 5/-1;
  }
`;

export const LoginRequiredText = styled('p')(({ theme }) => ({
  fontSize: '2rem',
  textAlign: 'center',
  maxWidth: '60rem',
  fontWeight: 'lighter',
  color: theme.palette.text.primary,
  textUnderlineOffset: '4px',
}));

export const StyledCard = styled((prop: CardProps) => (
  <Card variant="outlined" {...prop} />
))(({ theme }) => ({
  backgroundColor: theme.palette.background.card,

  '& .MuiCardActions-root,.MuiCardContent-root': {
    padding: '2rem',
  },
}));

export const StyledCardTitle = styled('h2')`
  font-size: 2.5rem;
  font-weight: 600;
  margin-top: 0;
`;

export const StyledCardContent = styled(CardContent)`
  display: flex;
  flex-direction: row;
`;
