import { styled } from '@mui/material/styles';
import Button, { ButtonProps } from '@mui/material/Button';

interface CustomButtonProps extends ButtonProps {
  btnSize: 'large' | 'small';
}

export function CustomButton({
  children,
  btnSize,
  ...props
}: CustomButtonProps) {
  if (btnSize === 'large') {
    return <LargeButton {...props}>{children}</LargeButton>;
  } else if (btnSize === 'small') {
    return <SmallButton {...props}>{children}</SmallButton>;
  }
  return <></>;
}

export const LargeButton = styled(Button)(({ theme }) => ({
  lineHeight: '2.4rem',
  fontSize: '1.6rem',
  fontWeight: 500,
  borderRadius: '10rem',
  padding: '1.6rem 3.2rem',
  gap: '0.8rem',
  transition: 'color 0.2s ease, background 0.35s ease',
  color: theme.palette.primary.contrastText,
  backgroundColor: theme.palette.primary.main,
  textTransform: 'capitalize',
  '&:hover': {
    backgroundColor: theme.palette.primary.light,
  },
}));

export const SmallButton = styled(Button)(({ theme }) => ({
  lineHeight: '2.4rem',
  fontSize: '1.6rem',
  fontWeight: 500,
  borderRadius: '10rem',
  padding: '0.8rem 2.4rem',
  gap: '0.8rem',
  transition: 'color 0.2s ease, background 0.35s ease',
  color: theme.palette.primary.contrastText,
  backgroundColor: theme.palette.primary.main,
  textTransform: 'capitalize',
  '&:hover': {
    backgroundColor: theme.palette.primary.light,
  },
  '&:disabled': {
    backgroundColor: theme.palette.primary.light,
  },
}));

export const StyledButton = styled(Button)(({ theme }) => ({
  textTransform: 'none',
}));
