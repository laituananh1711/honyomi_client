import { Circle } from '@mui/icons-material';
import { Menu, MenuItem, MenuProps } from '@mui/material';
import { styled } from '@mui/material/styles';
import PopupState, {
  bindMenu,
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import React, { MouseEvent, ReactNode } from 'react';
import { LargeChip } from '../CustomChip';
export interface AppMenuOption {
  label: string;
  value: string | number;
  prefix?: ReactNode;
  suffix?: ReactNode;
}

export interface AppMenuProp {
  title: string;
  options: AppMenuOption[];
  selected: AppMenuOption[];
  onMenuItemClick: (event: MouseEvent, option: AppMenuOption) => void;
}

const AppMenu: React.FC<AppMenuProp> = (prop): JSX.Element => {
  return (
    <PopupState variant="popover" popupId="popup-menu">
      {popupState => (
        <>
          <LargeChip
            isActived={bindPopover(popupState).open}
            hasArrowIcon={false}
            labelText={prop.title}
            label={
              <>
                {prop.title}
                {prop.selected.length > 0 && (
                  <>
                    <Circle sx={{ fontSize: '0.5rem', margin: '0.5rem' }} />
                    {prop.selected.length}
                  </>
                )}
              </>
            }
            {...bindTrigger(popupState)}
          />
          <StyledMenu {...bindMenu(popupState)}>
            {prop.options.map(option => (
              <MenuItem
                key={option.value}
                value={option.value}
                autoFocus
                onClick={event => prop.onMenuItemClick(event, option)}
                selected={
                  prop.selected.findIndex(
                    item => item.label === option.label,
                  ) >= 0
                }
              >
                {option.prefix}
                <StyledMenuItemContent>
                  <span>{option.label}</span>
                  {option.suffix}
                </StyledMenuItemContent>
              </MenuItem>
            ))}
          </StyledMenu>
        </>
      )}
    </PopupState>
  );
};

const StyledMenu = styled((props: MenuProps) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'left',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'left',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    animation: '0.2s ease-in-out 0s 1 normal none running animation-1l98evl',
    position: 'absolute',
    left: '0rem',
    transform: 'translateY(100%)',
    backgroundColor: theme.palette.background.card,
    filter: `drop-shadow( 0px 1.2rem 3rem ${theme.palette.text.secondary})`,
    zIndex: 99,
    width: '100%',
    maxHeight: '28.8rem',
    borderRadius: '0.8rem',
    overflowY: 'auto',
    marginTop: '1.6rem',
    '@media (min-width: 768px)': {
      width: '33.9rem',
    },
    '& .MuiMenu-list': {
      padding: '0.8rem',
      boxSizing: 'border-box',
      display: 'block',
      overflowX: 'hidden',
      '&:last-child': {
        paddingBottom: '0',
      },
    },
    '& .MuiMenuItem-root': {
      fontSize: '1.6rem',
      marginBottom: '0.8rem',
      textTransform: 'capitalize',
      display: 'flex',
      webkitBoxAlign: 'center',
      alignItems: 'center',
      padding: '1.8rem 1.8rem 1.8rem 1.6rem',
      borderRadius: '0.8rem',
      cursor: 'pointer',
      transition: 'background 0.2s ease 0s',
      background: theme.palette.background.card,
      pointEvents: 'none',
      '&:hover': {
        backgroundColor: theme.palette.background.contrastCard,
        color: theme.palette.primary.contrastText,
        '& .large-chip': {
          backgroundColor: theme.palette.primary.contrastText,
          color: theme.palette.primary.contrastText,
          borderColor: theme.palette.primary.contrastText,
        },
      },
      '&:active': {
        backgroundColor: theme.palette.background.contrastCard,
        color: theme.palette.primary.contrastText,
      },
    },
    '& .Mui-selected': {
      backgroundColor: theme.palette.background.contrastCard,
      color: theme.palette.primary.contrastText,
      '& .large-chip': {
        backgroundColor: theme.palette.primary.contrastText,
        color: theme.palette.primary.contrastText,
        borderColor: theme.palette.primary.contrastText,
      },
    },
  },
}));

const StyledMenuItemContent = styled('div')`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

export { AppMenu, StyledMenu, StyledMenuItemContent };
