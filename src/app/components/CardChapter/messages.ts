import { _t } from '../../../utils/messages';
import { translations } from '../../../locales/translations';

export const messages = {
  chapter: () => _t(translations.chapterFeature.chapter),
};
