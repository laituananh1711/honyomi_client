import { Card, CardActionArea } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';
import styled from 'styled-components/macro';

const WIDTH = '100%';
const MIN_HEIGHT = '9.3rem';

interface CardChapterProps {
  number: string;
  chapterTitle: string;
  focus?: boolean;
}

export function CardChapter({ number, chapterTitle, focus }: CardChapterProps) {
  const { t } = useTranslation();

  return (
    <Card
      sx={
        focus === true
          ? {
              width: WIDTH,
              height: MIN_HEIGHT,
              minHeight: MIN_HEIGHT,
              boxShadow: 'none',
              bgcolor: 'background.contrastCard',
              color: 'primary.contrastText',
            }
          : {
              width: WIDTH,
              height: MIN_HEIGHT,
              minHeight: MIN_HEIGHT,
              boxShadow: 'none',
              bgcolor: 'background.card',
              color: 'text.primary',
              '&:hover': {
                bgcolor: 'background.contrastCard',
                color: 'primary.contrastText',
              },
            }
      }
    >
      <CardActionArea>
        <CardContent>
          <p className={'title'}>
            {t(messages.chapter())} {number}
          </p>
          <p className={'sub-title'}>{chapterTitle}</p>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

const CardContent = styled.div`
  width: 100%;
  height: 9.3rem;
  display: flex;
  justify-content: center;
  flex-direction: column;
  transition: background 0.2s ease 0s;
  border-radius: 0.4rem;
  margin-bottom: 2rem;
  padding: 2.4rem 1.6rem;
  min-height: 9.3rem;

  h1,
  h2,
  p {
    margin: 0;
    padding: 0;
    line-height: 1;
  }

  .title {
    font-size: 1.8rem;
    line-height: 2.2rem;
    font-weight: 700;
    margin-bottom: 0.5rem;
    transition: color 0.2s ease;
  }
  .sub-title {
    font-size: 1.4rem;
    line-height: 2rem;
    font-weight: 500;
    transition: color 0.2s ease;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    opacity: 80%;
  }
`;
