import { KeyboardArrowDown, KeyboardArrowUp } from '@mui/icons-material';
import { Chip, ChipProps, styled } from '@mui/material';
import React from 'react';

interface LargeChipProps extends ChipProps {
  isActived: boolean;
  hasArrowIcon: boolean;
  labelText: string;
  inArrayChip?: boolean;
}

export const CustomChip = styled(Chip)<ChipProps>(({ theme }) => ({
  color: theme.palette.primary.main,
  backgroundColor: theme.palette.background.chip,
  '&:hover': {
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.background.contrastCard,
    cursor: 'pointer',
  },
  fontSize: '1.2rem',
  lineHeight: '1.6rem',
  fontWeight: 400,
  padding: '0.4rem 1.2rem 0.6rem',
  height: '2.8rem',
  borderRadius: '20rem',
}));

export const LargeChip = styled(
  ({
    isActived,
    hasArrowIcon,
    labelText,
    inArrayChip,
    ...props
  }: LargeChipProps) => (
    <Chip
      sx={
        isActived === true
          ? {
              bgcolor: 'background.contrastCard',
              color: 'primary.contrastText',
              '& .custom-icon': {
                color: 'primary.contrastText',
              },
            }
          : {
              bgcolor: 'background.card',
              color: 'primary.main',
              '&:hover': {
                bgcolor: 'background.contrastCard',
                color: 'primary.contrastText',
                '& .custom-icon': {
                  color: 'primary.contrastText',
                },
              },
            }
      }
      label={
        hasArrowIcon === true ? (
          isActived === true ? (
            <React.Fragment>
              {labelText} <ArrowUpIcon />
            </React.Fragment>
          ) : (
            <React.Fragment>
              {labelText} <ArrowDownIcon />
            </React.Fragment>
          )
        ) : (
          labelText
        )
      }
      {...props}
    />
  ),
)(({ theme }) => ({
  fontSize: '1.6rem',
  lineHeight: '2.4rem',
  fontWeight: 400,
  height: '4rem',
  cursor: 'pointer',
  whiteSpace: 'nowrap',
  borderRadius: '10rem',
  padding: '0.8rem 2rem',
  transition: 'color 0.2s ease,background 0.35s ease',
  display: '-webkit-inline-box',
  webkitAlignItems: 'center',
  webkitBoxAlign: 'center',
  msFlexAlign: 'center',
  alignItems: 'center',
  webkitBoxPack: 'center',
  msFlexPack: 'center',
  webkitJustifyContent: 'center',
  justifyContent: 'center',
  gap: '0.8rem',
}));

function ArrowDownIcon() {
  return (
    <KeyboardArrowDown
      sx={{ color: 'primary.main', marginRight: -1 }}
      className="custom-icon"
      fontSize="large"
    />
  );
}

function ArrowUpIcon() {
  return (
    <KeyboardArrowUp
      sx={{ color: 'primary.main', marginRight: -1 }}
      className="custom-icon"
      fontSize="large"
    />
  );
}
