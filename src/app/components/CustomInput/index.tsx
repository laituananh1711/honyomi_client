import { InputBase } from '@mui/material';
import { styled } from '@mui/system';

const CustomInput = styled('input')(({ theme }) => ({
  backgroundColor: theme.palette.background.card,
  color: theme.palette.text.primary,
  borderRadius: '99rem',
  border: '0px',
  lineHeight: '2.4rem',
  width: '100%',
  padding: '1.2rem 2.4rem',
  fontSize: '1.6rem',
  transition:
    'background .2s ease, color .1s ease, fill .2s ease, border .2s ease',

  ':focus': {
    outline: '0px',
  },
}));

const CustomMaterialInput = styled(InputBase)(({ theme }) => ({
  backgroundColor: theme.palette.background.card,
  color: theme.palette.text.primary,
  borderRadius: '99rem',
  border: '0px',
  lineHeight: '2.4rem',
  width: '100%',
  padding: '1.2rem 2.4rem',
  fontSize: '1.6rem',
  transition:
    'background .2s ease, color .1s ease, fill .2s ease, border .2s ease',

  ':focus': {
    outline: '0px',
  },
}));

export const CustomTextarea = styled(InputBase)(({ theme }) => ({
  backgroundColor: theme.palette.background.card,
  color: theme.palette.text.primary,
  border: '0px',
  lineHeight: '2.4rem',
  width: '100%',
  padding: '1.2rem 2.4rem',
  fontSize: '1.6rem',
  transition:
    'background .2s ease, color .1s ease, fill .2s ease, border .2s ease',

  ':focus': {
    outline: '0px',
  },
}));

export default CustomInput;
export { CustomMaterialInput };
