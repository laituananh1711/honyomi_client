import { Box, styled, Typography } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { IconCrown, rankColors } from '../Common/IconCrown';
import { messages } from './messages';

const TITLE_COLOR = '#1a1a1a';

interface CardPopularSwipperProps {
  title: string;
  rank: number;
  imageUrl: string;
}

export function CardPopularSwipper({
  title,
  rank,
  imageUrl,
}: CardPopularSwipperProps) {
  const { t } = useTranslation();
  const IsInTop3 = rank <= 3;
  return (
    <SwiperSlideWrapper sx={{ backgroundImage: `url(${imageUrl})` }}>
      <SlideBackground
        className="bg-color-container"
        sx={
          IsInTop3 === false
            ? {
                bgcolor: 'background.card',
              }
            : {
                background: rankColors.get(rank),
              }
        }
      ></SlideBackground>
      <SlideTitle
        sx={IsInTop3 ? { color: TITLE_COLOR } : { color: 'text.primary' }}
      >
        {title}
      </SlideTitle>
      <SlideSubtitle
        sx={IsInTop3 ? { color: TITLE_COLOR } : { color: 'text.secondary' }}
      >
        {t(messages.mostPopular())}
      </SlideSubtitle>
      {IsInTop3 ? <div>{IconCrown(rank)}</div> : null}
    </SwiperSlideWrapper>
  );
}

const SwiperSlideWrapper = styled(Box)`
  position: relative;
  cursor: pointer;
  border-radius: 0.8rem;
  -webkit-background-size: cover;
  background-size: cover;
  -webkit-background-position: center 16%;
  background-position: center 16%;
  transition: background 0.3s ease-in-out;
  width: 31rem;
  height: 14rem;
  gap: 1rem;
  padding: 2.4rem 2.4rem 1.6rem 1.6rem;
  border-radius: 0.8rem;
  display: grid;
  grid-template-columns: 1fr 1.8rem;
  grid-template-rows: 1fr 2rem;

  *:not(.bg-color-container) {
    z-index: 2;
    pointer-events: none;
  }

  @media (min-width: 375px) {
    .css-slide-content {
      width: 32.5rem;
      height: 16rem;
    }
  }
`;

const SlideBackground = styled(Box)`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: 1;
  border-radius: 0.7rem;
  transition: opacity 0.35s ease-in-out;
  opacity: 0.8;
  -webkit-transform: translateZ(0);
  -moz-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);

  :hover {
    transition: opacity 0.5s ease-in-out;
    opacity: 0.8;
  }

  @media (min-width: 1030px) {
    opacity: 0.95;
  }
`;

const SlideTitle = styled(Typography)`
  font-size: 1.8rem;
  line-height: 2.4rem;
  font-weight: 700;
  grid-area: 1/1/2/2;

  @media (min-width: 375px) {
    font-size: 2.4rem;
  }
`;

const SlideSubtitle = styled(Typography)`
  font-size: 1.4rem;
  line-height: 2rem;
  font-weight: 700;
  opacity: 0.5;
  grid-area: 2/1/3/3;
`;
