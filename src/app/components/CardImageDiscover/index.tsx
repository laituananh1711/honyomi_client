import { Typography } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { CardImageProps, CustomChipRead } from '../CardImage';
import { messages } from './messages';
import './style.scss';

export function CardImageDiscover({
  title,
  subtitle,
  imageUrl,
  path,
  style,
  showMoreInfo = true,
}: CardImageProps) {
  const { t } = useTranslation();
  return (
    <div style={style} className="card-image-discover">
      <Link to={path} className="css-link">
        <div className="css-image-discover-wrapper">
          <img
            className="css-image-discover-card"
            src={imageUrl}
            alt={'Cover art for ' + title}
            loading="lazy"
          />

          {showMoreInfo && (
            <CustomChipRead
              variant="filled"
              label={t(messages.moreInfo())}
              className="css-chip-read"
            />
          )}
        </div>

        <div className="css-info-serie-wrapper">
          <Typography
            className="css-mange-name-serie"
            sx={{ color: 'text.primary' }}
          >
            {title}
          </Typography>
          <Typography className="css-chapter-more-info">{subtitle}</Typography>
        </div>
      </Link>
    </div>
  );
}
