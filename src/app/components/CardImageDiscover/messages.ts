import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  moreInfo: () => _t(translations.mangaFeature.moreInfo),
};
