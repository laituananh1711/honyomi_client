import { _t } from '../../../utils/messages';
import { translations } from '../../../locales/translations';

export const messages = {
  recommend: () => _t(translations.mangaFeature.recommend),
  ongoing: () => _t(translations.mangaFeature.ongoing),
  checkItOut: () => _t(translations.mangaFeature.checkItOut),
};
