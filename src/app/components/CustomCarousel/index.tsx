import {
  Box,
  BoxProps,
  CircularProgress,
  IconButton,
  IconButtonProps,
  Stack,
  styled,
  Typography,
} from '@mui/material';
import React from 'react';
import { Link } from 'react-router-dom';
import { Carousel } from 'react-responsive-carousel';
import { Circle, NavigateBefore, NavigateNext } from '@mui/icons-material';
import { CustomChip } from '../CustomChip';
import { TFunction } from 'i18next';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';
import { RecommendedManga } from 'app/models/RecommendManga';
import { CustomButton } from '../Button';
import { RenderError } from '../Error';
import { CATEGORY, DISCOVER, SERIES } from 'app/api/manga/mangaApi.constants';

interface CarouselItemProps {
  slide: RecommendedManga;
  t: TFunction;
}

interface CarouselItemsListProps {
  slides: RecommendedManga[];
  t: TFunction;
}

const CustomIconButton = styled(IconButton)<IconButtonProps>(({ theme }) => ({
  color: theme.palette.primary.main,
  '&:hover': {
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.background.contrastCard,
    cursor: 'pointer',
  },
}));

const BackgroundImage = styled(Box)<BoxProps>(({ theme }) => ({
  position: 'absolute',
  right: 0,
  bottom: 0,
  width: '100%',
  height: '100%',
  '&::before': {
    content: "''",
    position: 'absolute',
    width: '100%',
    height: 'calc(100% + 0.1rem)',
    background: `linear-gradient(180deg,rgba(244, 244, 244, 0)  0%, ${theme.palette.background.card} 100%)`,
    zIndex: 10,
  },

  '@media (min-width: 768px)': {
    maxWidth: '60%',
    maxHeight: '100%',
  },

  '@media (min-width: 1030px)': {
    '&::before': {
      content: 'none',
    },
  },

  '@media (min-width: 1560px)': {
    right: '20%',
    top: '8rem',
  },

  '@media (min-width: 2240px)': {
    right: '30%',
  },
}));

const carouselItem = ({ slide, t }: CarouselItemProps) => (
  <div className="css-carousel-wrapper" key={slide.id}>
    <Box
      className="carousel-root"
      tabIndex={0}
      sx={{ bgcolor: 'background.card' }}
    >
      <div className="carousel carousel-slider" style={{ width: '100%' }}>
        <div className="slider-wrapper axis-horizontal">
          <ul className="slider animated">
            <li className="slide">
              <div className="css-slide-wrapper">
                <Box className="css-slide-root" sx={{ color: 'text.primary' }}>
                  <Typography className="css-recommended">
                    {t(messages.recommend())}
                  </Typography>

                  <Typography color={slide.titleColor} className="css-title">
                    {slide.manga.title}
                  </Typography>

                  <div className="css-genre">
                    <Stack direction="row" spacing={1}>
                      {slide.manga.categories.map((category, index) => (
                        <Link
                          key={index}
                          to={`/${DISCOVER}?${CATEGORY}=${category.id}`}
                          className="css-genre-link"
                        >
                          <CustomChip
                            size="small"
                            variant="filled"
                            label={category.title}
                          />
                        </Link>
                      ))}
                    </Stack>
                  </div>

                  <Typography className="css-description">
                    {slide.description}
                  </Typography>
                  <div className="css-check-it-out">
                    <Link
                      href={`/${SERIES}/${slide.manga.id}`}
                      to={`/${SERIES}/${slide.manga.id}`}
                      className="css-genre-link"
                    >
                      <CustomButton btnSize={'small'}>
                        {t(messages.checkItOut())}
                      </CustomButton>
                    </Link>
                    <div className="css-ongoing-wrapper">
                      <Circle className="css-ongoing-circle" />
                      <Typography className="css-ongoing">
                        {t(messages.ongoing())}
                      </Typography>
                    </div>
                  </div>
                </Box>

                <BackgroundImage>
                  <img
                    className="css-imagebg"
                    src={slide.imageUrl}
                    alt={slide.manga.title}
                  />
                </BackgroundImage>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </Box>
  </div>
);

const carouselItemsList = ({ slides, t }: CarouselItemsListProps) => (
  <div>
    {slides.map((slide: RecommendedManga) => carouselItem({ slide, t }))}
  </div>
);

interface CustomCarouselProps {
  carouselSlides: RecommendedManga[];
  loading: boolean;
  error: Error | undefined;
}

export function CustomCarousel({
  carouselSlides,
  loading,
  error,
}: CustomCarouselProps) {
  const [currSlide, setCurrSlide] = React.useState<number>(0);
  const { t } = useTranslation();
  const slides = carouselSlides;

  const children = React.useMemo(
    () => carouselItemsList({ slides, t }),
    [slides, t],
  );

  const handleNextSlide = () => {
    currSlide === slides.length - 1
      ? setCurrSlide(0)
      : setCurrSlide(currSlide + 1);
  };
  const handlePrevSlide = () => {
    currSlide === 0
      ? setCurrSlide(slides.length - 1)
      : setCurrSlide(currSlide - 1);
  };

  const updateCurrSlide = (index: number) => {
    const currentSlide = currSlide;

    if (currentSlide !== index) {
      setCurrSlide(index);
    }
  };

  return (
    <div>
      <Carousel
        interval={8000}
        showArrows={false}
        showStatus={false}
        showIndicators={false}
        showThumbs={false}
        thumbWidth={100}
        centerSlidePercentage={80}
        infiniteLoop={true}
        useKeyboardArrows={false}
        autoPlay={true}
        dynamicHeight={false}
        emulateTouch={true}
        autoFocus={false}
        transitionTime={350}
        preventMovementUntilSwipeScrollTolerance={false}
        verticalSwipe="standard"
        selectedItem={currSlide}
        onChange={updateCurrSlide}
      >
        {children.props.children}
      </Carousel>
      <div className="css-footer-wrapper">
        <div className="css-footer">
          <div className="css-items-list">
            <Stack direction="row" spacing={1}>
              {loading ? (
                <CircularProgress />
              ) : error ? (
                <RenderError error={error} />
              ) : (
                slides.map((slide, index) => (
                  <li
                    style={{ listStyle: 'none' }}
                    key={slide.id}
                    value={index}
                    onClick={e => updateCurrSlide(index)}
                  >
                    <Box
                      sx={{ bgcolor: 'text.primary' }}
                      className={
                        slides[currSlide].id === slide.id
                          ? 'css-selected-item'
                          : 'css-item'
                      }
                    />
                  </li>
                ))
              )}
            </Stack>
          </div>
          <div className="css-control-wrapper">
            <CustomIconButton
              className="css-control-button"
              size="large"
              onClick={handlePrevSlide}
            >
              <NavigateBefore className="css-button-navigate" />
            </CustomIconButton>
            <CustomIconButton
              className="css-control-button"
              onClick={handleNextSlide}
            >
              <NavigateNext className="css-button-navigate" />
            </CustomIconButton>
          </div>
        </div>
      </div>
    </div>
  );
}
