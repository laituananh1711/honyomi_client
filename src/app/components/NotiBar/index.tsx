import styled from 'styled-components';
import { Box } from '@mui/material';

export default function NotiBar() {
  return <Bar sx={{ backgroundColor: 'background.contrastCard' }} />;
}

const Bar = styled(Box)`
  position: relative;
  height: 1rem;
  min-height: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  transition: min-height 0.2s ease-out;
  width: 100%;
  z-index: 999;
`;
