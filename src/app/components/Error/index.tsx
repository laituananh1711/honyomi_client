import { Alert, Snackbar } from '@mui/material';
import React, { useEffect } from 'react';

interface RenderErrorProps {
  error: Error;
}

export function RenderError({ error }: RenderErrorProps) {
  const [open, setOpen] = React.useState(true);
  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  useEffect(() => {
    let timer = setTimeout(() => setOpen(false), 5000);
    return () => {
      clearTimeout(timer);
    };
  }, []);
  return (
    <Snackbar open={open}>
      <Alert
        onClose={handleClose}
        severity="error"
        sx={{ width: '100%', fontSize: '1.5rem' }}
      >
        {error.message}
      </Alert>
    </Snackbar>
  );
}
