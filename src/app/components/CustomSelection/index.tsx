import { LargeChip } from '../CustomChip';
import { Menu, MenuItem, MenuProps } from '@mui/material';
import React, { ReactNode } from 'react';
import styled from 'styled-components/macro';

interface Prop {
  value: any;
  selection: ReactNode[];
  callback: () => void;
}

export const CustomSelection: React.FC<Prop> = ({
  value,
  selection,
  callback,
  children,
}) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    callback();
    setAnchorEl(null);
  };

  return (
    <div>
      <LargeChip
        isActived={open}
        hasArrowIcon={true}
        labelText={value}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        variant="filled"
      />
      <StyledMenu anchorEl={anchorEl} open={open} onClose={handleClose}>
        {selection.map((v, i) => {
          return (
            <MenuItem autoFocus onClick={handleClose} key={i}>
              {v}
            </MenuItem>
          );
        })}
      </StyledMenu>
    </div>
  );
};

const StyledMenu = styled((props: MenuProps) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'left',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'left',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    animation: '0.2s ease-in-out 0s 1 normal none running animation-1l98evl',
    position: 'absolute',
    left: '0rem',
    transform: 'translateY(100%)',
    backgroundColor: theme.palette.background.card,
    filter: `drop-shadow( 0px 1.2rem 3rem ${theme.palette.text.secondary})`,
    zIndex: 99,
    width: '100%',
    maxHeight: '28.8rem',
    borderRadius: '0.8rem',
    overflowY: 'auto',
    marginTop: '1.6rem',
    '@media (min-width: 768px)': {
      width: '33.9rem',
    },
    '& .MuiMenu-list': {
      padding: '0.8rem',
      boxSizing: 'border-box',
      display: 'block',
      overflowX: 'hidden',
      '&:last-child': {
        paddingBottom: '0',
      },
    },
    '& .MuiMenuItem-root': {
      fontSize: '1.6rem',
      marginBottom: '0.8rem',
      textTransform: 'capitalize',
      display: 'flex',
      webkitBoxAlign: 'center',
      alignItems: 'center',
      padding: '1.8rem 1.8rem 1.8rem 1.6rem',
      borderRadius: '0.8rem',
      cursor: 'pointer',
      transition: 'background 0.2s ease 0s',
      background: theme.palette.background.card,
      pointEvents: 'none',
      '&:hover': {
        backgroundColor: theme.palette.background.contrastCard,
        color: theme.palette.primary.contrastText,
        '& .large-chip': {
          backgroundColor: theme.palette.primary.contrastText,
          color: theme.palette.primary.contrastText,
          borderColor: theme.palette.primary.contrastText,
        },
      },
      '&:active': {
        backgroundColor: theme.palette.background.contrastCard,
        color: theme.palette.primary.contrastText,
      },
    },
    '& .Mui-selected': {
      backgroundColor: theme.palette.background.contrastCard,
      color: theme.palette.primary.contrastText,
      '& .large-chip': {
        backgroundColor: theme.palette.primary.contrastText,
        color: theme.palette.primary.contrastText,
        borderColor: theme.palette.primary.contrastText,
      },
    },
  },
}));
