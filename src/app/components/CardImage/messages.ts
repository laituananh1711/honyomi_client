import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  read: () => _t(translations.mangaFeature.read),
};
