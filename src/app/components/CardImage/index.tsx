import { Chip, ChipProps, styled, Typography } from '@mui/material';
import { CSSProperties } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { messages } from './messages';
import './style.scss';

export interface CardImageProps {
  title: string;
  subtitle?: string;
  imageUrl: string;
  path: string;
  style?: CSSProperties;
  showMoreInfo?: boolean;
}

export const CustomChipRead = styled(Chip)<ChipProps>(({ theme }) => ({
  color: theme.palette.primary.main,
  backgroundColor: theme.palette.background.card,
  '&:hover': {
    backgroundColor: theme.palette.background.contrastCard,
    color: theme.palette.primary.contrastText,
  },
  width: '23rem',
  height: '4rem',
}));

const placeholderImgUrl =
  'https://www.unfe.org/wp-content/uploads/2019/04/SM-placeholder.png';

export function CardImage({ title, subtitle, imageUrl, path }: CardImageProps) {
  const { t } = useTranslation();
  return (
    <div className={'CardImage'}>
      <Link to={path} className="css-link">
        <div className="css-image-wrapper">
          <object data={imageUrl} type="image/*">
            <img
              className="css-image-card"
              src={placeholderImgUrl}
              alt={'Cover art for ' + title}
              loading="lazy"
            />
          </object>

          <CustomChipRead
            variant="filled"
            label={t(messages.read()) + ' ' + subtitle}
            className="css-chip-read"
          />
        </div>

        <div className="css-info-wrapper">
          <div className="css-mange-name-wrapper">
            <Typography
              className="css-mange-name"
              sx={{ color: 'text.primary' }}
            >
              {title}
            </Typography>
          </div>
          <Typography
            className="css-chapter-value"
            sx={{ color: 'text.primary' }}
          >
            {subtitle}
          </Typography>
        </div>
      </Link>
    </div>
  );
}
