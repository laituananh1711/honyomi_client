import { Link as LinkRouterDom } from 'react-router-dom';
import styled from 'styled-components';

export const Link = styled(LinkRouterDom)`
  text-decoration: none;
`;

export const UnderlinedLink = styled(LinkRouterDom)(({ theme }) => ({
  color: theme.palette.text.primary,
}));
