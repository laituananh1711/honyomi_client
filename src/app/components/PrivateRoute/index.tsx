import { User } from 'app/models/User';
import React from 'react';
import { useEffect } from 'react';
import { Redirect, Route, RouteProps, useLocation } from 'react-router';

export type ProtectedRouteProps = {
  isAuthenticated: boolean;
  authenticationPath: string;
  redirectPath: string;
  currentUser: User;
  setRedirectPath: (path: string) => void;
} & RouteProps;

export default function ProtectedRoute({
  isAuthenticated,
  authenticationPath,
  redirectPath,
  currentUser,
  setRedirectPath,
  ...routeProps
}: ProtectedRouteProps) {
  const currentLocation = useLocation();

  useEffect(() => {
    if (!isAuthenticated) {
      setRedirectPath(currentLocation.pathname);
    }
  }, [isAuthenticated, setRedirectPath, currentLocation]);

  return (
    <>
      {isAuthenticated ? (
        <Route {...routeProps} />
      ) : (
        <Redirect
          to={{ pathname: isAuthenticated ? redirectPath : authenticationPath }}
        />
      )}
    </>
  );
}
