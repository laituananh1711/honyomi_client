import { Box, styled, Typography } from '@mui/material';
import { IconCrown, rankColors } from '../Common/IconCrown';

const PREFIX = 'index';

const classes = {
  root: `${PREFIX}-root`,
  rankStyle: `${PREFIX}-rankStyle`,
  titleStyle: `${PREFIX}-titleStyle`,
  subtitleStyle: `${PREFIX}-subtitleStyle`,
};

const StackWrapper = styled('div')(({ theme }) => ({
  [`& .${classes.root}`]: {
    zIndex: 2,
    padding: '1.8rem',
    position: 'relative',
    height: '100%',
    webkitBoxFlex: 1,
    webkitFlexGrow: 1,
    msFlexPositive: 1,
    flexGrow: 1,
    display: 'flex',
    webkitAlignItems: 'center',
    webkitBoxAlign: 'center',
    msFlexAlign: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    marginTop: '2rem',
  },
  [`& .${classes.rankStyle}`]: {
    lineHeight: '2.2rem',
    fontWeight: 700,
    marginRight: '3.5rem',
    fontSize: '1.8rem',
    padding: 0,
    whiteSpace: 'nowrap',
    textOverflow: 'clip',
  },
  [`& .${classes.titleStyle}`]: {
    fontSize: '1.8rem',
    lineHeight: '2.2rem',
    fontWeight: 700,
    textOverflow: 'ellipsis',
    margin: '0 auto',
    width: '100%',
    overflow: 'hidden',
    padding: 0,
    whiteSpace: 'nowrap',
  },
  [`& .${classes.subtitleStyle}`]: {
    fontSize: '1.8rem',
    lineHeight: '2rem',
    fontWeight: 700,
    opacity: 0.5,
  },
}));

const CustomBox = styled(Box)({
  position: 'relative',
  cursor: 'pointer',
  width: '100%',
  height: '9.6rem',
  borderRadius: '0.8rem',
  WebkitBackgroundSize: 'cover',
  backgroundSize: 'cover',
  backgroundPosition: 'center 16%',
  transition: 'background 0.3s ease-in-out',
  overflow: 'hidden',
  display: '-webkit-box',
  WebkitAlignItems: 'center',
  WebkitBoxAlign: 'center',
  alignItems: 'center',
  WebkitBackgroundClip: 'content-box',
  backgroundClip: 'content-box',
  padding: '1px',
});

const BoxContent = styled(Box)({
  position: 'absolute',
  height: '100%',
  width: '100%',
  transform: 'translateZ(1)',
  borderRadius: '0.7rem',
  transition: 'opacity 0.35s ease-in-out',
  MozTransform: 'translateY(-50%)',
  '@media (max-width: 1030px)': {
    opacity: 0.8,
  },
});

interface CardPopularProps {
  title: string;
  rank: number;
  imageUrl: string;
}

export function CardPopular({ title, rank, imageUrl }: CardPopularProps) {
  const IsInTop3 = rank <= 3;
  return (
    <CustomBox
      sx={{
        width: '100%',
        backgroundImage: `url(${imageUrl})`,
      }}
    >
      <BoxContent
        sx={
          IsInTop3 === false
            ? {
                color: 'text.primary',
                bgcolor: 'background.card',
                '&:hover': {
                  transition: 'opacity 0.5s ease-in-out',
                  opacity: 0.8,
                },
                opacity: 1,
              }
            : {
                color: 'black',
                background: rankColors.get(rank),
                '&:hover': {
                  transition: 'opacity 0.5s ease-in-out',
                  opacity: 0.8,
                },
                opacity: 1,
              }
        }
      >
        <StackWrapper>
          <div className={classes.root}>
            <Typography variant="h6" className={classes.rankStyle}>
              {rank}
            </Typography>
            <Typography variant="h6" className={classes.titleStyle}>
              {title}
            </Typography>
            {IsInTop3 ? <div>{IconCrown(rank)}</div> : null}
          </div>
        </StackWrapper>
      </BoxContent>
    </CustomBox>
  );
}
