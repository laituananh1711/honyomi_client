import { _t } from '../../../utils/messages';
import { translations } from '../../../locales/translations';

export const messages = {
  mostPopular: () => _t(translations.mangaFeature.mostPopular),
};
