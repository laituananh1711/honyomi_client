import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  metadataDes: () => _t(translations.login.metadata.description),
  title: () => _t(translations.login.title),
  username: () => _t(translations.login.username),
  password: () => _t(translations.login.password),
  or: () => _t(translations.login.or),
  login: () => _t(translations.login.login),
  google: () => _t(translations.login.google),
  unauthorized: () => _t(translations.login.unauthorized),
  required: () => _t(translations.login.required),
  signUp: () => _t(translations.register.signUp),
  userNotExist: () => _t(translations.login.userNotExist),
};
