import { Google } from '@mui/icons-material';
import { Box, BoxProps, FormHelperText } from '@mui/material';
import { styled } from '@mui/material/styles';
import authApi from 'app/api/auth/authApi';
import { RenderError } from 'app/components/Error';
import { useSessionContext } from 'app/hooks/sessionContext';
import { signInWithPopup } from 'firebase/auth';
import { Form, Formik } from 'formik';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { UnderlinedLink } from 'app/components/Link';
import * as Yup from 'yup';
import { auth, providers } from '../../../../service/firebase';
import { CustomButton } from '../../../components/Button';
import CustomInput from '../../../components/CustomInput';
import FavIcon from '../../../components/Navbar/FavIcon';
import { messages } from './message';

export function Login() {
  const { t } = useTranslation();
  const [session, setSession] = useSessionContext();
  const history = useHistory();
  const [err, setError] = useState<Error | null>(null);

  function validationSchema() {
    return Yup.object().shape({
      username: Yup.string().required(t(messages.required())),
      password: Yup.string().required(t(messages.required())),
    });
  }

  function getProfileAndRedirect() {
    authApi.getProfile().then(res => {
      setSession({
        ...session,
        isAuthenticated: true,
        currentUser: res,
      });
      history.push(session.redirectPath);
      window.location.href = session.redirectPath;
    });
  }

  function handleLoginWithGoogle() {
    signInWithPopup(auth, providers.google)
      .then(async result => {
        const idToken = await auth.currentUser?.getIdToken();
        if (idToken == null) {
          return;
        }
        authApi
          .loginWithGoogle(idToken)
          .then(res => getProfileAndRedirect())
          .catch(err => {
            setError(new Error(t(messages.userNotExist())));
          });
      })
      .catch(console.error);
  }

  return (
    <>
      {err && <RenderError error={err}></RenderError>}
      <Container>
        <AuthCard>
          <Logo>
            <FavIcon />
          </Logo>
          <Formik
            initialValues={{ username: '', password: '' }}
            validationSchema={validationSchema}
            onSubmit={async values => {
              authApi
                .login(values.username, values.password)
                .then(res => getProfileAndRedirect())
                .catch(error => {
                  setError(new Error(t(messages.unauthorized())));
                });
            }}
          >
            {formik => {
              const { errors, touched } = formik;
              return (
                <Form>
                  <label>{t(messages.username())}</label>
                  <LoginInput
                    name="username"
                    id="username"
                    value={formik.values.username}
                    onChange={formik.handleChange}
                    placeholder={t(messages.username())}
                  />
                  <StyledFormHelperText
                    error={touched.username && Boolean(errors.username)}
                  >
                    {touched.username && errors.username}
                  </StyledFormHelperText>

                  <label>{t(messages.password())}</label>
                  <LoginInput
                    type="password"
                    name="password"
                    id="password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    placeholder={t(messages.password())}
                  />
                  <StyledFormHelperText
                    error={touched.password && Boolean(errors.password)}
                  >
                    {touched.password && errors.password}
                  </StyledFormHelperText>
                  <LoginBtn btnSize={'small'} type="submit">
                    {t(messages.login())}
                  </LoginBtn>
                </Form>
              );
            }}
          </Formik>

          <Or>{t(messages.or())}</Or>
          <LoginBtn btnSize={'small'} onClick={handleLoginWithGoogle}>
            <Google color={'error'} />
            {t(messages.google())}
          </LoginBtn>
          <Or></Or>
          <UnderlinedLink to="/register" style={{ textAlign: 'right' }}>
            {t(messages.signUp())}
          </UnderlinedLink>
        </AuthCard>
      </Container>
    </>
  );
}

const Container = styled(Box)<BoxProps>(({ theme }) => ({
  display: 'grid',
  gridTemplateColumns: 'repeat(6, minmax(0rem, 1fr))',
  // backgroundColor: theme.palette.background.contrastCard,
  // color: theme.palette.text.secondary,
  padding: '0 4rem',
  width: '100%',
  minHeight: 'calc(100vh - 16.8rem)',
  transition:
    'background .5s ease, color .1s ease, fill .5s ease, border .5s ease',

  fontSize: '1.6rem',
  lineHeight: '2.4rem',
  fontWeight: 700,

  '@media (min-width: 768px)': {
    gridTemplateColumns: 'repeat(13, minmax(0rem, 1fr))',
  },

  '@media (min-width: 1270px)': {
    gridTemplateColumns: 'repeat(24, minmax(0rem, 1fr))',
  },
}));

const AuthCard = styled(`div`)(({ theme }) => ({
  gridColumn: '2/6',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: '5em 0',
  '@media (min-width: 768px)': {
    gridColumn: '5 / 10',
  },
  '@media (min-width: 1270px)': {
    gridColumn: '10 / 16',
  },
}));

export const Logo = styled('div')`
  align-self: center;
  min-width: 50%;
  margin-bottom: 5rem;
`;

const LoginInput = styled(CustomInput)`
  margin-bottom: 2rem;
`;

const Or = styled('div')`
  text-align: center;
  margin: 1.2rem 0;
`;

const LoginBtn = styled(CustomButton)`
  text-transform: initial !important;
  width: 100%;
`;

const StyledFormHelperText = styled(FormHelperText)`
  font-size: 1.2rem;
  margin-top: -1rem;
  margin-bottom: 1rem;
`;
