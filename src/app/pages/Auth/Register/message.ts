import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  username: () => _t(translations.register.username),
  password: () => _t(translations.register.password),
  email: () => _t(translations.register.email),
  requiredValue: () => _t(translations.register.requiredValue),
  lengthPasswordError: () => _t(translations.register.lengthPasswordError),
  lengthUsernameError: () => _t(translations.register.lengthUsernameError),
  invalidEmailError: () => _t(translations.register.invalidEmailError),
  signUp: () => _t(translations.register.signUp),
  login: () => _t(translations.register.login),
  existedAccount: () => _t(translations.register.existedAccount),
  copyright: () => _t(translations.register.copyright),
  registerFailed: () => _t(translations.register.registerFailed),
};
