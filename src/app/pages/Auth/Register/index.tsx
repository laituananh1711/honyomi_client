import { styled } from '@mui/material';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import authApi from 'app/api/auth/authApi';
import { Link } from 'app/components/Link';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { CustomButton } from '../../../components/Button';
import CustomInput from '../../../components/CustomInput';
import FavIcon from '../../../components/Navbar/FavIcon';
import { Logo } from '../Login';
import { messages } from './message';

const Register: React.FC = () => {
  const { t } = useTranslation();
  const history = useHistory();

  const [values, setValues] = useState({
    username: '',
    email: '',
    password: '',
  });

  const [errors, setErrors] = useState({} as any);
  const validate: any = (fieldValues = values) => {
    let temp: any = {};

    if ('username' in fieldValues) {
      temp.username = fieldValues.username ? '' : t(messages.requiredValue());
      if (fieldValues.username.length > 24 || fieldValues.username.length < 4) {
        temp.username = t(messages.lengthUsernameError());
      }
    }

    if ('email' in fieldValues) {
      temp.email = fieldValues.email ? '' : t(messages.requiredValue());
      if (fieldValues.email)
        temp.email = /^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(fieldValues.email)
          ? ''
          : t(messages.invalidEmailError());
    }

    if ('password' in fieldValues) {
      temp.password = fieldValues.password ? '' : t(messages.requiredValue());
      if (fieldValues.password.length > 32 || fieldValues.password.length < 8) {
        temp.password = t(messages.lengthPasswordError());
      }
    }

    setErrors(temp);
    return !(temp['email'] && temp['username'] && temp['password']);
  };

  const handleInputValue = (e: any) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (validate(values)) {
      authApi
        .register(values.username, values.email, values.password)
        .then(res => {
          console.log(res.data);
          history.push('/login');
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  return (
    <Container
      component="main"
      maxWidth="xs"
      sx={{
        minHeight: 'calc(100vh - 16.8rem)',
        marginTop: '3rem',
        fontSize: '1.6rem',
      }}
    >
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Logo>
          <FavIcon />
        </Logo>
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <label>
                <b>{t(messages.username())} *</b>
              </label>
              <CustomInput
                required
                name={'username'}
                onChange={handleInputValue}
                placeholder={t(messages.username())}
                sx={{ marginTop: 1 }}
              />
              {errors['username'] ? (
                <ErrorInfo>{errors['username']}</ErrorInfo>
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <label>
                <b>{t(messages.email())} *</b>
              </label>
              <CustomInput
                required
                name={'email'}
                onChange={handleInputValue}
                placeholder={t(messages.email())}
                sx={{ marginTop: 1 }}
              />
              {errors['email'] ? (
                <ErrorInfo>{errors['email']}</ErrorInfo>
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <label>
                <b>{t(messages.password())} *</b>
              </label>
              <CustomInput
                required
                name={'password'}
                onChange={handleInputValue}
                placeholder={t(messages.password())}
                type={'password'}
                sx={{ marginTop: 1 }}
              />
              {errors['password'] ? (
                <ErrorInfo>{errors['password']}</ErrorInfo>
              ) : null}
            </Grid>
          </Grid>
          <CustomButton
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            btnSize={'small'}
          >
            {t(messages.signUp())}
          </CustomButton>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <StyledLink to="/login">
                {t(messages.existedAccount())} {t(messages.login())}
              </StyledLink>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
};

const StyledLink = styled(Link)`
  font-size: 1.4rem;

  &:hover {
    text-decoration: underline;
    text-underline-offset: 4px;
  }
`;

const ErrorInfo = styled('div')(({ theme }) => ({
  color: 'red',
}));

export default Register;
