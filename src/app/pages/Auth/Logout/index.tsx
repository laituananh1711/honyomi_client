import authApi from 'app/api/auth/authApi';
import { useSessionContext } from 'app/hooks/sessionContext';
import { userDefault } from 'app/models/session';
import { useHistory } from 'react-router-dom';

export function Logout() {
  const [session, setSession] = useSessionContext();
  const history = useHistory();

  authApi.logout().then(res => {
    setSession({
      ...session,
      isAuthenticated: false,
      currentUser: userDefault,
    });
    history.push(session.redirectPath);
    window.location.href = session.redirectPath;
  });
  return <></>;
}
