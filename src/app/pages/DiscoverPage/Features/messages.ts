import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  discover: () => _t(translations.mangaFeature.discover),
  moreInfo: () => _t(translations.mangaFeature.moreInfo),
  sort: () => _t(translations.mangaFeature.sort),
  genre: () => _t(translations.mangaFeature.genre),
  group: () => _t(translations.mangaFeature.group),
  author: () => _t(translations.mangaFeature.author),
  status: () => _t(translations.mangaFeature.status),
  series: () => _t(translations.mangaFeature.series),
  ongoing: () => _t(translations.mangaFeature.ongoing),
  chapters: () => _t(translations.mangaFeature.chapters),
  titleAsc: () => _t(translations.mangaFeature.titleAsc),
  titleDesc: () => _t(translations.mangaFeature.titleDesc),
  clearAll: () => _t(translations.mangaFeature.clearAll),
};
