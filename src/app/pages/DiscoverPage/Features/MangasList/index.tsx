import { Circle } from '@mui/icons-material';
import CloseIcon from '@mui/icons-material/Close';
import {
  Menu,
  MenuItem,
  MenuProps,
  styled,
  Tooltip,
  tooltipClasses,
  TooltipProps,
  Typography,
  TypographyProps,
} from '@mui/material';
import { SERIES } from 'app/api/manga/mangaApi.constants';
import { CardImageDiscover } from 'app/components/CardImageDiscover';
import { LargeChip } from 'app/components/CustomChip';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import 'swiper/swiper-bundle.min.css';
import 'swiper/swiper.min.css';
import authorApi from '../../../../api/author';
import categoryApi from '../../../../api/category';
import mangaApi from '../../../../api/manga/mangaApi';
import {
  AuthorWithCountDTO,
  CategoryWithCountDTO,
  MangaStatusDTO,
  PaginatedResultsMeta,
} from '../../../../api/types';
import { AppMenu, AppMenuOption } from '../../../../components/AppMenu';
import { useAsync } from '../../../../hooks/useAsync';
import { useQuery } from '../../../../hooks/useQuery';
import { Manga } from '../../../../models/Manga';
import { messages } from '../messages';

const TITLE_ASCENDING = '+canonicalTitle';
const TITLE_DESCENDING = '-canonicalTitle';
const CATEGORY_QS = 'category';
const AUTHOR_QS = 'author';

export default function MangasList() {
  const history = useHistory();

  // get mangas list from API.
  const { data: mangaData, run: runManga } = useAsync<{
    data: Manga[];
    meta: PaginatedResultsMeta;
  }>({
    data: {
      data: [],
      meta: {
        totalItems: 0,
        totalPages: 0,
        perPage: 0,
        currentPage: 0,
      },
    },
  });

  const {
    data: mangas,
    meta: { totalItems: mangaCount },
  } = mangaData!;

  const { data: genres, run: runGenre } = useAsync<CategoryWithCountDTO[]>({
    data: [],
  });

  useEffect(() => {
    runGenre(categoryApi.getCategoriesWithCount());
  }, [runGenre]);

  const { data: authors, run: runAuthor } = useAsync<AuthorWithCountDTO[]>({
    data: [],
  });

  useEffect(() => {
    runAuthor(authorApi.getAuthorsWithCount());
  }, [runAuthor]);

  const { data: statuses, run: runStatus } = useAsync<MangaStatusDTO[]>({
    data: [],
  });

  useEffect(() => {
    runStatus(mangaApi.getMangaStatusWithCount());
  }, [runStatus]);

  const [selectedGenres, setSelectedGenres] = useState<AppMenuOption[]>([]);
  const [selectedAuthors, setSelectedAuthors] = useState<AppMenuOption[]>([]);
  const [selectedStatuses, setSelectedStatuses] = useState<AppMenuOption[]>([]);
  const [selectedSort, setSelectedSort] = useState<string>(TITLE_ASCENDING);

  useEffect(() => {
    runManga(
      mangaApi.getManga({
        sort: [selectedSort],
        filter: {
          categories: selectedGenres.map(item => item.value.toString()),
          authors: selectedAuthors.map(item => item.value.toString()),
          statuses: selectedStatuses.map(item => item.value.toString()),
        },
        include: ['chapters'],
      }),
    );
  }, [
    runManga,
    selectedAuthors,
    selectedGenres,
    selectedStatuses,
    selectedSort,
  ]);

  const { t } = useTranslation();
  // filterOptions includes genre, author and status filter, etc...
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const closeSortFilterMenu = () => {
    setAnchorEl(null);
  };

  /* WORKAROUND: the time is near. We need to be ready */
  /* Check if the query string is the same as the selected options. If not, set
   * the selected options to be equal to the query strings.*/
  const query = useQuery();
  const categoryIds = query.get(CATEGORY_QS)?.split(',') || [];
  const authorIds = query.get(AUTHOR_QS)?.split(',') || [];

  const queryCategories = genres!.filter(item =>
    categoryIds.includes(item.category.id),
  );
  const isSameQueryCategories =
    selectedGenres.map(item => item.value).join(',') ===
    queryCategories.map(item => item.category.id).join(',');
  if (!isSameQueryCategories) {
    setSelectedGenres(
      queryCategories.map(item => ({
        label: item.category.title,
        value: item.category.id,
      })),
    );
  }

  const queryAuthors = authors!.filter(item =>
    authorIds.includes(item.author.id),
  );
  const isSameQueryAuthors =
    selectedAuthors.map(item => item.value).join(',') ===
    queryAuthors.map(item => item.author.id).join(',');
  if (!isSameQueryAuthors) {
    setSelectedAuthors(
      queryAuthors.map(item => ({
        label: item.author.name,
        value: item.author.id,
      })),
    );
  }

  /* END WORKAROUND */

  const toggleGenre = (option: AppMenuOption) => {
    const categoryIds = queryCategories.map(item => item.category.id);

    const optIndex = selectedGenres.findIndex(
      item => item.label === option.label,
    );

    if (optIndex >= 0) {
      categoryIds.splice(optIndex, 1);
    } else {
      categoryIds.push(option.value.toString());
    }
    query.set(CATEGORY_QS, categoryIds.join(','));
    history.push({ search: query.toString() });
  };

  // toggleAuthor changes the selected state of an author filter option.
  const toggleAuthor = (option: AppMenuOption) => {
    const authorIds = queryAuthors.map(item => item.author.id);

    const optIndex = selectedAuthors.findIndex(
      item => item.label === option.label,
    );

    if (optIndex >= 0) {
      authorIds.splice(optIndex, 1);
    } else {
      authorIds.push(option.value.toString());
    }
    query.set(AUTHOR_QS, authorIds.join(','));
    history.push({ search: query.toString() });
  };

  const toggleStatus = (statusOpt: AppMenuOption) => {
    const currentSelectedStatuses = [...selectedStatuses];
    const optIndex = currentSelectedStatuses.findIndex(
      item => item.label === statusOpt.label,
    );

    if (optIndex >= 0) {
      currentSelectedStatuses.splice(optIndex, 1);
    } else {
      currentSelectedStatuses.push(statusOpt);
    }

    setSelectedStatuses(currentSelectedStatuses);
  };

  // handleDeleteAll resets all filter options.
  const handleDeleteAll = () => {
    setSelectedStatuses([]);
    history.push({});
  };

  const filterOptionsLen =
    selectedGenres.length + selectedAuthors.length + selectedStatuses.length;

  const genreFilterItems = selectedGenres.map(option => {
    return (
      <ListItem key={option.label}>
        <LargeChip
          isActived={false}
          hasArrowIcon={false}
          labelText={option.label}
          inArrayChip={true}
          deleteIcon={<CustomCloseIcon />}
          onDelete={() => toggleGenre(option)}
          onClick={() => toggleGenre(option)}
          sx={{ border: '1px solid', borderColor: 'text.secondary' }}
        />
      </ListItem>
    );
  });

  const authorFilterItems = selectedAuthors.map(option => (
    <ListItem key={option.label}>
      <LargeChip
        isActived={false}
        hasArrowIcon={false}
        labelText={option.label}
        inArrayChip={true}
        deleteIcon={<CustomCloseIcon />}
        onDelete={() => toggleAuthor(option)}
        onClick={() => toggleAuthor(option)}
        sx={{ border: '1px solid', borderColor: 'text.secondary' }}
      />
    </ListItem>
  ));

  const statusFilterItems = selectedStatuses.map(option => (
    <ListItem key={option.label}>
      <LargeChip
        isActived={false}
        hasArrowIcon={false}
        labelText={option.label}
        inArrayChip={true}
        deleteIcon={<CustomCloseIcon />}
        onDelete={() => toggleStatus(option)}
        onClick={() => toggleStatus(option)}
        sx={{ border: '1px solid', borderColor: 'text.secondary' }}
      />
    </ListItem>
  ));

  return (
    <MangasListContainer>
      <FilterContainer>
        <FilterWrapper>
          <div>
            <LargeChip
              isActived={open}
              hasArrowIcon={true}
              labelText={t(messages.sort())}
              aria-haspopup="true"
              aria-expanded={open ? 'true' : undefined}
              onClick={handleClick}
              variant="filled"
            ></LargeChip>
            <StyledMenu
              anchorEl={anchorEl}
              open={open}
              onClose={closeSortFilterMenu}
            >
              <MenuItem
                onClick={() => {
                  setSelectedSort(TITLE_ASCENDING);
                  closeSortFilterMenu();
                }}
                selected={selectedSort === TITLE_ASCENDING}
              >
                <CustomCircle /> {t(messages.titleAsc())}
              </MenuItem>
              <MenuItem
                onClick={() => {
                  setSelectedSort(TITLE_DESCENDING);
                  closeSortFilterMenu();
                }}
                selected={selectedSort === TITLE_DESCENDING}
              >
                <CustomCircle />
                {t(messages.titleDesc())}
              </MenuItem>
            </StyledMenu>
          </div>
          <AppMenu
            title={t(messages.genre())}
            options={genres!.map(item => ({
              label: item.category.title,
              value: item.category.id,
              prefix: <CustomCircle />,
              suffix: <span>{item.mangaCount}</span>,
            }))}
            selected={selectedGenres}
            onMenuItemClick={(event, option) => {
              toggleGenre(option);
            }}
          />
          <AppMenu
            title={t(messages.author())}
            options={authors!.map(item => ({
              label: item.author.name,
              value: item.author.id,
              prefix: <CustomCircle />,
              suffix: <span>{item.mangaCount}</span>,
            }))}
            selected={selectedAuthors}
            onMenuItemClick={(event, option) => {
              toggleAuthor(option);
            }}
          />
          <AppMenu
            title={t(messages.status())}
            options={statuses!.map(item => ({
              label: item.status.name,
              value: item.status.value,
              prefix: <CustomCircle />,
              suffix: <span>{item.mangaCount}</span>,
            }))}
            selected={selectedStatuses}
            onMenuItemClick={(event, option) => {
              toggleStatus(option);
            }}
          />
        </FilterWrapper>
        <TotalSeries>
          {mangaCount.toString() + ' ' + t(messages.series())}
        </TotalSeries>
      </FilterContainer>
      {filterOptionsLen > 0 ? <StyledDivider></StyledDivider> : <></>}
      <SelectedFilterOptions>
        {/* list selected filter item and show a 'Clear All' button if necessary */}
        {genreFilterItems}
        {authorFilterItems}
        {statusFilterItems}
        {filterOptionsLen > 1 ? (
          <ListItem>
            <LargeChip
              isActived={false}
              hasArrowIcon={false}
              labelText={t(messages.clearAll())}
              onClick={handleDeleteAll}
            />
          </ListItem>
        ) : null}
      </SelectedFilterOptions>
      <SeriesListContainer>
        {mangas.map((serie, index) => (
          <CustomTooltip
            key={index}
            followCursor
            placement="right"
            title={
              <React.Fragment>
                <StyledMangaTitle>
                  {t(messages.series())}
                  <Circle
                    fontSize="small"
                    sx={{ width: '0.6rem', height: '0.6rem', margin: '0.6rem' }}
                  ></Circle>
                  {serie.chapters.length + ' ' + t(messages.chapters())}
                </StyledMangaTitle>
                <StyledMangaType>{serie.mangaType}</StyledMangaType>
              </React.Fragment>
            }
          >
            <div>
              <CardImageDiscover
                title={serie.title}
                imageUrl={serie.coverImage.original.url}
                path={`${SERIES}/${serie.id}`}
              />
            </div>
          </CustomTooltip>
        ))}
      </SeriesListContainer>
    </MangasListContainer>
  );
}

const MangasListContainer = styled('div')`
  display: grid;
  grid-template-columns: repeat(6, minmax(0rem, 1fr));
  grid-column-gap: 1.6rem;
  padding: 6.4rem 3.2rem;

  @media (min-width: 768px) {
    grid-template-columns: repeat(13, minmax(0rem, 1fr));
    grid-column-gap: 2rem;
  }

  @media (min-width: 1270px) {
    grid-template-columns: repeat(24, minmax(0rem, 1fr));
    max-width: none;
  }

  @media (min-width: 768px) {
    padding: 6.4rem 4rem;
  }
`;

const FilterContainer = styled('div')`
  width: 100%;
  display: flex;
  -webkit-box-pack: justify;
  -webkit-justify-content: space-between;
  justify-content: space-between;
  position: relative;
  grid-column: 1/-1;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;

  @media (min-width: 546px) {
    grid-column: 1/-1;
  }

  @media (min-width: 768px) {
    grid-column: 2/-1;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
  }

  @media (min-width: 1270px) {
    grid-column: 5/-1;
  }
`;

const FilterWrapper = styled('div')`
  display: flex;
  -webkit-box-flex-wrap: nowrap;
  -webkit-flex-wrap: nowrap;
  -ms-flex-wrap: nowrap;
  flex-wrap: nowrap;
  gap: 1.6rem;
  overflow-x: auto;
  @media (min-width: 768px) {
    overflow-x: unset;
  }

  :-webkit-scrollbar {
    display: none;
  }
`;

const TotalSeries = styled('p')`
  font-size: 1.2rem;
  line-height: 1.6rem;
  font-weight: 500;
  -webkit-align-self: flex-start;
  -ms-flex-item-align: flex-start;
  align-self: flex-start;
  padding-top: 1.2rem;

  @media (min-width: 768px) {
    padding-top: 0rem;
    -webkit-align-self: center;
    -ms-flex-item-align: center;
    align-self: center;
  }
`;

const SelectedFilterOptions = styled('div')`
  grid-column: 1/-1;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-flex-wrap: wrap;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  gap: 1.2rem;
  margin-bottom: 3.2rem;

  @media (min-width: 546px) {
    grid-column: 1/-1;
  }

  @media (min-width: 768px) {
    grid-column: 2/-1;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
  }

  @media (min-width: 1270px) {
    grid-column: 5/-1;
  }

  .e6ul01d1 {
    text-transform: capitalize;
  }
`;

const SeriesListContainer = styled('div')`
  grid-column: 1/-1;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: auto;
  grid-gap: 2rem;

  @media (min-width: 375px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (min-width: 546px) {
    grid-template-columns: repeat(3, 1fr);
  }

  @media (min-width: 768px) {
    grid-column: 2/-1;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 4rem 2rem;
  }

  @media (min-width: 938px) {
    grid-column: 2/-1;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 4rem 2rem;
  }

  @media (min-width: 1270px) {
    grid-column: 5/-1;
    grid-template-columns: repeat(5, 1fr);
  }

  @media (min-width: 1560px) {
    grid-template-columns: repeat(6, 1fr);
  }
`;

const CustomTooltip = styled(({ className, ...props }: TooltipProps) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    background: 'rgba(0, 0, 0, 0.85) !important',
    border: '1px solid transparent',
    borderRadius: '0.8rem !important',
    padding: '1.6rem',
  },
}));

const StyledMangaTitle = styled(Typography)<TypographyProps>(({ theme }) => ({
  fontSize: '1.8rem',
  lineHeight: '2.2rem',
  fontWeight: 700,
  marginBottom: '1.7rem',
  textTransform: 'capitalize',
}));

const StyledMangaType = styled(Typography)<TypographyProps>(({ theme }) => ({
  fontSize: '1.4rem',
  lineHeight: '2rem',
  fontWeight: 500,
  paddingTop: '0.2rem',
  color: '#6d6d6d',
}));

const StyledMenu = styled((props: MenuProps) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'left',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'left',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    animation: '0.2s ease-in-out 0s 1 normal none running animation-1l98evl',
    position: 'absolute',
    left: '0rem',
    transform: 'translateY(100%)',
    backgroundColor: theme.palette.background.card,
    filter: `drop-shadow( 0px 1.2rem 3rem ${theme.palette.text.secondary})`,
    zIndex: 99,
    width: '100%',
    maxHeight: '28.8rem',
    borderRadius: '0.8rem',
    overflowY: 'auto',
    marginTop: '1.6rem',
    '@media (min-width: 768px)': {
      width: '33.9rem',
    },
    '& .MuiMenu-list': {
      padding: '0.8rem',
      boxSizing: 'border-box',
      display: 'block',
      overflowX: 'hidden',
      '&:last-child': {
        paddingBottom: '0',
      },
    },
    '& .MuiMenuItem-root': {
      fontSize: '1.6rem',
      marginBottom: '0.8rem',
      textTransform: 'capitalize',
      display: 'flex',
      webkitBoxAlign: 'center',
      alignItems: 'center',
      padding: '1.8rem 1.8rem 1.8rem 1.6rem',
      borderRadius: '0.8rem',
      cursor: 'pointer',
      transition: 'background 0.2s ease 0s',
      background: theme.palette.background.card,
      pointEvents: 'none',
      '&:hover': {
        backgroundColor: theme.palette.background.contrastCard,
        color: theme.palette.primary.contrastText,
        '& .large-chip': {
          backgroundColor: theme.palette.primary.contrastText,
          color: theme.palette.primary.contrastText,
          borderColor: theme.palette.primary.contrastText,
        },
      },
      '&:active': {
        backgroundColor: theme.palette.background.contrastCard,
        color: theme.palette.primary.contrastText,
      },
    },
    '& .Mui-selected': {
      backgroundColor: theme.palette.background.contrastCard,
      color: theme.palette.primary.contrastText,
      '& .large-chip': {
        backgroundColor: theme.palette.primary.contrastText,
        color: theme.palette.primary.contrastText,
        borderColor: theme.palette.primary.contrastText,
      },
    },
  },
}));

function CustomCircle() {
  return (
    <Circle
      className="large-chip"
      fontSize="small"
      sx={{
        height: '1rem',
        width: '1rem',
        marginRight: '1.6rem',
        borderRadius: '0.4rem',
        transition: 'border 0.2s ease 0s',
        border: 2,
        color: 'primary.contrastText',
        borderColor: 'text.secondary',
        bgcolor: 'primary.contrastText',
        '&:hover': {
          bgcolor: 'primary.contrastText',
          color: 'primary.contrastText',
          borderColor: 'primary.contrastText',
        },
      }}
    ></Circle>
  );
}

const ListItem = styled('li')(({ theme }) => ({
  margin: theme.spacing(0.5),
  listStyleType: 'none',
}));

function CustomCloseIcon() {
  return (
    <CloseIcon
      sx={{ color: 'primary.main', marginLeft: '-0.8rem', fontWeight: 700 }}
      className="custom-icon"
    />
  );
}

const StyledDivider = styled('div')(({ theme }) => ({
  height: '0.1rem',
  margin: '2.4rem 0rem',
  gridArea: '2 / 1 / 3 / -1',
  borderBottom: `1px solid ${theme.palette.background.chip}`,
  '@media (min-width: 546px)': {
    gridColumn: '1 / -1',
  },
  '@media (min-width: 768px)': {
    gridColumn: '2 / -1',
    flexDirection: 'row',
  },
  '@media (min-width: 1270px)': {
    gridColumn: '5 / -1',
  },
}));
