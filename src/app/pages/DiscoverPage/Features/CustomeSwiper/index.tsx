import { CircularProgress, styled } from '@mui/material';
import { CardPopularSwipper } from 'app/components/CardPopularSwiper';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.min.css';
import 'swiper/swiper.min.css';
import mangaApi from '../../../../api/manga/mangaApi';
import { SERIES } from '../../../../api/manga/mangaApi.constants';
import { MostPopularMangaDTO } from '../../../../api/types';
import { RenderError } from '../../../../components/Error';
import { useAsync } from '../../../../hooks/useAsync';
import { messages } from '../messages';

export default function CustomSwiper() {
  const { t } = useTranslation();

  const {
    data: mostPopularMangas,
    run,
    error,
    loading,
  } = useAsync<MostPopularMangaDTO[]>({
    data: [],
  });

  useEffect(() => {
    run(mangaApi.getMostPopularManga());
  }, [run]);

  return (
    <CustomSwiperWrapper>
      <SwiperTitle>{t(messages.discover())}</SwiperTitle>
      <SwiperWrapper>
        <Swiper
          freeMode={true}
          freeModeMomentum={false}
          grabCursor={true}
          slidesPerView="auto"
          spaceBetween={20}
        >
          {error && <RenderError error={error} />}
          {loading ? (
            <CircularProgress />
          ) : (
            mostPopularMangas!.map(({ manga }, index) => (
              <SwiperSlide className="swiper-slide" key={manga.id}>
                <StyledLink to={`${SERIES}/${manga.id}`}>
                  <CardPopularSwipper
                    title={manga.title}
                    rank={index + 1}
                    imageUrl={manga.coverImage.original.url}
                  />
                </StyledLink>
              </SwiperSlide>
            ))
          )}
        </Swiper>
      </SwiperWrapper>
    </CustomSwiperWrapper>
  );
}

const SwiperTitle = styled('p')`
  font-size: 2.4rem;
  line-height: 4rem;
  font-weight: 700;
  grid-row: 1;
  grid-column: 1/-1;
  margin-top: 2rem;

  @media (min-width: 375px) {
    font-size: 4rem;
  }

  @media (min-width: 768px) {
    margin-top: 0;
    grid-column: 2/-1;
  }

  @media (min-width: 1270px) {
    margin-top: 0;
    grid-column: 5/-1;
  }
`;

const CustomSwiperWrapper = styled('div')`
  display: grid;
  grid-template-columns: repeat(6, minmax(0rem, 1fr));
  grid-column-gap: 1.6rem;
  padding: 6.4rem 3.2rem;
  grid-row-gap: 20px;
  padding-top: 9rem !important;
  padding-bottom: 0rem !important;

  @media (min-width: 768px) {
    grid-template-columns: repeat(13, minmax(0rem, 1fr));
    grid-column-gap: 2rem;
  }

  @media (min-width: 1270px) {
    grid-template-columns: repeat(24, minmax(0rem, 1fr));
    max-width: none;
  }

  @media (min-width: 768px) {
    padding: 6.4rem 4rem;
  }
`;

const SwiperWrapper = styled('div')`
  position: relative;
  grid-row: 2;
  grid-column: 1/-1;
  top: -4rem;

  @media (min-width: 768px) {
    grid-column: 2/-1;
    margin-right: 0px;
  }

  @media (min-width: 1270px) {
    grid-column: 5/-1;
  }

  .swiper-slide {
    width: auto;
  }
`;

const StyledLink = styled(Link)`
  -webkit-text-decoration: none;
  text-decoration: none;
  text-underline-offset: 4px;
  overflow: hidden;

  :hover {
    -webkit-text-decoration: none;
    text-decoration: none;
  }
`;
