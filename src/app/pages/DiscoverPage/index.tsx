import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import '../../components/CustomCarousel/customCarousel.css';
import CustomSwiper from './Features/CustomeSwiper';
import MangasList from './Features/MangasList';
import { messages } from './messages';

export function DiscoverPage() {
  const { t } = useTranslation();
  return (
    <>
      <Helmet>
        <title>{t(messages.title())}</title>
      </Helmet>
      <CustomSwiper />
      <MangasList />
    </>
  );
}
