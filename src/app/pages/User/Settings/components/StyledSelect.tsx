import { SelectProps, Select, MenuProps } from '@mui/material';
import { styled } from '@mui/system';
import { CustomMaterialInput } from 'app/components/CustomInput';

export const StyledSelect = styled((props: SelectProps<any>) => (
  <Select input={<CustomMaterialInput />} MenuProps={menuProps} {...props} />
))(({ theme }) => ({}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const menuProps: Partial<MenuProps> = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
  elevation: 0,
  anchorOrigin: {
    vertical: 'bottom',
    horizontal: 'left',
  },
  transformOrigin: {
    vertical: 'top',
    horizontal: 'left',
  },
  sx: (theme: any) => ({
    '& .MuiPaper-root': {
      animation: '0.2s ease-in-out 0s 1 normal none running animation-1l98evl',
      position: 'absolute',
      left: '0rem',
      transform: 'translateY(100%)',
      backgroundColor: theme.palette.background.card,
      filter: `drop-shadow( 0px 1.2rem 3rem ${theme.palette.text.secondary})`,
      zIndex: 99,
      width: '100%',
      maxHeight: '28.8rem',
      borderRadius: '0.8rem',
      overflowY: 'auto',
      marginTop: '1.6rem',
      '@media (min-width: 768px)': {
        width: '33.9rem',
      },
      '& .MuiList-root': {
        padding: '0.8rem',
        boxSizing: 'border-box',
        display: 'block',
        overflowX: 'hidden',
        '&:last-child': {
          paddingBottom: '0',
        },
      },
      '& .MuiMenuItem-root': {
        fontSize: '1.6rem',
        marginBottom: '0.8rem',
        textTransform: 'capitalize',
        display: 'flex',
        webkitBoxAlign: 'center',
        alignItems: 'center',
        padding: '1.8rem 1.8rem 1.8rem 1.6rem',
        borderRadius: '0.8rem',
        cursor: 'pointer',
        transition: 'background 0.2s ease 0s',
        background: theme.palette.background.card,
        pointEvents: 'none',
        '&:hover': {
          backgroundColor: theme.palette.background.contrastCard,
          color: theme.palette.primary.contrastText,
          '& .large-chip': {
            backgroundColor: theme.palette.primary.contrastText,
            color: theme.palette.primary.contrastText,
            borderColor: theme.palette.primary.contrastText,
          },
        },
        '&:active': {
          backgroundColor: theme.palette.background.contrastCard,
          color: theme.palette.primary.contrastText,
        },
      },
      '& .Mui-selected': {
        backgroundColor: theme.palette.background.contrastCard,
        color: theme.palette.primary.contrastText,
        '& .large-chip': {
          backgroundColor: theme.palette.primary.contrastText,
          color: theme.palette.primary.contrastText,
          borderColor: theme.palette.primary.contrastText,
        },
      },
    },
  }),
};
