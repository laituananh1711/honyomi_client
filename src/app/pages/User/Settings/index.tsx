import {
  CardActions,
  CircularProgress,
  MenuItem,
  SelectChangeEvent,
} from '@mui/material';
import { useTheme } from '@mui/material/styles';
import { ColorModeContext } from 'app';
import { LanguageCodeToName } from 'app/api/types';
import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components/macro';
import { meApi } from '../../../api/me';
import { languages } from '../../../assets/lang';
import { SmallButton } from '../../../components/Button';
import { CustomMaterialInput } from '../../../components/CustomInput';
import { RenderError } from '../../../components/Error';
import {
  PageContainer,
  PageContent,
  PageTitle,
  StyledCard,
  StyledCardContent,
  StyledCardTitle,
} from '../../../components/styled';
import { useAsync } from '../../../hooks/useAsync';
import { Setting, User } from '../../../models/User';
import { StyledSelect } from './components/StyledSelect';
import { messages } from './messages';

const initialUser: User = {
  username: '',
  id: '',
  role: 'user',
  email: '',
  avatarUrl: '',
  setting: {
    uiTheme: '',
    language: '',
    mangaLanguages: [],
  },
};

// TODO: integrate setting API.
export const Settings: React.FC = () => {
  const { t } = useTranslation();

  const {
    data: userInfo,
    error,
    run,
    loading,
  } = useAsync<User>({
    data: initialUser,
  });

  useEffect(() => {
    run(meApi.getProfile());
  }, [run]);

  const handleSaveEmail = (email: string) => {
    run(
      meApi.updateProfile({
        email,
      }),
    );
  };

  const handleChangePassword = (currentPass: string, newPass: string) => {
    run(
      meApi.updatePassword({
        currentPassword: currentPass,
        newPassword: newPass,
      }),
    );
  };

  const handleChangeSettings = (setting: Setting) => {
    run(
      meApi.updateProfile({
        setting,
      }),
    );
  };

  return (
    <>
      <Helmet>
        <title>{t(messages.setting())}</title>
      </Helmet>
      <PageContainer>
        <PageTitle>{t(messages.setting())}</PageTitle>
        <PageContent>
          {error && <RenderError error={error} />}
          {loading ? (
            <CircularProgress />
          ) : (
            <>
              <UpdateProfileCard user={userInfo!} onSave={handleSaveEmail} />
              <ChangePasswordCard onSave={handleChangePassword} />
              <ReadingPreferenceCard
                onSave={handleChangeSettings}
                setting={userInfo!.setting}
              />
            </>
          )}
        </PageContent>
      </PageContainer>
    </>
  );
};

interface UserProps {
  user: User;
}

interface UpdateProfileCardProps extends UserProps {
  onSave: (email: string) => void;
}

const UpdateProfileCard: React.FC<UpdateProfileCardProps> = ({
  user,
  onSave,
}) => {
  const { t } = useTranslation();

  const [email, setEmail] = useState(user.email);

  const isSaveBtnDisabled = email === user.email;

  return (
    <SettingCard>
      <SettingCardContent style={{ paddingBottom: 0 }}>
        <StyledCardTitle>{t(messages.profile())}</StyledCardTitle>
        <p>{t(messages.profileDesc())}</p>
        <div>
          <label>Username</label>
          <SettingCardInput
            placeholder="Username"
            defaultValue={user.username}
            readOnly={true}
          />

          <label>Email</label>
          <SettingCardInput
            placeholder="Email"
            style={{ marginBottom: 0 }}
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </div>
      </SettingCardContent>
      <SettingCardActions>
        <SmallButton disabled={isSaveBtnDisabled} onClick={() => onSave(email)}>
          {t(messages.save())}
        </SmallButton>
      </SettingCardActions>
    </SettingCard>
  );
};

interface ChangePasswordCardProps {
  onSave: (currentPassword: string, newPassword: string) => void;
}

const ChangePasswordCard: React.FC<ChangePasswordCardProps> = ({ onSave }) => {
  const { t } = useTranslation();

  const [currentPass, setCurrentPass] = useState('');
  const [newPass, setNewPass] = useState('');
  const [reenterPass, setReenterPass] = useState('');

  const [errors, setErrors] = useState<Record<string, string>>({});

  const isSaveBtnDisabled = currentPass === '' || newPass === '';

  const validate = (): boolean => {
    let tempErrors: Record<string, string> = {};
    if (newPass.length < 8) {
      tempErrors['newPass'] = t(messages.lengthPasswordError());
    }
    if (newPass !== reenterPass) {
      tempErrors['reenterPass'] = t(messages.passwordNotMatching());
    }

    setErrors(tempErrors);
    return Object.keys(tempErrors).length === 0;
  };

  const handleSubmit = () => {
    if (!validate()) {
      return;
    }
    onSave(currentPass, newPass);
  };

  return (
    <SettingCard>
      <SettingCardContent>
        <StyledCardTitle>{t(messages.changePassword())}</StyledCardTitle>
        <p>{t(messages.changePasswordDesc())}</p>
        <form>
          <label>{t(messages.currentPassword())}</label>
          <SettingCardInput
            name="current"
            placeholder="Current Password"
            value={currentPass}
            onChange={e => setCurrentPass(e.target.value)}
            required
            type="password"
            autoComplete="current-password"
          />

          <label>{t(messages.newPassword())}</label>
          <SettingCardInput
            name="new"
            placeholder="New Password"
            value={newPass}
            onChange={e => setNewPass(e.target.value)}
            type="password"
            autoComplete="new-password"
          />
          {errors['newPass'] && <ErrorInfo>{errors['newPass']}</ErrorInfo>}

          <label>{t(messages.reenterPassword())}</label>
          <SettingCardInput
            name="reenter"
            placeholder="Reenter Password"
            value={reenterPass}
            onChange={e => setReenterPass(e.target.value)}
            type="password"
            autoComplete="new-password"
          />
          {errors['reenterPass'] && (
            <ErrorInfo>{errors['reenterPass']}</ErrorInfo>
          )}
        </form>
      </SettingCardContent>
      <SettingCardActions>
        <SmallButton disabled={isSaveBtnDisabled} onClick={handleSubmit}>
          {t(messages.save())}
        </SmallButton>
      </SettingCardActions>
    </SettingCard>
  );
};

const uiLanguages: LanguageCodeToName = {
  en: {
    name: 'English',
    nativeName: 'English',
  },
  vi: {
    name: 'Vietnamese',
    nativeName: 'Tiếng Việt',
  },
};

interface ReadingPreferenceCardProps {
  setting: Setting;
  onSave: (setting: Setting) => void;
}
const ReadingPreferenceCard: React.FC<ReadingPreferenceCardProps> = ({
  setting,
  onSave,
}) => {
  const { t, i18n } = useTranslation();
  const theme = useTheme();
  const colorMode = React.useContext(ColorModeContext);

  const [selectedLanguages, setSelectedLanguages] = useState<string[]>(
    setting.mangaLanguages,
  );

  const handleChangeChapterLanguages = (event: SelectChangeEvent<string[]>) => {
    const {
      target: { value },
    } = event;

    setSelectedLanguages(typeof value === 'string' ? value.split(',') : value);
  };

  const handleChangeUILanguage = (event: SelectChangeEvent<string>) => {
    i18n.changeLanguage(event.target.value);
  };

  const handleChangeUITheme = (event: SelectChangeEvent<'light' | 'dark'>) => {
    colorMode.setMode(event.target.value as any);
  };

  const handleSubmit = () => {
    onSave({
      mangaLanguages: selectedLanguages,
      language: i18n.language,
      uiTheme: theme.palette.mode,
    });
  };

  return (
    <SettingCard>
      <SettingCardContent>
        <StyledCardTitle>{t(messages.preference())}</StyledCardTitle>
        <div>
          <label id="chapter-language-select-label">
            {t(messages.chapterLanguage())}
          </label>
          <StyledSelect
            labelId="chapter-language-select-label"
            id="chapter-language-select"
            multiple
            value={selectedLanguages}
            onChange={handleChangeChapterLanguages}
            input={<SettingCardInput />}
          >
            {Object.keys(languages).map(key => (
              <MenuItem key={key} value={key}>
                {languages[key].name}
              </MenuItem>
            ))}
          </StyledSelect>
        </div>
        <div>
          <label>{t(messages.uiLanguage())}</label>
          <StyledSelect
            input={<SettingCardInput />}
            value={i18n.language}
            onChange={handleChangeUILanguage}
          >
            {Object.keys(uiLanguages).map(key => (
              <MenuItem key={key} value={key}>
                {uiLanguages[key].name}
              </MenuItem>
            ))}
          </StyledSelect>
        </div>
        <div>
          <label>{t(messages.theme())}</label>
          <StyledSelect
            input={<SettingCardInput />}
            value={theme.palette.mode}
            onChange={handleChangeUITheme}
          >
            {['dark', 'light'].map(item => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </StyledSelect>
        </div>
      </SettingCardContent>
      <SettingCardActions>
        <SmallButton onClick={handleSubmit}>{t(messages.save())}</SmallButton>
      </SettingCardActions>
    </SettingCard>
  );
};

const SettingCard = styled(StyledCard)`
  margin-bottom: 3rem;
`;

const SettingCardContent = styled(StyledCardContent)`
  display: block;
  font-size: 1.6rem;

  label {
    font-weight: 700;
  }
`;

const SettingCardInput = styled(CustomMaterialInput)(({ theme }) => ({
  background: theme.palette.background.cardDarker,
  marginBottom: '2rem',
}));

const SettingCardActions = styled(CardActions)`
  display: flex;
  flex-direction: row;
  justify-content: end;
`;

const ErrorInfo = styled('div')(({ theme }) => ({
  color: 'red',
}));
