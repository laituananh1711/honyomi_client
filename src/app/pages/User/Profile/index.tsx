import React from 'react';
import styled from 'styled-components/macro';
import { UserDetails } from './UserDetail';

// TODO: integrate user, history and favorites API.
const Profile: React.FC = () => {
  return (
    <Container>
      <UserDetails />
    </Container>
  );
};

const Container = styled.div`
  padding: 0;
  width: 100vw;
  min-height: 85vh;
  margin: 0;
`;

export { Profile };
