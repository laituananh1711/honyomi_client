import React, { useRef } from 'react';
import styled from 'styled-components/macro';
import { Avatar } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { useContainerDimensions } from '../../../../hooks/useContainerDimemsion';
import { messages } from '../messages';

interface Prop {
  id?: string;
  username?: string;
  email?: string;
  avatarUrl?: string;
  updatedAt?: Date;
  createdAt?: Date;
}

export const UserDetails: React.FC<Prop> = ({
  id,
  username,
  email,
  avatarUrl,
  createdAt,
  updatedAt,
}) => {
  const { t } = useTranslation();
  const ref = useRef(null);
  const { width } = useContainerDimensions(ref);

  return (
    <>
      <Container>
        <DataWrapper>
          <Id>61671aec2d29bc26431f6ee6</Id>
          <Username>Haipro287</Username>
          <Data>
            <DataTitle>{t(messages.email())}</DataTitle>
            <DataContent>haipro287@gmail.com</DataContent>
          </Data>
          <Data>
            <DataTitle>{t(messages.joinedDate())}</DataTitle>
            <DataContent>28-07-2000</DataContent>
          </Data>
          <Data>
            <DataTitle>{t(messages.languagePreference())}</DataTitle>
            <DataContent>Engrisk</DataContent>
          </Data>
        </DataWrapper>
        <AvatarWrapper ref={ref}>
          <Avatar
            variant={'square'}
            alt="Remy Sharp"
            src="https://images.catmanga.org/series/kanokari/covers/23.jpg"
            sx={{ width: width, height: width }}
          />
        </AvatarWrapper>
      </Container>
    </>
  );
};

const Container = styled('div')`
  display: grid;
  grid-template-columns: repeat(6, minmax(0rem, 1fr));
  column-gap: 1.6rem;
  padding-top: 6.4rem;
  padding-right: 3.2rem;
  padding-left: 3.2rem;
  padding-bottom: 0 !important;

  @media (min-width: 768px) {
    grid-template-columns: repeat(13, minmax(0rem, 1fr));
    column-gap: 2rem;
  }

  @media (min-width: 768px) {
    padding: 8.4rem 4rem;
  }

  @media (min-width: 1270px) {
    grid-template-columns: repeat(24, minmax(0rem, 1fr));
    max-width: none;
  }
`;

const DataWrapper = styled('div')`
  grid-column: 1 / -1;
  z-index: 20;

  @media (min-width: 768px) {
    grid-column: 2 / span 8;
  }

  @media (min-width: 1270px) {
    grid-column: 5 / span 13;
  }
`;

const Id = styled('div')`
  font-size: 1.8rem;
  font-weight: 700;
  color: #989898;
  padding-right: 0.8rem;
  margin: 1rem 0;

  @media (min-width: 375px) {
    font-size: 2.4rem;
  }
`;
const Username = styled('div')(({ theme }) => ({
  color: theme.palette.primary.main,
  fontSize: '3.2rem',
  lineHeight: '3.2rem',
  fontWeight: 700,
  paddingBottom: '1.6rem',

  '@media (min-width: 375px)': {
    fontSize: '4.8rem',
    lineHeight: '4.8rem',
    fontWeight: 700,
  },
}));

const Data = styled('div')`
  display: grid;
  grid-template-columns: repeat(6, minmax(0rem, 1fr));
  column-gap: 1.6rem;
  //@media (min-width: 768px) {
  //  grid-template-columns: repeat(13, minmax(0rem, 1fr));
  //  column-gap: 2rem;
  //}

  //@media (min-width: 1270px) {
  //  grid-template-columns: repeat(24, minmax(0rem, 1fr));
  //  max-width: none;
  //}
`;

const DataTitle = styled('div')`
  grid-row-start: 1;
  grid-column: 1 / span 3;
  font-size: 1.8rem;
  font-weight: 700;
  padding-right: 0.8rem;
  margin: 1rem 0;

  @media (min-width: 375px) {
    font-size: 2.4rem;
  }
`;
const DataContent = styled('div')`
  grid-row-start: 1;
  grid-column: 3 / span 6;
  font-size: 1.8rem;
  font-weight: normal;
  padding-right: 0.8rem;
  margin: 1rem 0;

  @media (min-width: 375px) {
    font-size: 2.4rem;
  }
`;

const AvatarWrapper = styled('div')`
  position: relative;
  grid-column: 3/-3;
  grid-row: 1/2;

  @media (min-width: 768px) {
    grid-row: auto;
    grid-column: -4 / span 4;
  }

  @media (min-width: 1270px) {
    grid-column: -6 / span 5;
  }
`;

// const Avatar = styled('img')`
//   object-fit: cover;
//   object-position: top;
//   border-radius: 0.8rem;
//   width: 100%;
//   height: 18.4rem;
//
//   @media (min-width: 375px) {
//     height: 40rem;
//   }
//
//   @media (min-width: 768px) {
//     height: auto;
//   }
// `;
