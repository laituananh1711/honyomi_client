import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  languagePreference: () => _t(translations.profile.languagePreference),
  email: () => _t(translations.profile.email),
  joinedDate: () => _t(translations.profile.joinedDate),
};
