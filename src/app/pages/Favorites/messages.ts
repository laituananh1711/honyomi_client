import { translations } from '../../../locales/translations';
import { _t } from '../../../utils/messages';

export const messages = {
  favorites: () => _t(translations.favorites),
  noFavoritesFound: () => _t(translations.favoritePage.noFavoritesFound),
  loginRequired: () => _t(translations.favoritePage.loginRequired),
  remove: () => _t(translations.favoritePage.remove),
};
