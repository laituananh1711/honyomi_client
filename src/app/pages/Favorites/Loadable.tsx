import { lazyLoad } from '../../../utils/loadable';

export const FavoritePage = lazyLoad(
  () => import('./index'),
  module => module.FavoritePage,
);
