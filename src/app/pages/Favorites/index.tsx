import { CircularProgress, styled, Typography } from '@mui/material';
import React, { ReactNode, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { UnderlinedLink as Link } from '../../components/Link';
import { SERIES } from '../../api/manga/mangaApi.constants';
import { meApi } from '../../api/me';
import {
  LoginRequiredText,
  PageContainer,
  PageContent,
} from '../../components/styled';
import { useAsync } from '../../hooks/useAsync';
import { Manga } from '../../models/Manga';
import { messages } from './messages';
import { BookIcon } from 'app/assets/BookIcon';

export function FavoritePage(): JSX.Element {
  const { t } = useTranslation();
  const {
    data: favoriteMangas,
    run,
    loading,
    error,
  } = useAsync<Manga[]>({
    data: [],
  });

  useEffect(() => {
    run(meApi.getFavorites());
  }, [run]);

  const handleRemoveFavorite = (mangaId: string) => {
    run(meApi.removeFavorite(mangaId));
  };

  const favoriteMangaList: ReactNode =
    favoriteMangas!.length === 0 ? (
      <NoFavoritesFoundComponent />
    ) : (
      <SeriesListContainer>
        {favoriteMangas!.map(manga => (
          <div
            key={manga.id}
            style={{ maxWidth: '24rem' }}
            className="card-image-discover"
          >
            <Link to={`/${SERIES}/${manga.id}`} className="css-link">
              <div className="css-image-discover-wrapper">
                <img
                  className="css-image-discover-card"
                  src={manga.coverImage.original.url}
                  alt={'Cover art for ' + manga.title}
                  loading="lazy"
                />
              </div>

              <div className="css-info-serie-wrapper">
                <Typography
                  className="css-mange-name-serie"
                  sx={{ color: 'text.primary' }}
                >
                  {manga.title}
                </Typography>
              </div>
            </Link>
            <div
              style={{ width: 'fit-content' }}
              onClick={() => handleRemoveFavorite(manga.id)}
            >
              <RemoveText className="css-chapter-more-info">
                {t(messages.remove())}
              </RemoveText>
            </div>
          </div>
        ))}{' '}
      </SeriesListContainer>
    );

  // handle no favorites manga
  return (
    <>
      <Helmet>
        <title>{t(messages.favorites())}</title>
      </Helmet>

      <PageContainer>
        <PageTitle>{t(messages.favorites())}</PageTitle>

        <PageContent>
          {loading ? <CircularProgress /> : favoriteMangaList}
          {error && (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Link to="/login">
                <LoginRequiredText>
                  {t(messages.loginRequired())}
                </LoginRequiredText>
              </Link>
            </div>
          )}
        </PageContent>
      </PageContainer>
    </>
  );
}

const NoFavoritesFoundComponent: React.FC = () => {
  const { t } = useTranslation();
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <div>
        <BookIcon style={{ width: '15rem' }} />
      </div>
      <LoginRequiredText>{t(messages.noFavoritesFound())}</LoginRequiredText>
    </div>
  );
};

const PageTitle = styled('p')`
  font-size: 2.4rem;
  line-height: 4rem;
  font-weight: 700;
  grid-row: 1;
  grid-column: 1/-1;
  margin-top: 2rem;

  @media (min-width: 375px) {
    font-size: 4rem;
  }

  @media (min-width: 768px) {
    margin-top: 0;
    grid-column: 2/-1;
  }

  @media (min-width: 1270px) {
    margin-top: 0;
    grid-column: 5/-1;
  }
`;

const SeriesListContainer = styled('div')`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: auto;
  grid-gap: 2rem;

  @media (min-width: 375px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (min-width: 546px) {
    grid-template-columns: repeat(3, 1fr);
  }

  @media (min-width: 768px) {
    grid-column: 2/-1;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 4rem 2rem;
  }

  @media (min-width: 938px) {
    grid-column: 2/-1;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 4rem 2rem;
  }

  @media (min-width: 1270px) {
    grid-column: 5/-1;
    grid-template-columns: repeat(5, 1fr);
  }

  @media (min-width: 1560px) {
    grid-template-columns: repeat(6, 1fr);
  }
`;

const RemoveText = styled(Typography)`
  transition: color 0.2s ease;
  &:hover {
    cursor: pointer;
    color: red;
  }
`;
