import { lazyLoad } from '../../../utils/loadable';

export const ReadingHistoryPage = lazyLoad(
  () => import('./index'),
  module => module.ReadingHistoryPage,
);
