import { Card, CardContent, CardProps, List, ListItem } from '@mui/material';
import { READER, SERIES } from 'app/api/manga/mangaApi.constants';
import {
  LoginRequiredText,
  PageContainer,
  PageContent,
  PageTitle,
} from 'app/components/styled';
import React, { CSSProperties, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { UnderlinedLink as Link } from '../../components/Link';
import styled from 'styled-components';
import { meApi } from '../../api/me';
import { useAsync } from '../../hooks/useAsync';
import { Manga } from '../../models/Manga';
import { messages } from './messages';

export function ReadingHistoryPage(): JSX.Element {
  const { t } = useTranslation();

  const {
    data: readMangas,
    error,
    run,
  } = useAsync<Manga[]>({
    data: [],
  });

  useEffect(() => {
    run(meApi.getHistory());
  }, [run]);

  return (
    <>
      <Helmet>
        <title>{t(messages.history())}</title>
      </Helmet>
      <PageContainer>
        <PageTitle>{t(messages.history())}</PageTitle>
        <PageContent>
          {readMangas!.map(manga => (
            <MangaReadHistoryCard
              key={manga.id}
              manga={manga}
              style={{
                marginBottom: '2rem',
              }}
            />
          ))}

          {error && (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Link to="/login">
                <LoginRequiredText>
                  {t(messages.loginRequired())}
                </LoginRequiredText>
              </Link>
            </div>
          )}
        </PageContent>
      </PageContainer>
    </>
  );
}

export interface MangaReadHistoryCardProps {
  manga: Manga;
  style?: CSSProperties;
}

export const MangaReadHistoryCard: React.FC<MangaReadHistoryCardProps> = ({
  manga,
  style,
}) => {
  const { t } = useTranslation();
  return (
    <StyledCard style={style}>
      <StyledCardContent>
        <LeftCardImageWrapper>
          <Link to={`/${SERIES}/${manga.id}`}>
            <LeftCardImage
              src={manga.coverImage.original.url}
              alt="manga cover"
            />
          </Link>
        </LeftCardImageWrapper>
        <div>
          <StyledLink to={`/${SERIES}/${manga.id}`}>
            <StyledCardTitle>{manga.title}</StyledCardTitle>
          </StyledLink>
          <List>
            {manga.chapters.map(chapter => (
              <ListItem>
                <StyledLink to={`/${READER}/${manga.id}/${chapter.number}`}>
                  {t(messages.chapter())} {chapter.number}: {chapter.title}
                </StyledLink>
              </ListItem>
            ))}
          </List>
        </div>
      </StyledCardContent>
    </StyledCard>
  );
};

const StyledLink = styled(Link)`
  -webkit-text-decoration: none;
  text-decoration: none;
  text-underline-offset: 4px;
  overflow: hidden;
  color: inherit;
  transition: background 0.35s ease 0s;

  &:hover {
    opacity: 0.8;
  }
`;

const StyledCardTitle = styled('h2')`
  font-size: 2.5rem;
  font-weight: 600;
  margin-top: 0;
`;

const StyledCard = styled((prop: CardProps) => (
  <Card variant="outlined" {...prop} />
))(({ theme }) => ({
  backgroundColor: theme.palette.background.card,
}));

const StyledCardContent = styled(CardContent)`
  display: flex;
  flex-direction: row;
`;

const LeftCardImageWrapper = styled('div')`
  width: 20%;
  padding-top: 30%;
  position: relative;
  margin-right: 2rem;
  max-height: 100px;
`;

const LeftCardImage = styled('img')`
  width: 100%;
  height: 100%;
  object-fit: cover;
  object-position: top;
  border-radius: 0.8rem;
  -webkit-transition: box-shadow 0.3s ease-in-out;
  transition: box-shadow 0.3s ease-in-out;
  position: absolute;
  top: 0;
  z-index: 10;
`;
