import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  history: () => _t(translations.history),
  loginRequired: () => _t(translations.historyPage.loginRequired),
  chapter: () => _t(translations.Chapter),
};
