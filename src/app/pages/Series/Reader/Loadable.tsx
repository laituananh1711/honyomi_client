/**
 * Asynchronously loads the component for HomePage
 */

import { lazyLoad } from 'utils/loadable';

export const Reader = lazyLoad(
  () => import('./index'),
  module => module.Reader,
);
