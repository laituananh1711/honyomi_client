import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  page: () => _t(translations.chapterFeature.page),
  reply: () => _t(translations.comment.reply),
  cancel: () => _t(translations.comment.cancel),
  newestCmt: () => _t(translations.comment.newestCmt),
  oldestCmt: () => _t(translations.comment.oldestCmt),
  readMore: () => _t(translations.comment.readMore),
  showLess: () => _t(translations.comment.showLess),
  whatAreYouThought: () => _t(translations.comment.whatAreYouThought),
  comment: () => _t(translations.comment.comment),
  loginRequired: () => _t(translations.comment.loginRequired),
};
