import styled from 'styled-components/macro';
import { UIEvent, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { CommentDrawer } from '../CommentDrawer';
import { StatusFavorite } from '../../Chapters/components/StatusFavorite';
import { Stack } from '@mui/material';

interface Prop {
  images: string[];
  hide?: boolean;
  chapterId: string;
  mangaId: string;
}

export default function LongStripeReader({
  images,
  hide,
  chapterId,
  mangaId,
}: Prop) {
  const { t } = useTranslation();
  const ref = useRef(null);
  const [lastScrollPosition, setLastScrollPosition] = useState<number>(0);
  const handleScroll = (e: UIEvent<HTMLElement>) => {
    let element = e.currentTarget;
    if (element.scrollTop < lastScrollPosition) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    } else {
      // @ts-ignore
      ref.current.scrollIntoView({ behavior: 'smooth' });
      // window.scrollTo();
    }

    setLastScrollPosition(element.scrollTop);
  };

  return (
    <LongStripeContainer ref={ref} hide={hide} onScroll={handleScroll}>
      {images.map((image, index) => {
        return (
          <ImageContainer key={index}>
            <img src={image} alt={`${t('page')} ${index + 1}`} />
          </ImageContainer>
        );
      })}
      <CommentIconContainer>
        <Stack direction="row" alignItems="center" spacing={2}>
          <StatusFavorite isFavorited={true} mangaId={mangaId} />
          <CommentDrawer chapterId={chapterId} />
        </Stack>
      </CommentIconContainer>
    </LongStripeContainer>
  );
}

const LongStripeContainer = styled.div<{ hide?: boolean }>`
  width: 100%;
  position: relative;
  max-height: calc(100vh - 1rem);
  overflow-y: scroll;
  grid-area: 3 / 1 / -1 / -1;
  cursor: pointer;
  user-select: none;
  text-align: 'center';
  @media (min-width: 768px) {
    grid-column: ${props => (props.hide ? 5 : 1)} / -1;
    z-index: 20;
  }
  @media (min-width: 1270px) {
    background-color: transparent;
    grid-area: 1 / 6 / auto / -1;
  }

  ::-webkit-scrollbar {
    display: none;
  }

  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none;

  div {
    height: unset;
    margin-bottom: 1.6rem;
  }
`;

const ImageContainer = styled.div`
  cursor: pointer;
  min-width: 100%;
  width: 100%;
  height: 100%;
  flex-shrink: 1;
  position: relative;
  display: grid;
  -webkit-box-pack: center;
  justify-content: center;

  img {
    object-fit: contain;
    grid-row-start: 2;
    grid-column-start: 2;
    max-width: 100%;
    min-width: 0px;
    width: 100%;
    height: auto;
  }
`;

const CommentIconContainer = styled.div`
  cursor: pointer;
  flex-shrink: 1;
  position: relative;
  display: grid;
  -webkit-box-pack: center;
  justify-content: center;
`;
