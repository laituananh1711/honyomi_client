import { Box, CircularProgress } from '@mui/material';
import SideBar from '../../../components/SideBar';
import styled from 'styled-components';
import LongStripeReader from './LongStripeReader';
import { useState } from 'react';
import * as React from 'react';
import { useAsync } from 'app/hooks/useAsync';
import { Manga } from 'app/models/Manga';
import mangaApi from 'app/api/manga/mangaApi';
import { useParams } from 'react-router-dom';
import { RenderError } from 'app/components/Error';
import { Chapter } from 'app/models/Chapter';
import { meApi } from '../../../api/me';

export const SideBarContext = React.createContext({
  toggleSideBar: () => {},
});

export interface ReaderParams {
  mangaId: string;
  number: string;
}

export function Reader() {
  let { mangaId, number } = useParams<ReaderParams>();
  const { loading, data: manga, error, run } = useAsync<Manga>();
  // const manga = data;

  React.useEffect(() => {
    run(mangaApi.getMangaEndpoint(mangaId));
  }, [run, mangaId]);

  let mangaTitle = '';
  let chapters: Chapter[] = [];
  let images: string[] = [];
  let currentChapter: Chapter | undefined;
  if (manga !== undefined) {
    mangaTitle = manga.title;

    let chaptersData = manga.chapters.sort(function (chapterA, chapterB) {
      return Number(chapterA.number) < Number(chapterB.number) ? -1 : 1;
    });
    chapters = chaptersData ?? [];

    let filteredChapters = manga.chapters.filter(
      chapter => chapter.number === number,
    );

    currentChapter = manga.chapters.find(chapter => chapter.number === number);

    let pagesData = filteredChapters.map(chapter => chapter.pages);
    images = pagesData[0] ?? [];

    // save this chapter to user history
    meApi.addToHistory(filteredChapters[0]?.id ?? '');
  }
  const [hide, setHide] = useState<boolean>(false);
  const sideBarMode = {
    toggleSideBar: () => {
      setHide(prevState => !prevState);
    },
  };

  return (
    <ReaderContainer>
      {error && <RenderError error={error} />}
      {loading ? (
        <CircularProgress />
      ) : manga ? (
        <SideBarContext.Provider value={sideBarMode}>
          <SideBar chapters={chapters} mangaTitle={mangaTitle} />
          {currentChapter ? (
            <LongStripeReader
              images={images}
              hide={hide}
              chapterId={currentChapter.id}
              mangaId={mangaId}
            />
          ) : (
            <></>
          )}
        </SideBarContext.Provider>
      ) : (
        <> </>
      )}
    </ReaderContainer>
  );
}
const ReaderContainer = styled(Box)`
  overflow-y: visible;
  overflow: hidden;
  display: grid;
  grid-template-columns: repeat(6, minmax(0rem, 1fr));
  position: relative;
  grid-template-rows: 13rem 5.8rem 100vh;
  grid-column-gap: 0;
  padding: 0;
  width: 100vw;
  height: 100%;
  margin: 0;

  @media (min-width: 768px) {
    grid-template-columns: repeat(13, minmax(0rem, 1fr));
    grid-column-gap: 2rem;
    padding: 0;
    grid-template-rows: 15rem 9rem 100vh;
    grid-row-gap: 1.6rem;
  }
  @media (min-width: 1270px) {
    grid-template-columns: repeat(24, minmax(0rem, 1fr));
    padding: 0 4rem;
    height: calc(100vh - 1rem);
    max-width: 100%;
    grid-template-rows: 1fr;
    grid-gap: 0;
  }
`;
