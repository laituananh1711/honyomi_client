import { ChatBubbleOutline } from '@mui/icons-material';
import {
  Box,
  Button,
  CircularProgress,
  Drawer,
  Stack,
  styled,
  Tooltip,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { messages } from '../messages';
import CloseIcon from '@mui/icons-material/Close';
import { EditorComment } from './component/EditorComment';
import OptionComment from './component/OptionComment';
import { CardComment } from './component/CardComment';
import { Comment } from 'app/models/Comment';
import { useAsync } from 'app/hooks/useAsync';
import commentApi from 'app/api/comment/commentApi';
import { RenderError } from 'app/components/Error';
import { SORT_DESC_TIME_PARAM } from 'app/api/comment/commentApi.constant';
import { useSessionContext } from 'app/hooks/sessionContext';
import { UnderlinedLink } from 'app/components/Link';
import { LoginRequiredText } from 'app/components/styled';

interface CommentParams {
  chapterId: string;
}

export function CommentDrawer({ chapterId }: CommentParams) {
  const [state, setState] = React.useState(false);
  const [openReplyBox, setOpenReplyBox] = useState(false);
  const [sortParam, setSortParam] = useState(SORT_DESC_TIME_PARAM);
  const [session] = useSessionContext();
  const [reload, setReload] = useState(false);

  const handleOpenReplyBox = () => {
    setOpenReplyBox(!openReplyBox);
  };

  const handleSortParam = (_param: string) => {
    setSortParam(_param);
  };

  const handleReload = () => {
    setReload(!reload);
  };

  const { t } = useTranslation();
  const showContactTitle = t(messages.comment());
  const cancel = t(messages.cancel());
  const reply = t(messages.reply());
  const newestCmt = t(messages.newestCmt());
  const oldestCmt = t(messages.oldestCmt());
  const readMore = t(messages.readMore());
  const showLess = t(messages.showLess());
  const loginRequired = t(messages.loginRequired());

  const toggleDrawer =
    () => (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }
      setState(!state);
    };

  const { loading, data: listComments, error, run } = useAsync<Comment[]>();

  React.useEffect(() => {
    run(commentApi.getRootComments(chapterId, sortParam));
  }, [run, chapterId, sortParam, reload]);

  let numOfComment = 0;
  if (listComments)
    listComments?.map(cmt => (numOfComment += cmt.repliesCount));

  return (
    <div>
      <Tooltip title={showContactTitle} onClick={toggleDrawer()}>
        <div style={{ marginTop: '2rem' }}>
          <ChatBubbleOutline sx={{ fontSize: '3.5rem', textAlign: 'center' }} />
          {`(${numOfComment})`}
        </div>
      </Tooltip>
      <StyledDrawer anchor={'right'} open={state} onClose={toggleDrawer()}>
        <Stack
          direction="row"
          spacing={30}
          justifyContent="space-between"
          alignItems="center"
          sx={{
            padding: '2rem',
            paddingBottom: '1.5rem',
            paddingRight: '0',
            marginTop: '1rem',
          }}
        >
          <Typography variant="h4">{t(messages.comment())}</Typography>
          <Button onClick={toggleDrawer()}>
            <CloseIcon fontSize="large"></CloseIcon>
          </Button>
        </Stack>

        {openReplyBox ? (
          session.isAuthenticated ? (
            <EditorComment
              handleOpen={handleOpenReplyBox}
              cancel={cancel}
              reply={reply}
              commentId={chapterId}
              isNewComment={true}
              chapterId={chapterId}
              handleReload={handleReload}
            ></EditorComment>
          ) : (
            <UnderlinedLink to="/login">
              <LoginRequiredText>{loginRequired}</LoginRequiredText>
            </UnderlinedLink>
          )
        ) : (
          <Box
            onClick={handleOpenReplyBox}
            sx={{
              color: 'text.secondary',
              marginX: '2rem',
              padding: '2rem',
              border: 0.1,
              '&:hover': {
                cursor: 'text',
              },
            }}
          >
            {t(messages.whatAreYouThought())}
          </Box>
        )}

        <OptionComment
          handleSortParam={handleSortParam}
          newestCmt={newestCmt}
          oldestCmt={oldestCmt}
        />
        {error && <RenderError error={error} />}
        {loading ? (
          <CircularProgress />
        ) : listComments ? (
          listComments.map(comment => (
            <CardComment
              comment={comment}
              cancel={cancel}
              reply={reply}
              readMore={readMore}
              showLess={showLess}
              loginRequired={loginRequired}
              chapterId={chapterId}
              handleReload={handleReload}
            ></CardComment>
          ))
        ) : (
          <></>
        )}
      </StyledDrawer>
    </div>
  );
}

const StyledDrawer = styled(Drawer)(({ theme }) => ({
  transition:
    'transform 0.6s cubic-bezier(0.23, 1, 0.32, 1) 0s, opacity 0.6s cubic-bezier(0.23, 1, 0.32, 1) 0s',
  overflow: 'auto',
  boxSizing: 'border-box',
}));
