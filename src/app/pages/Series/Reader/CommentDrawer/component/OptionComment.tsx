import * as React from 'react';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import styled from 'styled-components';
import { Typography } from '@mui/material';
import { KeyboardArrowDown, KeyboardArrowUp } from '@mui/icons-material';
import { StyledButton } from 'app/components/Button';
import {
  SORT_ASC_TIME_PARAM,
  SORT_DESC_TIME_PARAM,
} from 'app/api/comment/commentApi.constant';

interface OptionCommentProps {
  handleSortParam: (_param: string) => void;
  newestCmt: string;
  oldestCmt: string;
}

export default function OptionComment({
  newestCmt,
  oldestCmt,
  handleSortParam,
}: OptionCommentProps) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [isNewest, setSelectedIsNewest] = React.useState(true);
  const open = Boolean(anchorEl);
  const handleClickListItem = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuItemClick = (
    event: React.MouseEvent<HTMLElement>,
    _isNewest: boolean,
  ) => {
    setSelectedIsNewest(_isNewest);
    setAnchorEl(null);
    handleSortParam(_isNewest ? SORT_DESC_TIME_PARAM : SORT_ASC_TIME_PARAM);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <MenuContainer>
      <StyledButton
        id="lock-button"
        aria-haspopup="listbox"
        aria-controls="lock-menu"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClickListItem}
        endIcon={isNewest ? <KeyboardArrowDown /> : <KeyboardArrowUp />}
      >
        <Typography variant="h5" fontStyle="bolder" fontWeight={700}>
          {isNewest ? newestCmt : oldestCmt}
        </Typography>
      </StyledButton>
      <Menu
        id="lock-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'lock-button',
          role: 'listbox',
        }}
      >
        <MenuItem
          selected={isNewest}
          onClick={event => handleMenuItemClick(event, true)}
        >
          <Typography variant="h5">{newestCmt}</Typography>
        </MenuItem>
        <MenuItem
          selected={!isNewest}
          onClick={event => handleMenuItemClick(event, false)}
        >
          <Typography variant="h5">{oldestCmt}</Typography>
        </MenuItem>
      </Menu>
    </MenuContainer>
  );
}

const MenuContainer = styled('div')(({ theme }) => ({
  margin: '2rem',
  marginBottom: '1rem',
}));
