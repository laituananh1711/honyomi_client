import { Avatar, Card, Divider, Stack, Typography } from '@mui/material';
import { Content } from './ReadMore';
import { Comment } from 'app/models/Comment';
import {
  ChatBubbleOutline,
  FavoriteBorder,
  FavoriteOutlined,
  Reply,
} from '@mui/icons-material';
import { ListRepliesComment } from './ListRepliesComment';
import { useState } from 'react';
import { EditorComment } from './EditorComment';
import commentApi from 'app/api/comment/commentApi';
import dayjs from 'dayjs';
import { useSessionContext } from 'app/hooks/sessionContext';
import { UnderlinedLink } from 'app/components/Link';
import { LoginRequiredText } from 'app/components/styled';

interface CardCommentProps {
  comment: Comment;
  cancel: string;
  reply: string;
  readMore: string;
  showLess: string;
  loginRequired: string;
  chapterId: string;
  handleReload: () => void;
}

export function CardComment({
  comment,
  cancel,
  reply,
  readMore,
  showLess,
  loginRequired,
  chapterId,
  handleReload,
}: CardCommentProps) {
  const [viewComments, setViewComments] = useState(false);
  const [openReplyBox, setOpenReplyBox] = useState(false);
  const [isLiked, setLiked] = useState(false);
  const [replies, setReplies] = useState<Comment[]>([]);
  const [session] = useSessionContext();

  const handleViewComments = () => {
    commentApi.getReplies(comment!.id!).then(res => {
      setReplies(res);
    });
    setViewComments(!viewComments);
  };

  const handleOpenReplyBox = () => {
    setOpenReplyBox(!openReplyBox);
  };

  const handleSetLiked = () => {
    setLiked(!isLiked);
  };

  return (
    <div>
      <Divider
        style={{
          height: 2,
        }}
        sx={{ bgcolor: 'background.chip' }}
      />
      <Card sx={{ maxWidth: 450, padding: '2rem', minWidth: 400 }}>
        <Stack direction="row" alignItems="center" spacing={1}>
          <Avatar src={comment.user.avatarUrl ?? ''}></Avatar>
          <Stack direction="column">
            <Typography variant="h5">{comment.user.username}</Typography>
            <Typography variant="subtitle1">
              {dayjs(comment.createdAt).format('DD/MM/YYYY')}
            </Typography>
          </Stack>
        </Stack>
        <Content
          content={comment.comment}
          readMore={readMore}
          showLess={showLess}
        ></Content>
        <Stack
          direction="row"
          justifyContent="space-between"
          sx={{ marginRight: '1rem' }}
        >
          <Stack spacing={1} direction="row">
            <div onClick={handleSetLiked}>
              {isLiked ? (
                <FavoriteOutlined fontSize="large" />
              ) : (
                <FavoriteBorder fontSize="large" />
              )}
            </div>
            <div onClick={handleViewComments}>
              <ChatBubbleOutline fontSize="large" />
              {comment.repliesCount}
            </div>
          </Stack>
          <div onClick={handleOpenReplyBox}>
            <Reply fontSize="large" />
          </div>
        </Stack>

        {openReplyBox ? (
          session.isAuthenticated ? (
            <EditorComment
              handleOpen={handleOpenReplyBox}
              cancel={cancel}
              reply={reply}
              commentId={comment!.id!}
              isNewComment={false}
              chapterId={chapterId}
              handleReload={handleReload}
            />
          ) : (
            <UnderlinedLink to="/login">
              <LoginRequiredText>{loginRequired}</LoginRequiredText>
            </UnderlinedLink>
          )
        ) : (
          <></>
        )}
        {viewComments && replies ? (
          <ListRepliesComment
            comments={replies}
            cancel={cancel}
            reply={reply}
            readMore={readMore}
            showLess={showLess}
            loginRequired={loginRequired}
            chapterId={chapterId}
            handleReload={handleReload}
          ></ListRepliesComment>
        ) : (
          <></>
        )}
      </Card>
    </div>
  );
}
