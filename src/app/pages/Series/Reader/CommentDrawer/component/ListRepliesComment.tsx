import { Divider, Stack } from '@mui/material';
import { Comment } from 'app/models/Comment';
import { CardComment } from './CardComment';

interface ListRepliesProps {
  comments: Comment[];
  cancel: string;
  reply: string;
  readMore: string;
  showLess: string;
  loginRequired: string;
  chapterId: string;
  handleReload: () => void;
}

export function ListRepliesComment({
  comments,
  cancel,
  reply,
  readMore,
  showLess,
  loginRequired,
  chapterId,
  handleReload,
}: ListRepliesProps) {
  return (
    <Stack direction="row">
      <Divider
        orientation="vertical"
        style={{
          width: 5,
        }}
        sx={{ bgcolor: 'background.chip' }}
        flexItem
      />
      <div>
        {comments.map(comment => (
          <CardComment
            comment={comment}
            cancel={cancel}
            reply={reply}
            readMore={readMore}
            showLess={showLess}
            loginRequired={loginRequired}
            chapterId={chapterId}
            handleReload={handleReload}
          ></CardComment>
        ))}
      </div>
    </Stack>
  );
}
