import { useState } from 'react';

interface ReadMoreProps {
  content: string;
  readMore: string;
  showLess: string;
}

const ReadMore = ({ content, readMore, showLess }: ReadMoreProps) => {
  const isLongContent = content.length > 150 ?? false;
  const [isReadMore, setIsReadMore] = useState(isLongContent);
  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };
  return (
    <p>
      {isReadMore ? content.slice(0, 150) : content}
      {isLongContent ? (
        <span
          onClick={toggleReadMore}
          style={{
            fontWeight: 'bold',
          }}
        >
          {isReadMore ? readMore : showLess}
        </span>
      ) : (
        <></>
      )}
    </p>
  );
};

export function Content({ content, readMore, showLess }: ReadMoreProps) {
  return (
    <p>
      <ReadMore
        content={content}
        readMore={readMore}
        showLess={showLess}
      ></ReadMore>
    </p>
  );
}
