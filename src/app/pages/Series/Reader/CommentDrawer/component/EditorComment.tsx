import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Avatar, Stack } from '@mui/material';
import { CustomTextarea } from 'app/components/CustomInput';
import { CustomButton, StyledButton } from 'app/components/Button';
import { useSessionContext } from 'app/hooks/sessionContext';
import commentApi from 'app/api/comment/commentApi';
import { Form, Formik } from 'formik';

interface EditorCommentInterface {
  handleOpen: () => void;
  cancel: string;
  reply: string;
  commentId: string;
  isNewComment: boolean;
  chapterId: string;
  handleReload: () => void;
}

export function EditorComment({
  handleOpen,
  cancel,
  reply,
  commentId,
  isNewComment,
  chapterId,
  handleReload,
}: EditorCommentInterface) {
  const [session] = useSessionContext();

  return (
    <Card
      sx={{
        maxWidth: 400,
        marginX: '2rem',
        minHeight: 250,
      }}
    >
      <Formik
        initialValues={{ content: '' }}
        onSubmit={values => {
          if (values.content !== '') {
            isNewComment === true
              ? commentApi.addNewComment(chapterId, values.content)
              : commentApi.addNewReply(commentId, values.content);
            handleReload();
            handleOpen();
          }
        }}
      >
        {formik => {
          return (
            <Form>
              <CardContent>
                <Stack direction="row" alignItems="center" spacing={1}>
                  <Avatar src={session.currentUser.avatarUrl}></Avatar>
                  <Typography variant="h5">
                    {session.currentUser.username}
                  </Typography>
                </Stack>
                <CustomTextarea
                  multiline
                  rows={4}
                  sx={{ marginTop: '1rem' }}
                  value={formik.values.content}
                  onChange={formik.handleChange}
                  id="content"
                  name="content"
                ></CustomTextarea>
              </CardContent>
              <div style={{ textAlign: 'right', marginRight: '2rem' }}>
                <StyledButton variant="text" size="large" onClick={handleOpen}>
                  <Typography variant="h5">{cancel}</Typography>
                </StyledButton>
                <CustomButton
                  type="submit"
                  btnSize="small"
                  sx={{ height: '3.5rem' }}
                  disabled={formik.values.content === ''}
                >
                  {reply}
                </CustomButton>
              </div>
            </Form>
          );
        }}
      </Formik>
    </Card>
  );
}
