import React from 'react';
import { Detail, ListChapters } from './components';
import { useParams } from 'react-router-dom';
import { useAsync } from 'app/hooks/useAsync';
import mangaApi from 'app/api/manga/mangaApi';
import { Manga } from 'app/models/Manga';
import { RenderError } from 'app/components/Error';
import { styled, CircularProgress } from '@mui/material';

export interface DetailParams {
  mangaId: string;
}

const Chapters: React.FC = () => {
  let { mangaId } = useParams<DetailParams>();
  const { loading, data: serie, error, run } = useAsync<Manga>();

  React.useEffect(() => {
    run(mangaApi.getMangaEndpoint(mangaId));
  }, [run, mangaId]);

  return (
    <ChaptersContainer>
      {error && <RenderError error={error} />}
      {loading ? (
        <CircularProgress />
      ) : serie ? (
        <div>
          <Detail manga={serie} />
          <ListChapters chapters={serie.chapters} />
        </div>
      ) : (
        <> </>
      )}
    </ChaptersContainer>
  );
};

const ChaptersContainer = styled('div')`
  padding: 0;
  width: 100vw;
  min-height: 85vh;
  margin: 0;
`;

export default Chapters;
