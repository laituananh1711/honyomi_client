import React, { useState } from 'react';
import { Box, styled } from '@mui/material';
import { ChapterTitle } from './Detail';
import { useTranslation } from 'react-i18next';
import { Chapter } from 'app/models/Chapter';
import { READER } from 'app/api/manga/mangaApi.constants';
import { useParams } from 'react-router-dom';
import { DetailParams } from '..';
import { CardChapter } from 'app/components/CardChapter';
import { Link } from 'app/components/Link';
import { LargeChip } from 'app/components/CustomChip';

interface IProps {
  chapters: Chapter[];
}

const ListChapters: React.FC<IProps> = ({ chapters }) => {
  const { t } = useTranslation();
  let { mangaId } = useParams<DetailParams>();

  const [isDecrease, setIsDecrease] = useState<boolean>(true);
  const [chapterNumber, setChapterNumber] = useState<number>(0);

  const sort = () => setIsDecrease(prev => !prev);

  const onChangeInputFilter = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChapterNumber(parseInt(event.target.value));
  };

  const chapterFilterCallback = (chapter: Chapter): boolean => {
    if (!chapterNumber) return true;
    return chapter.number.toString().includes(chapterNumber.toString());
  };

  const chapterSortCallback = (a: Chapter, b: Chapter): number => {
    if (!isDecrease) return Number(a.number) > Number(b.number) ? 1 : -1;
    return Number(a.number) < Number(b.number) ? 1 : -1;
  };

  return (
    <ListChaptersWrapper>
      <ListChaptersHeader>
        <ChapterTitle>{t('Chapters')}</ChapterTitle>
      </ListChaptersHeader>
      <ListChaptersFilter>
        <InputFilter
          placeholder={t('jumpTo')}
          name={'search-chapter'}
          type={'number'}
          min={1}
          onChange={onChangeInputFilter}
        />
        <LargeChip
          onClick={sort}
          isActived={isDecrease}
          hasArrowIcon={true}
          labelText={t('Sort')}
          variant="filled"
        ></LargeChip>
      </ListChaptersFilter>

      <ListChaptersContent>
        {chapters
          .filter(chapterFilterCallback)
          .sort(chapterSortCallback)
          .map(chapter => (
            <Link
              key={chapter.id}
              to={`/${READER}/${mangaId}/${chapter.number}`}
            >
              <CardChapter
                key={chapter.id}
                number={chapter.number}
                chapterTitle={chapter.title}
              ></CardChapter>
            </Link>
          ))}
      </ListChaptersContent>
    </ListChaptersWrapper>
  );
};

const ListChaptersContent = styled(Box)`
  display: grid;
  grid-column: 1/-1;
  grid-template-columns: repeat(auto-fit, minmax(100%, 1fr));
  grid-gap: 2rem;

  @media (min-width: 546px) {
    grid-template-columns: repeat(2, minmax(40%, 1fr));
  }

  @media (min-width: 768px) {
    grid-column: 2/-1;
    grid-template-columns: repeat(3, minmax(30%, 1fr));
  }

  @media (min-width: 1270px) {
    grid-column: 5/-1;
    grid-template-columns: repeat(4, minmax(20%, 1fr));
  }

  @media (min-width: 1560px) {
    grid-column: 5/-1;
    grid-template-columns: repeat(5, minmax(12%, 1fr));
  }
`;

const InputFilter = styled('input')(({ theme }) => ({
  border: 'none',
  outline: 'none',
  background: theme.palette.background.card,
  color: theme.palette.primary.main,
  fontSize: '1.6rem',
  fontWeight: 500,
  borderBottom: `0.2rem solid ${theme.palette.primary.dark}`,

  marginRight: '2.8rem',
  height: '100%',
  width: '9.6rem',

  '@media (min-width: 375px)': {
    display: 'inline-block',
  },
}));

const ListChaptersFilter = styled(Box)`
  height: 4rem;
  grid-row: 2/3;
  grid-column: 1/-1;
  justify-self: flex-end;
  -webkit-align-self: center;
  align-self: center;
  margin-bottom: 4.8rem;
  margin-top: -2.4rem;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;

  @media (min-width: 546px) {
    grid-row: 1/2;
    grid-column: 1/-1;
    margin-bottom: 0rem;
    margin-top: 0rem;
    -webkit-align-self: flex-start;
    align-self: flex-start;
  }
`;

const ListChaptersHeader = styled(Box)`
  width: 100%;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
  -webkit-justify-content: space-between;
  justify-content: space-between;
  grid-row: 1/2;
  grid-column: 1/-1;
  margin-bottom: 4.8rem;

  @media (min-width: 546px) {
    grid-column: 1/-1;
  }

  @media (min-width: 768px) {
    grid-column: 2/-1;
  }

  @media (min-width: 1270px) {
    grid-column: 5/-1;
  }
`;

const ListChaptersWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(6, minmax(0rem, 1fr));
  grid-column-gap: 1.6rem;
  padding: 6.4rem 3.2rem;

  @media (min-width: 768px) {
    padding: 10rem 4rem 6.4rem;
    grid-template-columns: repeat(13, minmax(0rem, 1fr));
    grid-column-gap: 2rem;
  }

  @media (min-width: 1270px) {
    grid-template-columns: repeat(24, minmax(0rem, 1fr));
    max-width: none;
  }
`;

export default ListChapters;
