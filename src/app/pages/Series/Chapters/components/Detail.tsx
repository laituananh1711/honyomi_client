import React, { useState } from 'react';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { styled } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { Box } from '@mui/material';
import { Manga } from 'app/models/Manga';
import { Link as StyledLink } from 'app/components/Link';
import { AUTHOR, CATEGORY, DISCOVER } from 'app/api/manga/mangaApi.constants';
import { Link } from 'react-router-dom';
import { StatusFavorite } from './StatusFavorite';

interface IProps {
  manga: Manga;
}

const Detail: React.FC<IProps> = ({ manga }) => {
  const { t } = useTranslation();
  return (
    <Container>
      <Content>
        <AlterTitles title={manga.romajiTitle} />
        <ChapterTitle>{manga.title}</ChapterTitle>

        <GenresWrapper>
          {manga.categories.map(item => (
            <StyledLink
              key={item.id}
              to={`/${DISCOVER}?${CATEGORY}=${item.id}`}
            >
              <GenreItem>
                <span style={{ fontWeight: 500 }}>{item.title}</span>
              </GenreItem>
            </StyledLink>
          ))}
        </GenresWrapper>

        <Description>{manga.synopsis}</Description>

        <ContentSource>
          <div>
            <b>
              {t('Author')} / {t('Artist')}{' '}
            </b>
            <span>
              {manga.authors.map((item, index, ar) => {
                return (
                  <span key={item.id}>
                    <LinkProfile to={`/${DISCOVER}?${AUTHOR}=${item.id}`}>
                      {item.name}
                    </LinkProfile>
                    {index !== ar.length - 1 && ', '}
                  </span>
                );
              })}
            </span>
          </div>
        </ContentSource>
      </Content>
      <CoverWrapper>
        <Cover src={manga.coverImage.original.url} alt={manga.title} />
        <StatusFavorite isFavorited={true} mangaId={manga.id} />
      </CoverWrapper>
    </Container>
  );
};

export const ChapterTitle = styled(Box)(({ theme }) => ({
  color: theme.palette.primary.main,
  fontSize: '3.2rem',
  lineHeight: '3.2rem',
  fontWeight: 700,
  paddingBottom: '1.6rem',

  '@media (min-width: 375px)': {
    fontSize: '4.8rem',
    lineHeight: '4.8rem',
    fontWeight: 700,
  },
}));

const LinkProfile = styled(Link)(({ theme }) => ({
  color: theme.palette.primary.main,
  textDecoration: 'underline',
  textUnderlineOffset: '4px',
  textDecorationColor: theme.palette.primary.light,

  '&:hover': {
    color: theme.palette.primary.main,
    textDecoration: 'underline',
    textDecorationColor: theme.palette.primary.main,
  },
}));

const CoverWrapper = styled('div')`
  position: relative;
  grid-column: 1/-1;
  grid-row: 1/2;
  text-align: right;

  @media (min-width: 768px) {
    grid-row: auto;
    grid-column: -4 / span 4;
  }

  @media (min-width: 1270px) {
    grid-column: -6 / span 5;
  }
`;

const Cover = styled('img')`
  object-fit: cover;
  object-position: top;
  border-radius: 0.8rem;
  width: 100%;
  height: 18.4rem;

  @media (min-width: 375px) {
    height: 40rem;
  }

  @media (min-width: 768px) {
    height: auto;
  }
`;

const ContentSource = styled(Box)(({ theme }) => ({
  background: theme.palette.primary.contrastText,
  padding: '3.2rem',
  borderRadius: '0.8rem',
  display: 'flex',
  flexDirection: 'column',
  gap: '1.6rem',
}));

const Description = styled(Box)(
  ({ theme }) => `
  font-size: 1.8rem;
  font-weight: 400;
  margin-bottom: 3.2rem;
  line-height: 3rem;
  color: theme.palette.primary.main;
`,
);

const GenresWrapper = styled('div')`
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-bottom: 2rem;
`;

const GenreItem = styled(Box)(({ theme }) => ({
  background: theme.palette.background.chip,
  color: theme.palette.text.primary,
  margin: '0 1rem 1rem 0',
  padding: '0.4rem 1.2rem 0.6rem',
  borderRadius: '20rem',
  '-webkit-transition': 'background 0.35s, color 0.2s',
  transition: 'background 0.35s, color 0.2s',
  cursor: 'pointer',

  '&:hover': {
    background: theme.palette.background.contrastCard,
    color: theme.palette.primary.contrastText,
  },
}));

const Container = styled('div')`
  display: grid;
  grid-template-columns: repeat(6, minmax(0rem, 1fr));
  column-gap: 1.6rem;
  padding-top: 9rem;
  padding-right: 3.2rem;
  padding-left: 3.2rem;
  padding-bottom: 0 !important;

  @media (min-width: 768px) {
    grid-template-columns: repeat(13, minmax(0rem, 1fr));
    column-gap: 2rem;
  }

  @media (min-width: 768px) {
    padding: 8.4rem 4rem;
  }

  @media (min-width: 1270px) {
    grid-template-columns: repeat(24, minmax(0rem, 1fr));
    max-width: none;
  }
`;

const Content = styled('div')`
  grid-column: 1 / -1;
  z-index: 20;

  @media (min-width: 768px) {
    grid-column: 2 / span 8;
  }

  @media (min-width: 1270px) {
    grid-column: 5 / span 13;
  }
`;

interface AlterTitleProps {
  title: string;
}

const AlterTitles: React.FC<AlterTitleProps> = ({ title }) => {
  const [expanded, setExpanded] = useState<boolean>(false);

  const expand = () => setExpanded(prev => !prev);
  const collapse = () => setExpanded(false);

  return (
    <AlterTitlesContainer tabIndex={0} onBlur={collapse} onClick={expand}>
      <AlterTitlePreview>
        {title} <ExpandMoreIcon fontSize={'large'} />
      </AlterTitlePreview>
      {expanded && (
        <AlterTitlesContent>
          <AlterTitle>{title}</AlterTitle>
        </AlterTitlesContent>
      )}
    </AlterTitlesContainer>
  );
};

const AlterTitle = styled('p')(({ theme }) => ({
  fontSize: '1.5rem',
  lineHeight: '1rem',
  fontWeight: 700,
  color: theme.palette.text.primary,
  margin: 0,
  padding: 0,

  '@media (min-width: 375px)': {
    fontSize: '2rem',
  },
}));

const AlterTitlesContainer = styled('div')`
  cursor: pointer;
  position: relative;
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  padding-bottom: 0.4rem;
  margin: 0;
`;

const AlterTitlePreview = styled('div')(({ theme }) => ({
  fontSize: '1.8rem',
  fontWeight: 700,
  color: theme.palette.text.secondary,
  paddingRight: '0.8rem',
  margin: '1rem 0',

  '@media (min-width: 375px)': {
    fontSize: '2.4rem',
  },
}));

const AlterTitlesContent = styled('div')(({ theme }) => ({
  cursor: 'default',
  display: 'flex',
  opacity: 1,
  zIndex: 2,
  animation: '1s ease-in-out',
  position: 'absolute',
  bottom: '-1rem',
  left: '-3.2rem',
  right: 0,
  transform: 'translateY(100%)',
  color: theme.palette.text.secondary,
  background: theme.palette.background.card,
  padding: '3.2rem',
  borderRadius: '0.8rem',
  boxShadow: `0 0.6rem 1rem ${theme.palette.text.secondary}`,
  flexDirection: 'column',
  gap: '1.8rem',

  '@media (min-width: 938px)': {
    right: 'auto',
  },
}));

export default Detail;
