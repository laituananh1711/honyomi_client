import { Favorite, FavoriteBorder } from '@mui/icons-material';
import { CircularProgress } from '@mui/material';
import { meApi } from 'app/api/me';
import { SmallButton } from 'app/components/Button';
import { useSessionContext } from 'app/hooks/sessionContext';
import { useAsync } from 'app/hooks/useAsync';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { messages } from '../messages';

interface IStatusFavoriteProps {
  isFavorited: boolean;
  mangaId: string;
}

export function StatusFavorite({ isFavorited, mangaId }: IStatusFavoriteProps) {
  const { t } = useTranslation();
  const { data: isFavor, run, loading } = useAsync<boolean>({ data: false });
  const [isFavorite, setFavorite] = useState<boolean>(isFavor ?? false);
  const [session] = useSessionContext();

  useEffect(() => {
    run(meApi.checkFavorite(mangaId));
  }, [run, mangaId, isFavorite]);
  const handleLike = () => {
    meApi.addFavorite(mangaId).then(res => {
      setFavorite(!isFavorite);
    });
  };

  const handleDislike = () => {
    meApi.removeFavorite(mangaId).then(res => {
      setFavorite(!isFavorite);
    });
  };

  if (loading) {
    return <CircularProgress />;
  }
  return (
    <>
      {isFavor === true ? (
        <SmallButton
          sx={{
            marginTop: '1rem',
          }}
          onClick={handleDislike}
          disabled={!session.isAuthenticated}
        >
          <Favorite /> {t(messages.liked())}
        </SmallButton>
      ) : (
        <SmallButton
          sx={{
            marginTop: '1rem',
          }}
          onClick={handleLike}
          disabled={!session.isAuthenticated}
        >
          <FavoriteBorder /> {t(messages.like())}
        </SmallButton>
      )}
    </>
  );
}
