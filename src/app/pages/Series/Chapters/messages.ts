import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  like: () => _t(translations.mangaFeature.like),
  liked: () => _t(translations.mangaFeature.liked),
  loginRequired: () => _t(translations.favoritePage.loginRequired),
};
