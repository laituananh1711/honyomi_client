import { CircularProgress, Typography } from '@mui/material';
import { CardPopular } from 'app/components/CardPopular';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import mangaApi from '../../../../api/manga/mangaApi';
import { SERIES } from '../../../../api/manga/mangaApi.constants';
import { MostPopularMangaDTO } from '../../../../api/types';
import { RenderError } from '../../../../components/Error';
import { useAsync } from '../../../../hooks/useAsync';
import { messages } from '../messages';
import './style.css';

export function MostPopular() {
  const { t } = useTranslation();
  const {
    data: mostPopularMangas,
    run,
    error,
    loading,
  } = useAsync<MostPopularMangaDTO[]>({
    data: [],
  });

  useEffect(() => {
    run(mangaApi.getMostPopularManga());
  }, [run]);

  return (
    <div className="css-last-update">
      <div className="css-title-category">
        <Typography className="css-category" sx={{ color: 'text.primary' }}>
          {t(messages.mostPopular())}
        </Typography>
      </div>
      <ol className="css-most-popular-serie">
        {error && <RenderError error={error} />}
        {loading ? (
          <CircularProgress />
        ) : (
          mostPopularMangas!.map((serie, index) => (
            <Link
              className="css-link"
              to={`${SERIES}/${serie.manga.id}`}
              key={serie.manga.id}
            >
              <CardPopular
                title={serie.manga.title}
                rank={index + 1}
                imageUrl={serie.manga.coverImage.original.url}
              />
            </Link>
          ))
        )}
      </ol>
    </div>
  );
}
