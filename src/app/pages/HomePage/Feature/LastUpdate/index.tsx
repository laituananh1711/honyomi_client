import { CircularProgress, Typography } from '@mui/material';
import mangaApi from 'app/api/manga/mangaApi';
import { READER } from 'app/api/manga/mangaApi.constants';
import { CardImage } from 'app/components/CardImage';
import { RenderError } from 'app/components/Error';
import { useAsync } from 'app/hooks/useAsync';
import { LastUpdateChapter } from 'app/models/LastUpdateChapter';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { messages } from '../messages';
import './style.css';

interface Prop {
  name: string;
}

export function LastUpdate({ name }: Prop) {
  const { t } = useTranslation();
  const { loading, data, error, run } = useAsync<LastUpdateChapter[]>();

  const lastUpdatedChapters = data ?? [];

  React.useEffect(() => {
    run(mangaApi.getLastUpdate());
  }, [run]);

  return (
    <div className="css-last-update">
      <div className="css-title-category">
        <Typography className="css-category" sx={{ color: 'text.primary' }}>
          {name}
        </Typography>
      </div>

      <div className="css-series-wrapper">
        {loading ? (
          <CircularProgress />
        ) : error ? (
          <RenderError error={error} />
        ) : (
          lastUpdatedChapters.map(serie => (
            <div key={serie.id}>
              <CardImage
                title={serie.title}
                subtitle={t(messages.chapter()) + ' ' + serie.number}
                imageUrl={serie.thumbnailUrl}
                path={`/${READER}/${serie.manga.id}/${serie.number}`}
              />
            </div>
          ))
        )}
      </div>
    </div>
  );
}
