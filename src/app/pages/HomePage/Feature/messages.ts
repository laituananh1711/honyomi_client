import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  lastUpdate: () => _t(translations.mangaFeature.lastUpdate),
  newSeries: () => _t(translations.mangaFeature.newSeries),
  mostPopular: () => _t(translations.mangaFeature.mostPopular),
  chapter: () => _t(translations.chapterFeature.chapter),
};
