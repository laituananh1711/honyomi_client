import { Box, CircularProgress, Typography } from '@mui/material';
import mangaApi from 'app/api/manga/mangaApi';
import { SERIES } from 'app/api/manga/mangaApi.constants';
import { CardImageDiscover } from 'app/components/CardImageDiscover';
import { RenderError } from 'app/components/Error';
import { useAsync } from 'app/hooks/useAsync';
import { Manga } from 'app/models/Manga';
import React from 'react';
import './style.css';

interface Prop {
  name: string;
}

export function NewSeries({ name }: Prop) {
  const { loading, data, error, run } = useAsync<Manga[]>();

  const newSeries = data ?? [];

  React.useEffect(() => {
    run(mangaApi.getNewSeries());
  }, [run]);
  return (
    <Box className="css-new-serie" sx={{ bgcolor: 'background.card' }}>
      <div className="css-title-new-series">
        <Typography
          className="css-category-new-serie"
          sx={{ color: 'text.primary' }}
        >
          {name}
        </Typography>
      </div>

      <div className="css-new-series-wrapper">
        {loading ? (
          <CircularProgress />
        ) : error ? (
          <RenderError error={error} />
        ) : (
          newSeries.map((serie, index) => (
            <div key={index}>
              <CardImageDiscover
                title={serie.title}
                subtitle={serie.mangaType}
                imageUrl={serie.coverImage.original.url}
                path={`/${SERIES}/${serie.id}`}
              />
            </div>
          ))
        )}
      </div>
    </Box>
  );
}
