import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { messages } from './messages';
import { useTranslation } from 'react-i18next';
import { CustomCarousel } from 'app/components/CustomCarousel';
import '../../components/CustomCarousel/customCarousel.css';
import { LastUpdate } from './Feature/LastUpdate';
import { MostPopular } from './Feature/MostPopular';
import { NewSeries } from './Feature/NewSeries';
import { useAsync } from 'app/hooks/useAsync';
import { RecommendedManga } from 'app/models/RecommendManga';
import mangaApi from 'app/api/manga/mangaApi';

export function HomePage() {
  const { t } = useTranslation();
  const { loading, data, error, run } = useAsync<RecommendedManga[]>();

  const carouselSlides = data ?? [];

  React.useEffect(() => {
    run(mangaApi.getRecommendedMangas());
  }, [run]);

  return (
    <>
      <Helmet>
        <title>{t('homePage.title')}</title>
        <meta name="description" content={t(messages.metadataDes())} />
      </Helmet>
      <CustomCarousel
        carouselSlides={carouselSlides}
        loading={loading}
        error={error}
      />
      <LastUpdate name={t(messages.lastUpdate())} />
      <MostPopular />
      <NewSeries name={t(messages.newSeries())} />
    </>
  );
}
