import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  metadataDes: () => _t(translations.homePage.metadata.description),
  title: () => _t(translations.homePage.title),
  welcome: () => _t(translations.homePage.welcome),
  lastUpdate: () => _t(translations.mangaFeature.lastUpdate),
  mostPopular: () => _t(translations.mangaFeature.mostPopular),
  newSeries: () => _t(translations.mangaFeature.newSeries),
};
