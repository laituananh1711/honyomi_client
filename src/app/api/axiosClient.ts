import axios, { AxiosRequestConfig } from 'axios';
import qs from 'qs';

const axiosClient = axios.create({
  baseURL: process.env.REACT_APP_API_URL || 'http://localhost:3000',
  headers: {
    'Content-Type': 'application/json',
  },
  paramsSerializer: params => {
    return qs.stringify(params, { arrayFormat: 'brackets' });
  },
});

axiosClient.interceptors.request.use(
  function (config: AxiosRequestConfig) {
    //TODO:Handle token here
    const accessToken = window.localStorage.getItem('accessToken');
    if (accessToken) {
      config.headers!['Authorization'] = `Bearer ${accessToken}`;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  },
);

export default axiosClient;
