import { Author } from 'app/models/Author';
import { Category } from 'app/models/Category';
import { Manga } from 'app/models/Manga';
import { CoverImage } from '../models/CoverImage';
import { Setting } from '../models/User';

export interface PaginatedResultsMeta {
  perPage: number;
  totalItems: number;
  totalPages: number;
  currentPage: number;
}

export interface ServerError {
  code: string;
  detail: string;
  meta: unknown;
}
export interface ServerResponse<T = any, M = any> {
  data: T;
  errors?: ServerError[];
  meta?: M;
}

export enum MangaStatus {
  ONGOING = 'ongoing',
  FINISHED = 'finished',
  DROPPED = 'dropped',
}

export interface MangaStatusDTO {
  status: {
    name: string;
    value: string;
  };
  mangaCount: number;
}

// MangaFilterQuery is a filter object for GetManga API.
// Get manga where `translatedLanguage in [] AND statues in [] AND ...`
// Empty array and null field will be ignored when stringified to query strings.
export interface MangaFilterQuery {
  // a comma-separated list of language code
  translatedLanguage?: string[];

  // status of the manga: ongoing, dropped, finished.
  statuses?: string[];

  // author's id to be filtered.
  authors?: string[];

  // category's id to be filtered.
  categories?: string[];
}

// MangaQuery is a query string object.
export interface MangaQuery {
  sort?: string[];
  include?: string[];
  q?: string;
  filter?: MangaFilterQuery;
}

// CategoryWithCountDTO is an object which stores category info and how many manga matches this criteria.
export interface CategoryWithCountDTO {
  category: Category;
  mangaCount: number;
}

// AuthorWithCountDTO ...
export interface AuthorWithCountDTO {
  author: Author;
  mangaCount: number;
}

export interface MostPopularMangaDTO {
  manga: {
    id: string;
    status: string;
    ageRating: string;
    mangaType: string;
    synopsis: string;
    title: string;
    createdAt: string;
    coverImage: CoverImage;
  };
  viewCount: number;
}

export interface UpdateProfileDto {
  password?: string;
  setting?: Setting;
  email?: string;
  avatarUrl?: string;
}

export type LanguageCodeToName = Record<
  string,
  { name: string; nativeName: string }
>;
