import { AxiosResponse } from 'axios';
import { Manga } from '../../models/Manga';
import { User } from '../../models/User';
import axiosClient from '../axiosClient';
import { ServerResponse, UpdateProfileDto } from '../types';

const GET_FAVORITES = '/me/favorite';
const GET_HISTORY = '/me/history';
const GET_PROFILE = '/me/profile';
const UPDATE_PASSWORD = '/me/password';
const STATUS = 'status';

export const meApi = {
  getFavorites(): Promise<Manga[]> {
    return axiosClient
      .get<ServerResponse<Manga[]>>(GET_FAVORITES)
      .then(res => res.data.data);
  },
  getHistory(): Promise<Manga[]> {
    return axiosClient
      .get<ServerResponse<Manga[]>>(GET_HISTORY)
      .then(res => res.data.data);
  },
  getProfile(): Promise<User> {
    return axiosClient
      .get<ServerResponse<User>>(GET_PROFILE)
      .then(res => res.data.data);
  },
  updateProfile(dto: UpdateProfileDto): Promise<User> {
    return axiosClient.put<ServerResponse<User>>(GET_PROFILE, dto).then(res => {
      return res.data.data;
    });
  },
  updatePassword(dto: {
    currentPassword: string;
    newPassword: string;
  }): Promise<User> {
    return axiosClient
      .put<ServerResponse<User>>(UPDATE_PASSWORD, dto)
      .then(res => res.data.data);
  },
  removeFavorite(manga: string): Promise<Manga[]> {
    return axiosClient
      .delete<ServerResponse<Manga[]>>(GET_FAVORITES, {
        data: {
          manga,
        },
      })
      .then(res => res.data.data);
  },
  addFavorite(mangaId: string): Promise<Manga[]> {
    const data = {
      manga: mangaId,
    };
    return axiosClient
      .post<ServerResponse<Manga[]>>(GET_FAVORITES, data)
      .then(res => res.data.data);
  },
  checkFavorite(mangaId: string): Promise<boolean> {
    return axiosClient
      .get(`${GET_FAVORITES}/${mangaId}/${STATUS}`)
      .then((res: AxiosResponse<any>) => {
        return res.data.data;
      });
  },
  addToHistory(chapterId: string): Promise<void> {
    return axiosClient.post(GET_HISTORY, {
      chapterId,
    });
  },
};
