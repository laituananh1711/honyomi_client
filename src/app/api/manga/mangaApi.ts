import { LastUpdateChapter } from 'app/models/LastUpdateChapter';
import { Manga } from 'app/models/Manga';
import { RecommendedManga } from 'app/models/RecommendManga';
import { AxiosResponse } from 'axios';
import axiosClient from '../axiosClient';
import {
  MangaQuery,
  MangaStatusDTO,
  MostPopularMangaDTO,
  PaginatedResultsMeta,
  ServerResponse,
} from '../types';
import {
  FAVORITE_MANGAS,
  GET_MANGA,
  GET_MANGA_STATUS,
  LAST_UPDATE_MANGAS,
  MANGA,
  MOST_POPULAR_MANGAS,
  NEW_SERIES,
  RECOMMENDED_MANGAS,
} from './mangaApi.constants';

const mangaApi = {
  getFavoriteMangas(): Promise<Manga[]> {
    const url = FAVORITE_MANGAS;
    return axiosClient.get(url).then((response: AxiosResponse<any>) => {
      return response.data.data;
    });
  },
  getRecommendedMangas(): Promise<RecommendedManga[]> {
    const url = RECOMMENDED_MANGAS;
    return axiosClient
      .get(url, {
        params: {
          isPublished: true,
        },
      })
      .then((response: AxiosResponse<any>) => {
        return response.data.data;
      });
  },
  getLastUpdate(): Promise<LastUpdateChapter[]> {
    const url = LAST_UPDATE_MANGAS;
    return axiosClient.get(url).then((response: AxiosResponse<any>) => {
      return response.data.data;
    });
  },
  getNewSeries(): Promise<Manga[]> {
    const url = NEW_SERIES;
    return axiosClient.get(url).then((response: AxiosResponse<any>) => {
      return response.data.data;
    });
  },
  getManga(
    query: MangaQuery,
  ): Promise<{ data: Manga[]; meta: PaginatedResultsMeta }> {
    const url = GET_MANGA;
    return axiosClient
      .get<ServerResponse<Manga[], PaginatedResultsMeta>>(url, {
        params: query,
      })
      .then(res => ({
        data: res.data.data,
        meta: res.data.meta!,
      }));
  },
  getMangaStatusWithCount(): Promise<MangaStatusDTO[]> {
    return axiosClient
      .get<ServerResponse<MangaStatusDTO[]>>(GET_MANGA_STATUS)
      .then(res => res.data.data);
  },
  getMangaEndpoint(mangaId: string): Promise<Manga> {
    const url = `/${MANGA}/${mangaId}`;
    return axiosClient.get(url).then((response: AxiosResponse<any>) => {
      return response.data.data;
    });
  },
  getMostPopularManga(): Promise<MostPopularMangaDTO[]> {
    const url = MOST_POPULAR_MANGAS;
    return axiosClient
      .get<ServerResponse<MostPopularMangaDTO[]>>(url)
      .then(res => res.data.data);
  },
};
export default mangaApi;
