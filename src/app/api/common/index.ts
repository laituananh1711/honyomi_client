import axiosClient from '../axiosClient';
import { LanguageCodeToName, ServerResponse } from '../types';

const GET_LANGUAGES = '/languages';

const commonApi = {
  getLanguages(): Promise<LanguageCodeToName> {
    return axiosClient
      .get<ServerResponse<LanguageCodeToName>>(GET_LANGUAGES)
      .then(res => res.data.data);
  },
};

export default commonApi;
