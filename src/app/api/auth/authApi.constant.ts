export const LOGIN = '/auth/login';
export const GET_ACCESS_TOKEN = 'auth/access';
export const GET_PROFILE = '/me/profile';
export const REGISTER = '/auth/register';
export const LOGOUT = '/auth/logout';
export const LOGIN_GOOGLE = '/auth/firebase';
