import { User } from 'app/models/User';
import { AxiosResponse } from 'axios';
import axiosClient from '../axiosClient';
import { ServerResponse } from '../types';
import {
  GET_ACCESS_TOKEN,
  GET_PROFILE,
  LOGIN,
  LOGIN_GOOGLE,
  LOGOUT,
  REGISTER,
} from './authApi.constant';

const headers = {
  'Content-Type': 'application/json',
};

const authApi = {
  async login(_username: string, _password: string) {
    const url = LOGIN;
    const data = {
      username: _username,
      password: _password,
    };
    return axiosClient
      .post(url, data, { headers: headers })
      .then((response: AxiosResponse<any>) => {
        const result = response.data.data;
        window.localStorage.setItem('accessToken', result.accessToken);
        window.localStorage.setItem('refreshToken', result.refreshToken);
      });
  },

  loginWithGoogle(idToken: string) {
    const url = LOGIN_GOOGLE;
    const data = {
      idToken,
    };
    return axiosClient.post<ServerResponse<any>>(url, data).then(res => {
      const result = res.data.data;
      window.localStorage.setItem('accessToken', result.accessToken);
      window.localStorage.setItem('refreshToken', result.refreshToken);
      return res;
    });
  },

  getAccessToken(refreshToken: string): Promise<string> {
    const url = GET_ACCESS_TOKEN;
    return axiosClient
      .post(url, {
        params: refreshToken,
      })
      .then((response: AxiosResponse<any>) => {
        const accessToken = response.data.data.accessToken;
        return accessToken;
      });
  },

  getProfile(): Promise<User> {
    const url = GET_PROFILE;
    return axiosClient.get(url).then((response: AxiosResponse<any>) => {
      return response.data.data;
    });
  },

  register(
    _username: string,
    _email: string,
    _password: string,
  ): Promise<{ data: {}; meta: { message: string } }> {
    const url = REGISTER;
    const data = {
      username: _username,
      email: _email,
      password: _password,
    };
    return axiosClient
      .post(url, data, { headers: headers })
      .then((response: AxiosResponse<any>) => ({
        data: response.data.data,
        meta: response.data.meta!,
      }));
  },

  logout(): Promise<{ meta: { message: string } }> {
    const url = LOGOUT;
    return axiosClient
      .post(url, { headers: headers })
      .then((response: AxiosResponse<any>) => {
        window.localStorage.removeItem('accessToken');
        window.localStorage.removeItem('refreshToken');
        return response.data.meta!;
      });
  },
};
export default authApi;
