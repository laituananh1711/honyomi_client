import axiosClient from '../axiosClient';

const AUTH = 'auth';

const authApi = {
  register: (data: any) => {
    return axiosClient.post(AUTH + '/register', data);
  },
};

export default authApi;
