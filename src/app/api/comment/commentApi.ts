import { Comment } from 'app/models/Comment';
import axiosClient from '../axiosClient';
import { ServerResponse } from '../types';
import { CHAPTERS, COMMENTS, REPLIES } from './commentApi.constant';

const headers = {
  'Content-Type': 'application/json',
};

const commentApi = {
  getRootComments(chapterId: string, _sort?: string): Promise<Comment[]> {
    const url = `/${CHAPTERS}/${chapterId}/${COMMENTS}`;
    const params = {
      sort: _sort ?? '',
    };
    return axiosClient
      .get<ServerResponse<Comment[]>>(url, { params: params })
      .then(res => {
        return res.data.data;
      });
  },
  getReplies(commentId: string, _sort?: string): Promise<Comment[]> {
    const url = `/${COMMENTS}/${commentId}/${REPLIES}`;
    const params = {
      sort: _sort ?? '',
    };
    return axiosClient
      .get<ServerResponse<Comment[]>>(url, { params: params })
      .then(res => {
        return res.data.data;
      });
  },
  addNewComment(chapterId: string, _comment: string): Promise<Comment[]> {
    const url = `/${CHAPTERS}/${chapterId}/${COMMENTS}`;
    const data = {
      comment: _comment,
    };
    return axiosClient
      .post<ServerResponse<Comment[]>>(url, data, { headers: headers })
      .then(res => {
        return res.data.data;
      });
  },
  addNewReply(commentId: string, _comment: string): Promise<Comment[]> {
    const url = `/${COMMENTS}/${commentId}/${REPLIES}`;
    const data = {
      comment: _comment,
    };
    return axiosClient
      .post<ServerResponse<Comment[]>>(url, data, { headers: headers })
      .then(res => {
        return res.data.data;
      });
  },
};
export default commentApi;
