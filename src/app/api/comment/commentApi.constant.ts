export const COMMENTS = 'comments';
export const REPLIES = 'replies';
export const CHAPTERS = 'chapters';
export const SORT_DESC_PARAM = '-repliesCount';
export const SORT_ASC_PARAM = '+repliesCount';
export const SORT_DESC_TIME_PARAM = '-createdAt';
export const SORT_ASC_TIME_PARAM = '+createdAt';
