import { Author } from '../../models/Author';
import axiosClient from '../axiosClient';
import { AuthorWithCountDTO, ServerResponse } from '../types';

const GET_AUTHORS = 'authors';
const GET_AUTHORS_WITH_COUNT = 'authors/withMangaCount';

const authorApi = {
  getAuthors(): Promise<Author[]> {
    return axiosClient
      .get<ServerResponse<Author[]>>(GET_AUTHORS)
      .then(res => res.data.data);
  },
  getAuthorsWithCount(): Promise<AuthorWithCountDTO[]> {
    return axiosClient
      .get<ServerResponse<AuthorWithCountDTO[]>>(GET_AUTHORS_WITH_COUNT)
      .then(res => res.data.data);
  },
};

export default authorApi;
