import { Category } from '../../models/Category';
import axiosClient from '../axiosClient';
import { CategoryWithCountDTO, ServerResponse } from '../types';

const GET_CATEGORIES = 'categories';
const GET_CATEGORIES_WITH_COUNT = 'categories/withMangaCount';

const categoryApi = {
  getCategories(): Promise<Category[]> {
    return axiosClient
      .get<ServerResponse<Category[]>>(GET_CATEGORIES)
      .then(res => res.data.data);
  },
  getCategoriesWithCount(): Promise<CategoryWithCountDTO[]> {
    return axiosClient
      .get<ServerResponse<CategoryWithCountDTO[]>>(GET_CATEGORIES_WITH_COUNT)
      .then(res => res.data.data);
  },
};

export default categoryApi;
