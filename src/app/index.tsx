/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import {
  Switch,
  Route,
  useLocation,
  useRouteMatch,
  Redirect,
} from 'react-router-dom';

import { useTranslation } from 'react-i18next';
import {
  createTheme,
  CssBaseline,
  PaletteMode,
  ThemeProvider,
} from '@mui/material';
import getTheme from 'styles/themes/getTheme';
import styled from 'styled-components/macro';
import NotiBar from './components/NotiBar';
import { Navbar } from './components/Navbar';
import Footer from './components/Footer';
import { HomePage } from './pages/HomePage/Loadable';

import { NotFoundPage } from './components/NotFoundPage/Loadable';
import { Reader } from './pages/Series/Reader/Loadable';
import { Chapters } from './pages/Series/Chapters/Loadable';
import { Login } from './pages/Auth/Login/Loadable';
import { Register } from './pages/Auth/Register/Loadable';
import { DiscoverPage } from './pages/DiscoverPage/Loadable';
import { useSessionContext } from './hooks/sessionContext';
import ProtectedRoute, { ProtectedRouteProps } from './components/PrivateRoute';
import { Settings } from './pages/User/Settings/Loadable';
import { Profile } from './pages/User/Profile/Loadable';
import { FavoritePage } from './pages/Favorites/Loadable';
import { ReadingHistoryPage } from './pages/ReadingHistoryPage/Loadable';

import './index.css';
import { Logout } from './pages/Auth/Logout';

export const ColorModeContext = React.createContext({
  toggleColorMode: () => {},
  setMode: (mode: 'light' | 'dark') => {},
});

export function App() {
  const [sessionContext, updateSessionContext] = useSessionContext();

  const setRedirectPath = (path: string) => {
    updateSessionContext({ ...sessionContext, redirectPath: path });
  };

  const defaultProtectedRouteProps: ProtectedRouteProps = {
    isAuthenticated: sessionContext.isAuthenticated!,
    authenticationPath: '/login',
    redirectPath: sessionContext.redirectPath,
    currentUser: sessionContext.currentUser,
    setRedirectPath: setRedirectPath,
  };

  const { i18n } = useTranslation();
  const location = useLocation();
  const [mode, setMode] = React.useState<'light' | 'dark'>(
    (sessionContext.currentUser.setting.uiTheme as PaletteMode) || 'light',
  );
  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setMode(prevMode => (prevMode === 'light' ? 'dark' : 'light'));
      },
      setMode: (mode: 'light' | 'dark') => {
        setMode(mode);
      },
    }),
    [],
  );

  const theme = React.useMemo(() => createTheme(getTheme(mode)), [mode]);
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />

        <Helmet
          titleTemplate="%s - Honyomi"
          defaultTitle="Honyomi"
          htmlAttributes={{
            lang: sessionContext.currentUser.setting.language ?? i18n.language,
          }}
        >
          <meta name="viewport" content="initial-scale=1, width=device-width" />
          <meta name="description" content="Basic Manga Reader App" />
        </Helmet>
        <NotiBar />
        <div className="app-container">
          <div className="app-content">
            <Container>
              <Navbar />
              <Switch>
                <Route exact path="/" component={HomePage} />
                <Route exact path="/reader">
                  <Redirect to="/" />
                </Route>
                <Route path="/reader/:mangaId/:number" component={Reader} />
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <Route exact path="/discover" component={DiscoverPage} />
                <Route path="/series/:mangaId" component={Chapters} />
                <Route path="/favorites" component={FavoritePage} />
                <Route path="/history" component={ReadingHistoryPage} />
                <ProtectedRoute
                  {...defaultProtectedRouteProps}
                  path="/user"
                  component={User}
                />
                <ProtectedRoute
                  {...defaultProtectedRouteProps}
                  path="/logout"
                  component={Logout}
                />
                <Route component={NotFoundPage} />
              </Switch>
            </Container>
          </div>
          <div className="app-footer">
            {location.pathname.includes('/reader/') ? <></> : <Footer />}
          </div>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}

const User = () => {
  let { path } = useRouteMatch();
  const settingPath = `${path}/settings`;
  const profilePath = `${path}/profile`;

  return (
    <Switch>
      <Route exact path={path}>
        <Redirect to={profilePath} />
      </Route>
      <Route path={settingPath} component={Settings} />
      <Route path={profilePath} component={Profile} />
    </Switch>
  );
};

const Container = styled.div`
  width: 100vw;
  display: flex;
  flex-direction: column;
  position: relative;
`;
