export interface Author {
  id: string;
  description: string;
  name: string;
  createdAt: string;
}
