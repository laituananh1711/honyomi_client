export interface Chapter {
  id: string;
  title: string;
  number: string;
  thumbnailUrl: string;
  pages: string[];
  translatedLanguage: string;
  originalLanguage: string;
  viewCount: number;
}
