import { Manga } from './Manga';

export interface LastUpdateChapter {
  id: string;
  manga: Manga;
  number: string;
  pages: string[];
  thumbnailUrl: string;
  title: string;
  translatedLanguage: string;
}
