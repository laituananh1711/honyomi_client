import { User } from './User';

export type Session = {
  currentUser: User;
  isAuthenticated?: boolean;
  redirectPath: string;
};

export const userDefault: User = {
  id: '',
  avatarUrl: '',
  role: '',
  email: '',
  username: '',
  setting: {
    uiTheme: '',
    language: '',
    mangaLanguages: [],
  },
};

export const initialSession: Session = {
  currentUser: userDefault,
  redirectPath: '',
};
