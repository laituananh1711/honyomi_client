import { Chapter } from './Chapter';
import { User } from './User';

export interface Comment {
  id: string;
  repliesCount: number;
  comment: string;
  chapter?: Chapter | string;
  user: User;
  replies?: Comment[];
  createdAt: string;
  updatedAt: string;
}
