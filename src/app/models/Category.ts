export interface Category {
  id: string;
  title: string;
  description: string;
  nsfw: boolean;
  createdAt: string;
}
