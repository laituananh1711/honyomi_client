export interface CoverImage {
  original: {
    heightInPx: number;
    widthInPx: number;
    url: string;
  };
}
