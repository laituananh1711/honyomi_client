import { Author } from './Author';
import { Category } from './Category';
import { Chapter } from './Chapter';
import { CoverImage } from './CoverImage';

export interface Manga {
  id: string;
  chapters: Chapter[];
  categories: Category[];
  status: string;
  ageRating: string;
  mangaType: string;
  authors: Author[];
  synopsis: string;
  title: string;
  createdAt: string;
  coverImage: CoverImage;
  romajiTitle: string;
}
