import { Manga } from './Manga';

export interface RecommendedManga {
  id: string;
  createdAt: string;
  description: string;
  isPublished: boolean;
  titleColor: string;
  imageUrl: string;
  manga: Manga;
}
