export interface Setting {
  language: string;
  uiTheme: string;
  mangaLanguages: string[];
}

export interface User {
  id: string;
  username: string;
  email: string;
  role: string;
  setting: Setting;
  avatarUrl: string;
}
