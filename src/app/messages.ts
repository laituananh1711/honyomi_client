import { _t } from '../utils/messages';
import { translations } from '../locales/translations';

export const messages = {
  metadataDes: () => _t(translations.metadata.description),
};
