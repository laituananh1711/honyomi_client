import axios from 'axios';

const customAxios = axios.create({
  baseURL: process.env.PUBLIC_URL,
});
export default customAxios;
